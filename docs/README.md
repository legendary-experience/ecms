# Development Guide
Updated 05.05.2019

## Introduction
This is your new reference guide for development of Battle of four Armies.

## Table of Contents
1. [Setup][1]
2. [Development Discord][2]
3. [Git][3]
4. [Dota 2 Tools][4]
5. [KV][5]
6. [Sound][6]
7. [Map][7]
8. [Materials][8]
9. [Models][9]
10. [Particles][10]
11. [Icons][11]
12. [Lua][12]
13. [Panorama][13]
14. [TypeScript][14]
15. [Design][15]
16. [Balance][16]
17. [Bug-Reports][17]
18. [Documentation][18]
19. [Writing][19]
20. [Translation][20]
21. [Tournament][21]

[1]: setup/README.md
[2]: discord/README.md
[3]: git/README.md
[4]: dota2tools/README.md
[5]: kv/README.md
[6]: sound/README.md
[7]: map/README.md
[8]: materials/README.md
[9]: models/README.md
[10]: particles/README.md
[11]: icons/README.md
[12]: lua/README.md
[13]: panorama/README.md
[14]: typescript/README.md
[15]: design/README.md
[16]: balance/README.md
[17]: bug_reports/README.md
[18]: documentation/README.md
[19]: writing/README.md
[20]: translation/README.md
[21]: tournament/README.md