# Setup
Updated 05.05.2019

[< Table of Contents][0]

## Intro
Ready to get started working on Battle of four Armies?
* You'll need to [Setup your client][1].
* Then if you're a coder make sure you have [What you need][2].
* Finally, head over to your channel in the [Discord][4] and get started!
* Tickets can be found in the [GitLab Repo][3] aswell as our [Development Boards][4].
* Good luck and have fun!

[0]: ../README.md
[1]: install.md
[2]: what_you_need.md
[3]: https://gitlab.com/legendary-experience/BFA/issues
[4]: https://gitlab.com/legendary-experience/BFA/boards
[5]: https://discord.gg/asfQepx