# Toolchain (or: the fun part)
Updated 05.05.2019

[< Setup][0]

## (This is for RFC)

### Programming Tools:
First and obious: we do need the Workshop Tools for Dota 2. You can find it if you click once Dota 2 in your library and under DLC:

![Image of Dota 2 Tools](https://i.imgsafe.org/b48317a26e.png)

As coder we mostly work on text files. Se we also need an IDE.
Recommended: [Visual Studio Code](https://code.visualstudio.com/)
If you don't want to use VSCode, these are the requirements:
 - It should have syntax-highlighting for Lua, TypeScript, CSS, JS and XML.
 - It should have on-the-fy linting.
 - It MUST respect [.editorconfig][https://editorconfig.org/]
 - A working setup for [TypeScript](https://www.typescriptlang.org/) and optional [TypeScriptForLua](https://github.com/TypeScriptToLua)

Now you have to install [Lua](https://www.lua.org/) (for linting) and [node.js](https://nodejs.org/en/download)

If you using VSCode, following extensions you should use:
 - [Dota 2 KV Toolkit](https://marketplace.visualstudio.com/items?itemName=nicholasgao.dota-2-kv-toolkit)
 - [dota-reborn-code](https://marketplace.visualstudio.com/items?itemName=XavierCHN.dota-reborn-code)
 - [Gitlab Notifications](https://marketplace.visualstudio.com/items?itemName=logerfo.gitlab-notifications)
 - [GitLab Workflow](https://marketplace.visualstudio.com/items?itemName=fatihacet.gitlab-workflow)
 - [vscode-lua](https://marketplace.visualstudio.com/items?itemName=trixnz.vscode-lua)
 - [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)

### GitLab Links
* [Organization][1]
* [Repository][2]
* [Development Boards][3]
* [Issue Tickets][4]

### Collaborating Tools:
You need a GitLab account. Every changes you do, will first go in a new branch. If you finished your work, make a merge-request. Your request will be visited then and merged into the master branch or not.

[0]: README.md
[1]: https://gitlab.com/legendary-experience/
[2]: https://gitlab.com/legendary-experience/BFA/
[3]: https://gitlab.com/legendary-experience/BFA/boards
[4]: https://gitlab.com/legendary-experience/BFA/issues