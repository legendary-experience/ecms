<?php
/***************************************************************\
| Functions:													|
| - bool	public	init_log()									|
| - void	public	set_log(string $logString,					|
|						    string $type)						|
| - void	public	set_func_call(string $func,					|
|								  array  $conditions,			|
|								  bool   $fail = false,			|
|								  array  $failCondition = NULL)	|
\***************************************************************/

// Get everything started up...
define('eCMS', 1, true);
define('START_MICROTIME', microtime(true));

// Define variables.
$loader_debug               = true;

$loader_params              = '';

$loader_mkdir               = false;
$loader_chmod               = false;
$loader_reload              = false;

$loader_log_date            = null;
$loader_log_file			= null;
$loader_log_file_name		= null;
$loader_log_perm_dir_logs	= null;
$loader_log_string			= null;

$loader_perm				= null;
$loader_perm_htaccess		= null;
$loader_perm_index			= null;
$loader_perm_ecms			= null;
$loader_perm_ecms_class		= null;



/**
 * Init the loader-log-system.
 * Check if folder exists and has right permissions.
 * Of not, try to create one.
 * If creation failed, abort the whole script to protect the system.
 */
if(!file_exists('./logs/')) {
	echo "<span style='color:#0000FF;'>eCMS [INFO]: Directory for loader log-files doesn't exists. Try to create it...<br /></span>";
	
	$loader_mkdir = mkdir('./logs/', 0700, false);
	if(!$loader_mkdir) die("<br /><br /><br /><span style='color:#FF0000; font-weight:bold;'>eCMS [CRITICAL]: 0x0000000000<br />Stopping loader to protect the system.<br /><br />Please contact the system administrator.<br /><br /><br /></span>");
	$loader_reload = true;
	
	echo "<span style='color:#00FF00;'>eCMS [INFO]: OK...Directory for loader log-files successfully created.<br /><br /><br /></span>";
}

// Check the permissions.
$loader_log_perm_dir_logs = get_permissions('./logs/');
if($loader_log_perm_dir_logs !== '0700' && get_os() === 'LIN') {
	echo "<span style='color:#0000FF;'>eCMS [INFO]: Needs to fix the permissions for loader log directory...<br /></span>";
	
	// Correct the permissions.
	$loader_chmod = set_permissions('./logs/', 0700);
	if($loader_chmod === false) die("<br /><br /><br /><span style='color:#FF0000; font-weight:bold;'>eCMS [CRITICAL]: 0x0000000001<br />Stopping loader to protect the system.<br /><br />Please contact the system administrator.<br /><br /><br /></span>");
	if($loader_chmod === true) {
		$loader_reload = true;
		
		echo "<span style='color:#00FF00;'>eCMS [INFO]: OK...Permissions fixed.<br /></span>";
	}
}

// Now start the loader logging-system.
init_log();

// All ready now.
// Ok, we check now the '.htaccess' and 'index.php' files.
// Check, if .htaccess existing.
if(!file_exists('./.htaccess')) {
	set_log('0x0000000005', 'critical');
	die("<br /><br /><br /><span style='color:#FF0000; font-weight:bold;'>eCMS [CRITICAL]: 0x0000000005<br />Stopping loader to protect the system.<br /><br />Please contact the system administrator.<br /><br /><br /></span>");
}
// Check, if windows or linux.
$os = get_os();
if($os === 'LIN') {
	$loader_perm_htaccess = get_permissions('./.htaccess');
	if($loader_perm_htaccess !== '0400') {
		set_log('Needs to fix the permissions for .htaccess file...', 'info');
		echo "<span style='color:#0000FF;'>eCMS [INFO]: Needs to fix the permissions for .htaccess file...<br /></span>";
		
		// Correct the permission
		$loader_chmod = set_permissions('./.htaccess', 0400);
		if($loader_chmod === false) {
			set_log('0x0000000006', 'critical');
			die("<br /><br /><br /><span style='color:#FF0000; font-weight:bold;'>eCMS [CRITICAL]: 0x0000000006<br />Stopping loader to protect the system.<br /><br />Please contact the system administrator.<br /><br /><br /></span>");
		}
		if($loader_chmod === true) {
			set_log('OK...Permissions fixed.', 'info');
			echo "<span style='color:#00FF00;'>eCMS [INFO]: OK...Permissions fixed.<br /></span>";
			
			$loader_reload = true;
		}
	}
	
	// Same for the 'index.php' himself.
	$loader_perm_index = get_permissions('./index.php');
	if($loader_perm_index !== '0600') {
		set_log('Needs to fix the permissions for index file...', 'info');
		echo "<span style='color:#0000FF;'>eCMS [INFO]: Needs to fix the permissions for .htaccess file...<br /></span>";
		
		// Correct the permission
		$loader_chmod = set_permissions('./index.php', 0600);
		if($loader_chmod === false) {
			set_log('0x0000000007', 'critical');
			die("<br /><br /><br /><span style='color:#FF0000; font-weight:bold;'>eCMS [CRITICAL]: 0x0000000007<br />Stopping loader to protect the system.<br /><br />Please contact the system administrator.<br /><br /><br /></span>");
		}
		if($loader_chmod === true) {
			set_log('OK...Permissions fixed.', 'info');
			echo "<span style='color:#00FF00;'>eCMS [INFO]: OK...Permissions fixed.<br /></span>";
			
			$loader_reload = true;
		}
	}
}

// Now check for the eCMS folder.
// We check the ecms.class.php later, because of the permissions.
// If we have no x-flag, we can't check for file existing. ;)
if(!file_exists('./ecms/')) {
	set_log('0x0000000008', 'critical');
	die("<br /><br /><br /><span style='color:#FF0000; font-weight:bold;'>eCMS [CRITICAL]: 0x0000000008<br />Stopping loader to protect the system.<br /><br />Please contact the system administrator.<br /><br /><br /></span>");
}
// Check, if windows or linux.
$os = get_os();
if($os === 'LIN') {
	$loader_perm_ecms = get_permissions('./ecms/');
	if($loader_perm_ecms !== '0700') {
		set_log('Needs to fix the permissions for eCMS folder...', 'info');
		echo "<span style='color:#0000FF;'>eCMS [INFO]: Needs to fix the permissions for eCMS folder...<br /></span>";
		
		// Correct the permission
		$loader_chmod = set_permissions('./ecms/', 0700);
		if($loader_chmod === false) {
			set_log('0x0000000009', 'critical');
			die("<br /><br /><br /><span style='color:#FF0000; font-weight:bold;'>eCMS [CRITICAL]: 0x0000000009<br />Stopping loader to protect the system.<br /><br />Please contact the system administrator.<br /><br /><br /></span>");
		}
		if($loader_chmod === true) {
			set_log('OK...Permissions fixed.', 'info');
			echo "<span style='color:#00FF00;'>eCMS [INFO]: OK...Permissions fixed.<br /></span>";
			
			$loader_reload = true;
		}
	}
}

// Now we can check, if the ecms.class.php is existing and the permissions.
if(!file_exists('./ecms/ecms.class.php')) {
	set_log('0x0000000010', 'critical');
	die("<br /><br /><br /><span style='color:#FF0000; font-weight:bold;'>eCMS [CRITICAL]: 0x0000000010<br />Stopping loader to protect the system.<br /><br />Please contact the system administrator.<br /><br /><br /></span>");
}
// Check, if windows or linux.
$os = get_os();
if($os === 'LIN') {
	$loader_perm_ecms_class = get_permissions('./ecms/ecms.class.php');
	if($loader_perm_ecms_class !== '0700') {
		set_log('Needs to fix the permissions for eCMS basic class...', 'info');
		echo "<span style='color:#0000FF;'>eCMS [INFO]: Needs to fix the permissions for eCMS basic class...<br /></span>";
		
		// Correct the permission
		$loader_chmod = set_permissions('./ecms/ecms.class.php', 0700);
		if($loader_chmod === false) {
			set_log('0x0000000011', 'critical');
			die("<br /><br /><br /><span style='color:#FF0000; font-weight:bold;'>eCMS [CRITICAL]: 0x0000000011<br />Stopping loader to protect the system.<br /><br />Please contact the system administrator.<br /><br /><br /></span>");
		}
		if($loader_chmod === true) {
			set_log('OK...Permissions fixed.', 'info');
			echo "<span style='color:#00FF00;'>eCMS [INFO]: OK...Permissions fixed.<br /></span>";
			
			$loader_reload = true;
		}
	}
}

// Check for reload.
if($loader_reload === true) die("<br /><br /><br /><span style='color:0000FF;'>eCMS [INFO]: Permissions for startup files fixed.<br />Please reload the page.<br /><br />If you see this message five times or more, please contact the system administrator.");

// Load the ecms.class.php-file.
require_once('./ecms/ecms.class.php');
// Load the params.
if(isset($_GET['params'])) $loader_params = $_GET['params'];

// Now we needs to check, if our ecms.class.php has an ecms-class.
if(!class_exists('ecms')) {
	set_log('0x0000000012', 'critical');
	die("<br /><br /><br /><span style='color:#FF0000; font-weight:bold;'>eCMS [CRITICAL]: 0x0000000012<br />Stopping loader to protect the system.<br /><br />Please contact the system administrator.<br /><br /><br /></span>");
}

// Finaly, load the class and start the eCMS.
$ecms = new ecms($loader_params);






// Returns the OS.
# @return string
function get_os() {
	return strtoupper(substr(PHP_OS, 0, 3));
}
// Returns the permission of a file/directory.
# @param string $file
# 
# @return int
function get_permissions($file) {
	return substr(sprintf('%o', fileperms($file)), -4);
}
// Correct the permissions.
# @param string $file
# @param int    $perm
# 
# @return bool
function set_permissions($file, $perm) {
	return chmod($file, $perm);
}



// Initializes logging.
# @return void
function init_log() {
	// Prepare variables
	$loader_log_date			= date('Y-m-d', time());
	$loader_log_file_name		= $loader_log_date.'_loader.log';
	$loader_log_file			= str_replace('\\', '/', './logs/'.$loader_log_file_name);
	$loader_log_perm_dir_logs	= 0000;
	
	// Check for logs-directory existing
	if('./logs/' !== '' && is_dir('./logs/')) {
		// Check, if file exists.
		// Create new one, if needed.
		if(file_exists($loader_log_file) && is_file($loader_log_file)) {
			$loader_log_perm_file_logs = get_permissions($loader_log_file);
			if($loader_log_perm_file_logs !== '0600' && get_os() === 'LIN') {
				echo "<span style='color:#0000FF;'>eCMS [INFO]: Needs to fix the permissions for loader log file...<br /></span>";
				
				// Correct the permissions
				$loader_chmod = set_permissions($loader_log_file, 0600);
				if($loader_chmod === false) die("<span style='color:#FF0000; font-weight:bold;'>eCMS [CRITICAL]: 0x0000000003<br />Stopping loader to protect the system.<br /><br />Please contact the system administrator.<br /><br /><br /></span>");
				if($loader_chmod === true) {
					global $loader_reload;
					$loader_reload = true;
					
					echo "<span style='color:#00FF00;'>eCMS [INFO]: OK...Permissions fixed.<br /></span>";
				}
			}
		} else {
			file_put_contents($loader_log_file, "eCMS Loader-Logfile.\r\n\r\n", LOCK_EX);
			file_put_contents($loader_log_file, "-------------------------\r\n\r\n", FILE_APPEND|LOCK_EX);
			
			return true;
		}
	} else die("<span style='color:#FF0000; font-weight:bold;'>eCMS [CRITICAL]: 0x0000000002<br />Stopping loader to protect the system<br /><br />Please contact the system administrator.<br /><br /></span>");
	
	return true;
}

// Add string to log-file.
# @param string	$logString
# @param string	$type
# 
# @return void
function set_log($logString, $type) {
	// Prepare variables
	$loader_log_date			= date('Y-m-d', time());
	$loader_log_date_time		= date('H:i:s', time());
	$loader_log_file_name		= $loader_log_date.'_loader.log';
	$loader_log_file			= str_replace('\\', '/', './logs/'.$loader_log_file_name);
	$type_string				= '';
	
	if(init_log()) {
		if(is_string($type)) {
			switch($type) {
				case 'verbose':
					$type_string = 'VERBOSE';
					
					break;
				
				case 'debug':
					$type_string = 'DEBUG';
					
					break;
				
				case 'info':
					$type_string = 'INFO';
					
					break;
				
				case 'notice':
					$type_string = 'NOTICE';
					
					break;
				
				case 'warning':
					$type_string = 'WARNING';
					
					break;
				
				case 'error':
					$type_string = 'ERROR';
					
					break;
				
				case 'critical':
					$type_string = 'CRITICAL';
					
					break;
				
				case 'alert':
					$type_string = 'ALERT';
					
					break;
				
				
				case 'emerg':
					$type_string = 'EMERG';
					
					break;
			}
			
			// Check if log-level is high enough
			if($type_string !== 'DEBUG') {
				file_put_contents($loader_log_file, "[".$type_string."] ".$loader_log_date_time.": ".$logString."\r\n", FILE_APPEND|LOCK_EX);
				file_put_contents($loader_log_file, " Script executed by ".$_SERVER['REMOTE_ADDR'].":".$_SERVER['REMOTE_PORT']."\r\n", FILE_APPEND|LOCK_EX);
				
				file_put_contents($loader_log_file, "\r\n", FILE_APPEND|LOCK_EX);
			}
		} else {
			set_func_call(	'set_log', 
							array(	0	=> array(	'title'	=> 'logString', 
													'nType'	=> 'string',
													'gType'	=> gettype($logString),
													'value'	=> $log_string
												), 
									1	=> array(	'title'	=> 'type', 
													'nType'	=> 'string', 
													'gType'	=> gettype($type), 
													'value'	=> $type
												), 
									2	=> array(	'title'	=> 'loader_log_file', 
													'nType'	=> 'string', 
													'gType'	=> gettype($loader_log_file), 
													'value'	=> $loader_log_file
												)), 
							true, 
							array(	'title'	=> 'type', 
									'nType'	=> 'string', 
									'gType'	=> gettype($type), 
									'value'	=> $type
							 )
			);
			
			file_put_contents($loader_log_file, "\r\n", FILE_APPEND|LOCK_EX);
		}
	} else die("<span style='color:#FF0000; font-weight:bold;'>eCMS [CRITICAL]: 0x0000000004<br />Stopping loader to protect the system<br /><br />Please contact the system administrator.<br /><br /></span>");
}

// Add function call error to log-file.
# @param string	$func
# @param array	$conditions
# @param bool	$fail (default: false)
# @param array	$failCondition
# 
# @return void
function set_func_call($func, $conditions, $fail = false, $failCondition = NULL) {
	$loader_log_date = date('Y-m-d', time());
	$loader_log_date_time = date('H:i:s', time());
	$loader_log_file_name = $loader_log_date.'_loader.log';
	$loader_log_file = str_replace('\\', '/', './logs/'.$loader_log_file_name);
	
	set_log('Call set_func_call.', 'debug');
	$error = false;
	
	if(!is_string($func)) { 
		$error = true;
		$setFuncCall(	'set_func_call', 
						array(	0	=> array(	'title'	=> 'func', 
												'nType'	=> 'string', 
												'gType' => gettype($func),
												'value' => $func
												), 
								1	=> array(	'title'	=> 'conditions', 
												'nType'	=> 'array', 
												'gType' => gettype($conditions),
												'value' => $conditions
											), 
								2	=> array(	'title'	=> 'fail', 
												'nType'	=> 'bool', 
												'gType' => gettype($fail),
												'value' => $fail
											), 
								3	=> array(	'title'	=> 'failCondition', 
												'nType'	=> 'array', 
												'gType' => gettype($failCondition),
												'value' => $failCondition
											)
							 ),
						true, 
						array(	'title'	=> 'func', 
								'nType'	=> 'string', 
								'gType'	=> gettype($func), 
								'value'	=> $func
							 )
					  );
	}
	if(!is_array($conditions)) {
		$error = true;
		$setFuncCall(	'set_func_call', 
						array(	0	=> array(	'title'	=> 'func', 
												'nType'	=> 'string', 
												'gType' => gettype($func),
												'value' => $func
											), 
								1	=> array(	'title'	=> 'conditions', 
												'nType'	=> 'array', 
												'gType' => gettype($conditions),
												'value' => $conditions
											), 
								2	=> array(	'title'	=> 'fail', 
												'nType'	=> 'bool', 
												'gType' => gettype($fail),
												'value' => $fail
											), 
								3	=> array(	'title'	=> 'failCondition', 
												'nType'	=> 'array', 
												'gType' => gettype($failCondition),
												'value' => $failCondition
											)
							 ), 
						true, 
						array(	'title'	=> 'func', 
								'nType'	=> 'array', 
								'gType'	=> gettype($conditions), 
								'value'	=> $conditions
							 )
					  );
	}
	if(!is_bool($fail)) {
		$error = true;
		$setFuncCall(	'set_func_call', 
						array(	0	=> array(	'title'	=> 'func', 
												'nType'	=> 'string', 
												'gType' => gettype($func),
												'value' => $func
											), 
								1	=> array(	'title'	=> 'conditions', 
												'nType'	=> 'array', 
												'gType' => gettype($conditions),
												'value' => $conditions
											), 
								2	=> array(	'title'	=> 'fail', 
												'nType'	=> 'bool', 
												'gType' => gettype($fail),
												'value' => $fail
											), 
								3	=> array(	'title'	=> 'failCondition', 
												'nType'	=> 'array', 
												'gType' => gettype($failCondition),
												'value' => $failCondition
											)
							 ), 
						true, 
						array(	'title'	=> 'func', 
								'nType'	=> 'boolean', 
								'gType'	=> gettype($fail), 
								'value'	=> $fail
							 )
					  );
	}
	if(!is_array($failCondition) && $failCondition != NULL) {
		$error = true;
		$setFuncCall(	'set_func_call', 
						array(	0	=> array(	'title'	=> 'func', 
												'nType'	=> 'string', 
												'gType' => gettype($func),
												'value' => $func
											), 
								1	=> array(	'title'	=> 'conditions', 
												'nType'	=> 'array', 
												'gType' => gettype($conditions),
												'value' => $conditions
											), 
								2	=> array(	'title'	=> 'fail', 
												'nType'	=> 'bool', 
												'gType' => gettype($fail),
												'value' => $fail
											), 
								3	=> array(	'title'	=> 'failCondition', 
												'nType'	=> 'array', 
												'gType' => gettype($failCondition),
												'value' => $failCondition
											)
							 ), 
						true, 
						array(	'title'	=> 'func', 
								'nType'	=> 'array', 
								'gType'	=> gettype($failCondition), 
								'value'	=> $failCondition
							 )
					  );
	}
	
	if($error === false) {
		if($fail === true) {
			file_put_contents($loader_log_file, "[ALERT] ".$loader_log_date_time."\r\n", FILE_APPEND|LOCK_EX);
			file_put_contents($loader_log_file, "Called function ".$func." with fail conditions.\r\n", FILE_APPEND|LOCK_EX);
			file_put_contents($loader_log_file, "Condition: \$".$failCondition['title']."\r\n", FILE_APPEND|LOCK_EX);
			file_put_contents($loader_log_file, "Needed type: ".$failCondition['nType']."\r\n", FILE_APPEND|LOCK_EX);
			file_put_contents($loader_log_file, "Given type: ".$failCondition['gType']."\r\n\r\n", FILE_APPEND|LOCK_EX);
			file_put_contents($loader_log_file, "Given value: ".$failCondition['value']."\r\n\r\n", FILE_APPEND|LOCK_EX);
			
			for($i = 0; $i < count($conditions); $i++) {
				file_put_contents($loader_log_file, " - @param ".$conditions[$i]['gType']." (".$conditions[$i]['nType'].") \$".$conditions[$i]['title'].": ".$conditions[$i]['value']."\r\n", FILE_APPEND|LOCK_EX);
			}
			
			file_put_contents($loader_log_file, " Script executed by ".$_SERVER['REMOTE_ADDR'].":".$_SERVER['REMOTE_PORT']."\r\n", FILE_APPEND|LOCK_EX);
		} else {
			
		}
	}
	
	file_put_contents($loader_log_file, "\r\n", FILE_APPEND|LOCK_EX);
}
?>