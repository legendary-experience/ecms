<?php
if(!defined('eCMS')) die('Hacking attempt...');

// NEEDED MODULE-VARIALES
$m_title		= '';

// VARIABLES
$error			= false;
$errorString	= '';
$successString	= '';

if($this->auth->checkPermission('xxx') === true) {
	
	
	$m_title = $this->setPageTitle($m_title);
	
	$tpl = 'xxx';
} else {
	$m_title = $this->setPageTitle('Error');
	$errorString = 'Nicht genug Rechte oder nicht angemeldet!';
	
	$tpl = '_error';
}

$this->assign('pageTitle',		$m_title);
$this->assign('error',			$error);
$this->assign('errorString',	$errorString);
$this->assign('successString',	$successString);

$this->display($tpl);
?>