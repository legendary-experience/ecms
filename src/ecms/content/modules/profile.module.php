<?php
if(!defined('eCMS')) die('Hacking attempt...');

// NEEDED MODULE-VARIALES
$m_title		= 'Profile';

// VARIABLES
$error			= false;
$errorString	= '';
$successString	= '';

$jsString				= '';
$profile_display		= false;

$user_uid				= '';
$user_username			= '';
$user_email				= '';
$user_registered		= '';
$user_activated			= '';
$user_disabled			= '';
$user_last_login		= '';
$user_last_action		= '';
$user_seo				= '';

$profile_first_name		= '';
$profile_second_name	= '';
$profile_sur_name		= '';
$profile_userpic		= '';
$profile_online			= '';
$profile_b_day			= '';
$profile_b_month		= '';
$profile_b_year			= '';
$profile_gender			= '';
$profile_nationality	= '';
$profile_location		= '';
$profile_profession		= '';
$profile_icq			= '';
$profile_msn			= '';
$profile_aim			= '';
$profile_yim			= '';
$profile_skype			= '';
$profile_steam			= '';
$profile_facebook		= '';
$profile_twitter		= '';
$profile_google_plus	= '';
$profile_wkw			= '';
$profile_youtube		= '';
$profile_myspace		= '';
$profile_signature		= '';
$profile_web_title		= '';
$profile_web_uri		= '';
$profile_slogan			= '';
$profile_about			= '';
$profile_relationship	= '';

$profile_fav_radio		= '';
$profile_fav_dj			= '';
$profile_fav_club		= '';
$profile_fav_genre		= '';
$profile_fav_disco		= '';
$profile_fav_song		= '';
$profile_fav_movie		= '';
$profile_fav_series		= '';

$profile_board_posts	= '';
$profile_profile_edited	= '';

$online_string			= '';
$age					= '';
$gender_string			= '';
$nationality_string		= '';
$web_title				= '';
$web_uri				= '';

$loggedIn				= false;
$ownUID					= 0;

$user					= array();
$profile				= array();
$countries				= array();
$relationships			= array();

$fListed				= false;
$fAdded					= false;
$fFriends				= false;
$fBlocked				= false;
$canSendPM				= false;

$canViewInfo			= true;
$canViewWall			= false;
$canViewGuestbook		= false;
$canViewFriends			= false;
$canViewGroups			= false;
$canViewEvents			= false;
$canViewLastVisitors	= false;
$canViewMedia			= false;

// First, we needs to know, if the user is logged in.
if($this->security->checkLogin() === true) {
	$loggedIn = true;
	$ownUID = $_SESSION['eCMSuserUID'];
}

if($this->auth->checkPermission('profile') === true) {
	// We have access to the profile.
	
	// Before we start with collecting data, we needs to analyze the url-params.
	// The url can have the following cases:
	// Profile-Output:
	//  - http://www.tld.com/profile/
	//  - http://www.tld.com/profile/UID/            or   http://www.tld.com/profile/SEO/
	//  - http://www.tld.com/profile/UID/AREA/       or   http://www.tld.com/profile/SEO/AREA/
	//  - http://www.tld.com/profile/UID/AREA/.../   or   http://www.tld.com/profile/SEO/AREA/.../
	// Other-Outputs:
	//  - http://www.tld.com/profile/edit/.../
	//  - http://www.tld.com/profile/.../.../
	// 
	// Ok, we know, what is possible.
	// But we needs to know it in length-groups.
	// What we have? No idea? Check the following text.
	//  - Length: 1
	//     - In this case, we have only ONE option and ONE check. Yeah, its the easiest one of all (length 3 is the hardest because of extreme range of possible options and much chances to get fail -.-). ;)
	//        - Check needed: Login
	//        - Options: View/Error
	
	//  - Length: 2
	//     - Little bit harder, but it's possible to control with only 2 general options and 1 switch-loop in every option.
	//     - What we have? We have the profile-output with UID or SEO (we can give the function both, it will comes out the profile, if possible, in both cases).
	//     - On the other hand, we have the edit, etc. options.
	//     - How to check in the easiest way?
	//     - Check for profile-exist. If not exist, we start a switch-loop to check the other possible options on position 1 (starts with 0).
	//     - In the switch-loop-default, we put out the normal permission-error.
	//     - But we have forgotten 1 option. We can check, if we have an integer or string-value. With the integer-value, we are save, that we have 100% user-profile.
	
	//  - Length: 3 or higher
	//     - Now the hardest. It will come later.
	if(count($this->urlParams) === 1) {
		// Check, if the user is logged in.
		// If not, he can't view his own profile ;)
		if($this->security->checkLogin() === true) {
			// If user is logged in, we give out the own profile.
			$user				= $this->user->getUser();
			$profile			= $this->user->getProfile();
			$countries			= $this->user->getCountry();
			
			$canViewInfo			= true;
			$canViewWall			= true;
			$canViewGuestbook		= true;
			$canViewFriends			= true;
			$canViewGroups			= true;
			$canViewEvents			= true;
			$canViewLastVisitors	= true;
			$canViewMedia			= true;
			
			$profile_display = true;
			$m_title = $this->setPageTitle($user['username']);
			
			$tpl = 'profile';
			$jsString = '<script type="text/javascript">showProfileDiv(\'info\')</script>';
		} else {
			$m_title = $this->setPageTitle('Error');
			$errorString = 'Nicht genug Rechte oder nicht angemeldet!';
			
			$tpl = '_error';
		}
	} elseif(count($this->urlParams) === 2) {
		// Now we have 2 general options.
		// 1st the profile.
		// 2nd the rest. ;)
		// But we check first the rest. The profile comes in the default-value as own check. ;)
		switch($this->urlParams[1]) {
			case 'edit':
				
				break;
			
			
			
			default:
				// Now we check, if our param is an UID or SEOtag.
				// If not, we have a hack-try or something like that.
				// Abort with an error and we are finish. ;)
				if($this->user->getUser($this->urlParams[1]) !== false) {
					$user = $this->user->getUser($this->urlParams[1]);
					$profile = $this->user->getProfile($this->urlParams[1]);
					
					
					
					if($this->security->checkLogin() === true && $user['uid'] === $_SESSION['eCMSuserUID']) {
						$canViewInfo			= true;
						$canViewWall			= true;
						$canViewGuestbook		= true;
						$canViewFriends			= true;
						$canViewGroups			= true;
						$canViewEvents			= true;
						$canViewLastVisitors	= true;
						$canViewMedia			= true;
					}
					
					$profile_display = true;
					$m_title = $this->setPageTitle($user['username']);
					
					$tpl = 'profile';
					$jsString = '<script type="text/javascript">showProfileDiv(\'info\')</script>';
				} else {
					$m_title = $this->setPageTitle('Error');
					$errorString = 'Gesuchte Person existiert nicht!';
					
					$tpl = '_error';
				}
		}
	} elseif(count($this->urlParams) >= 3) {
		$m_title = $this->setPageTitle('Error');
		$errorString = 'Nicht genug Rechte oder nicht angemeldet!';
		
		$tpl = '_error';
	}
} else {
	$m_title = $this->setPageTitle('Error');
	$errorString = 'Nicht genug Rechte oder nicht angemeldet!';
	
	$tpl = '_error';
}



// Now we can collect all data.
// Let us start with all important data-arrays. The sorting, etc. will come later.
if($profile_display === true) {
	$user_uid				= $user['uid'];
	$user_username			= $user['username'];
	$user_email				= $user['email'];
	$user_registered		= $user['registered'];
	$user_activated			= $user['activated'];
	$user_disabled			= $user['disabled'];
	$user_last_login		= $user['last_login'];
	$user_last_action		= $user['last_action'];
	$user_seo				= $user['seo'];
	
	$profile_first_name		= $profile['first_name'];
	$profile_second_name	= $profile['second_name'];
	$profile_sur_name		= $profile['sur_name'];
	$profile_userpic		= $profile['userpic'];
	$profile_online			= $profile['online'];
	$profile_b_day			= $profile['b_day'];
	$profile_b_month		= $profile['b_month'];
	$profile_b_year			= $profile['b_year'];
	$profile_gender			= $profile['gender'];
	$profile_nationality	= $profile['nationality'];
	$profile_location		= $profile['location'];
	$profile_profession		= $profile['profession'];
	$profile_icq			= $profile['icq'];
	$profile_msn			= $profile['msn'];
	$profile_aim			= $profile['aim'];
	$profile_yim			= $profile['yim'];
	$profile_skype			= $profile['skype'];
	$profile_steam			= $profile['steam'];
	$profile_facebook		= $profile['facebook'];
	$profile_twitter		= $profile['twitter'];
	$profile_google_plus	= $profile['google_plus'];
	$profile_wkw			= $profile['wkw'];
	$profile_youtube		= $profile['youtube'];
	$profile_myspace		= $profile['myspace'];
	$profile_signature		= $profile['signature'];
	$profile_web_title		= $profile['web_title'];
	$profile_web_uri		= $profile['web_uri'];
	$profile_slogan			= $profile['slogan'];
	$profile_about			= $profile['about'];
	$profile_relationship	= $profile['relationship'];
	
	$profile_fav_radio		= $profile['fav_radio'];
	$profile_fav_dj			= $profile['fav_dj'];
	$profile_fav_club		= $profile['fav_club'];
	$profile_fav_genre		= $profile['fav_genre'];
	$profile_fav_disco		= $profile['fav_disco'];
	$profile_fav_song		= $profile['fav_song'];
	$profile_fav_movie		= $profile['fav_movie'];
	$profile_fav_series		= $profile['fav_series'];
	
	$profile_board_posts	= $profile['board_posts'];
	$profile_profile_edited	= $profile['profile_edited'];
	
	
	
	// Now generate the other things, that are not direct defined in the user-/profile-table.
	// Online-String
	if($profile['online'] === '0') $online_string = 'Offline';
	if($profile['online'] === '1') $online_string = 'Online';
	// Generate the age from the birthday.
	if($profile['b_year'] !== '') $age = (date('Y') - $profile['b_year']) - intval(date('md') < sprintf('%02d%02d' , $profile['b_month'], $profile['b_day']));
	// Gender-String
	if($profile['gender'] === '1') $gender_string = 'M&auml;nnlich';
	if($profile['gender'] === '0') $gender_string = 'Weiblich';
	// Nationality-String
	if($profile['nationality'] !== '') $country = $this->user->getCountry($profile['nationality']);
	if($profile['nationality'] != '') $nationality_string = $country[0]['country'];
	
	
	
	// At least, we need the friend-checks, etc ;)
	if($this->user->checkFriend($user['uid']) === true) $fListed = true;
	if($this->user->getFriendState($user['uid']) === '1') $fFriends = true;
	if($this->user->checkAdd($user['uid']) === true) $fAdded = true;
}



$this->assign('pageTitle',				$m_title);
$this->assign('error',					$error);
$this->assign('errorString',			$errorString);
$this->assign('successString',			$successString);

$this->assign('jsString',				$jsString);

$this->assign('user_uid',				$user_uid);
$this->assign('user_username',			$user_username);
$this->assign('user_email',				$user_email);
$this->assign('user_registered',		$user_registered);
$this->assign('user_activated',			$user_activated);
$this->assign('user_disabled',			$user_disabled);
$this->assign('user_last_login',		$user_last_login);
$this->assign('user_last_action',		$user_last_action);
$this->assign('user_seo',				$user_seo);

$this->assign('profile_first_name',		$profile_first_name);
$this->assign('profile_second_name',	$profile_second_name);
$this->assign('profile_sur_name',		$profile_sur_name);
$this->assign('profile_userpic',		$profile_userpic);
$this->assign('profile_online',			$profile_online);
$this->assign('profile_b_day',			$profile_b_day);
$this->assign('profile_b_month',		$profile_b_month);
$this->assign('profile_b_year',			$profile_b_year);
$this->assign('profile_gender',			$profile_gender);
$this->assign('profile_nationality',	$profile_nationality);
$this->assign('profile_location',		$profile_location);
$this->assign('profile_profession',		$profile_profession);
$this->assign('profile_icq',			$profile_icq);
$this->assign('profile_msn',			$profile_msn);
$this->assign('profile_aim',			$profile_aim);
$this->assign('profile_yim',			$profile_yim);
$this->assign('profile_skype',			$profile_skype);
$this->assign('profile_steam',			$profile_steam);
$this->assign('profile_facebook',		$profile_facebook);
$this->assign('profile_twitter',		$profile_twitter);
$this->assign('profile_google_plus',	$profile_google_plus);
$this->assign('profile_wkw',			$profile_wkw);
$this->assign('profile_youtube',		$profile_youtube);
$this->assign('profile_myspace',		$profile_myspace);
$this->assign('profile_signature',		$profile_signature);
$this->assign('profile_web_title',		$profile_web_title);
$this->assign('profile_web_uri',		$profile_web_uri);
$this->assign('profile_slogan',			$profile_slogan);
$this->assign('profile_about',			$profile_about);
$this->assign('profile_relationship',	$profile_relationship);

$this->assign('profile_fav_radio',		$profile_fav_radio);
$this->assign('profile_fav_dj',			$profile_fav_dj);
$this->assign('profile_fav_club',		$profile_fav_club);
$this->assign('profile_fav_genre',		$profile_fav_genre);
$this->assign('profile_fav_disco',		$profile_fav_disco);
$this->assign('profile_fav_song',		$profile_fav_song);
$this->assign('profile_fav_movie',		$profile_fav_movie);
$this->assign('profile_fav_series',		$profile_fav_series);

$this->assign('profile_board_posts',	$profile_board_posts);
$this->assign('profile_profile_edited',	$profile_profile_edited);

$this->assign('online_string',			$online_string);
$this->assign('age',					$age);
$this->assign('gender_string',			$gender_string);
$this->assign('nationality_string',		$nationality_string);

$this->assign('loggedIn',				$loggedIn);
$this->assign('ownUID',					$ownUID);

$this->assign('user',					$user);
$this->assign('profile',				$profile);
$this->assign('countries',				$countries);
$this->assign('relationships',			$relationships);

$this->assign('fListed',				$fListed);
$this->assign('fAdded',					$fAdded);
$this->assign('fFriends',				$fFriends);
$this->assign('fBlocked',				$fBlocked);
$this->assign('canSendPM',				$canSendPM);

$this->assign('canViewInfo',			$canViewInfo);
$this->assign('canViewWall',			$canViewWall);
$this->assign('canViewGuestbook',		$canViewGuestbook);
$this->assign('canViewFriends',			$canViewFriends);
$this->assign('canViewGroups',			$canViewGroups);
$this->assign('canViewEvents',			$canViewEvents);
$this->assign('canViewLastVisitors',	$canViewLastVisitors);
$this->assign('canViewMedia',			$canViewMedia);

$this->display($tpl);
?>