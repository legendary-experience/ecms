<?php
if(!defined('eCMS')) die('Hacking attempt...');

// NEEDED MODULE-VARIALES
$m_title		= $this->setPageTitle();

// VARIABLES
$error			= false;
$errorString	= '';
$successString	= '';

$m_title = $this->setPageTitle($m_title);

$this->assign('pageTitle',		$m_title);
$this->assign('error',			$error);
$this->assign('errorString',	$errorString);
$this->assign('successString',	$successString);

$this->display('index');
?>