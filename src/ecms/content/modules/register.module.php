<?php
if(!defined('eCMS')) die('Hacking attempt...');

// NEEDED MODULE-VARIALES
$m_title				= 'Registration';

// VARIABLES
$error					= false;
$errorString			= '';
$successString			= '';

$user_uid				= '';
$user_username			= '';
$user_email				= '';
$user_registered		= '';
$user_activated			= '';
$user_disabled			= '';
$user_last_login		= '';
$user_last_action		= '';
$user_seo				= '';

$profile_first_name		= '';
$profile_second_name	= '';
$profile_sur_name		= '';
$profile_userpic		= '';
$profile_online			= '';
$profile_b_day			= '';
$profile_b_month		= '';
$profile_b_year			= '';
$profile_gender			= '';
$profile_nationality	= '';
$profile_location		= '';
$profile_profession		= '';
$profile_icq			= '';
$profile_msn			= '';
$profile_aim			= '';
$profile_yim			= '';
$profile_skype			= '';
$profile_steam			= '';
$profile_facebook		= '';
$profile_twitter		= '';
$profile_google_plus	= '';
$profile_wkw			= '';
$profile_youtube		= '';
$profile_myspace		= '';
$profile_signature		= '';
$profile_web_title		= '';
$profile_web_uri		= '';

$online_string			= '';
$age					= '';
$gender_string			= '';
$nationality_string		= '';

$fListed				= false;
$fAdded					= false;
$fFriends				= false;
$fBlocked				= false;
$canSendPM				= false;

$passwordVal			= '';
$password2Val			= '';

$secretQuestionVal		= '';
$secretAnswerVal		= '';

$countries				= array();

$days					= array();
$months					= array();
$years					= array();

$regStep				= '1';
$maxStep				= '8';

$firstNameError			= false;
$surNameError			= false;
$nickNameError			= false;
$emailError				= false;
$passwordError			= false;
$password2Error			= false;
$secretQuestionError	= false;
$secretAnswerError		= false;
$bDayError				= false;
$bMonthError			= false;
$bYearError				= false;
$genderError			= false;
$nationalityError		= false;
$locationError			= false;
$professionError		= false;
$picError				= false;
$webTitleError			= false;
$webUriError			= false;
$icqError				= false;
$msnError				= false;
$aimError				= false;
$yimError				= false;
$skypeError				= false;
$steamError				= false;
$facebookError			= false;
$twitterError			= false;
$wkwError				= false;
$youtubeError			= false;
$myspaceError			= false;

if($this->auth->checkPermission('register') === true) {
	if($this->security->checkLogin() == true) header('LOCATION: '.GENERAL_PAGE_URI.'profile/');
	else {
		if(isset($_SESSION['eCMSregStep']) && $_SESSION['eCMSregStep'] != '' && $_SESSION['eCMSregStep'] <= $maxStep) $regStep = $_SESSION['eCMSregStep'];
		else $_SESSION['eCMSregStep'] = '1';
		
		// Examination of the various required fields
		// All UIDs, usernames, emails and salts for comparison from the database fetch.
		$uids = array();
		$usernames = array();
		$emails = array();
		$salts = array();
		
		$countries = $this->user->getCountry();
		
		for($i = 1; $i <= 31; $i++) {
			$days[$i] = $i;
		}
		$months[1] = array('id' => 1, 'title' => 'Januar');
		$months[2] = array('id' => 2, 'title' => 'Februar');
		$months[3] = array('id' => 3, 'title' => 'M&auml;rz');
		$months[4] = array('id' => 4, 'title' => 'April');
		$months[5] = array('id' => 5, 'title' => 'Mai');
		$months[6] = array('id' => 6, 'title' => 'Juni');
		$months[7] = array('id' => 7, 'title' => 'Juli');
		$months[8] = array('id' => 8, 'title' => 'August');
		$months[9] = array('id' => 9, 'title' => 'September');
		$months[10] = array('id' => 10, 'title' => 'Oktober');
		$months[11] = array('id' => 11, 'title' => 'November');
		$months[12] = array('id' => 12, 'title' => 'Dezember');
		$nYear = date('Y', time());
		for($i = $nYear; $i >= 1900; $i--) {
			$years[] = $i;
		}
		
		$row = $this->db->getArray("SELECT uid, username, email, salt FROM {$this->db_prefix}user");
		for($i = 0; $i < count($row); $i++) {
			$uids[] = $row[$i]['uid'];
			$usernames[] = $row[$i]['username'];
			$emails[] = $row[$i]['email'];
			$salts[] = $row[$i]['salt'];
		}
		
		// Demo-Profile
		$dUser					= $this->user->getUser('1234567');
		$dProfile				= $this->user->getProfile('1234567');
		
		$user_uid				= $dUser['uid'];
		$user_username			= $dUser['username'];
		// $user_email				= $dUser['email'];
		$user_registered		= $dUser['registered'];
		$user_last_login		= $dUser['last_login'];
		$user_last_action		= $dUser['last_action'];
		$user_seo				= $dUser['seo'];
		
		$profile_first_name		= $dProfile['first_name'];
		$profile_sur_name		= $dProfile['sur_name'];
		$profile_userpic		= $dProfile['userpic'];
		$profile_online			= $dProfile['online'];
		// $profile_b_day			= $dProfile['b_day'];
		// $profile_b_month		= $dProfile['b_month'];
		// $profile_b_year			= $dProfile['b_year'];
		$profile_gender			= $dProfile['gender'];
		$profile_nationality	= $dProfile['nationality'];
		$profile_location		= $dProfile['location'];
		$profile_profession		= $dProfile['profession'];
		$profile_icq			= $dProfile['icq'];
		$profile_msn			= $dProfile['msn'];
		$profile_aim			= $dProfile['aim'];
		$profile_yim			= $dProfile['yim'];
		$profile_skype			= $dProfile['skype'];
		$profile_steam			= $dProfile['steam'];
		$profile_facebook		= $dProfile['facebook'];
		$profile_twitter		= $dProfile['twitter'];
		$profile_wkw			= $dProfile['wkw'];
		$profile_youtube		= $dProfile['youtube'];
		$profile_myspace		= $dProfile['myspace'];
		$profile_signature		= $dProfile['signature'];
		$profile_web_title		= $dProfile['web_title'];
		$profile_web_uri		= $dProfile['web_uri'];
		
		$country = $this->user->getCountry($profile_nationality);
		
		$nationality_string = $country[0]['country'];
		if($profile_online === '0') $online_string = 'Offline';
		if($profile_online === '1') $online_string = 'Online';
		if($profile_gender === '0')	$gender_string = 'M&auml;nnlich';
		if($profile_gender === '1')	$gender_string = 'Weiblich';
		$age = '';
		
		if(isset($_SESSION['eCMSregNickName']))			$user_username			= $_SESSION['eCMSregNickName'];
		if(isset($_SESSION['eCMSregEmail']))			$user_email				= $_SESSION['eCMSregEmail'];
		
		if(isset($_SESSION['eCMSregFirstName']))		$profile_first_name		= $_SESSION['eCMSregFirstName'];
		if(isset($_SESSION['eCMSregSurName']))			$profile_sur_name		= $_SESSION['eCMSregSurName'];
		
		if(isset($_SESSION['eCMSregBday']))				$profile_b_day			= $_SESSION['eCMSregBday'];
		if(isset($_SESSION['eCMSregBmonth']))			$profile_b_month		= $_SESSION['eCMSregBmonth'];
		if(isset($_SESSION['eCMSregByear']))			$profile_b_year			= $_SESSION['eCMSregByear'];
		if(isset($_SESSION['eCMSregGender']))			$profile_gender			= $_SESSION['eCMSregGender'];
		if(isset($_SESSION['eCMSregNationality']))		$profile_nationality	= $_SESSION['eCMSregNationality'];
		if(isset($_SESSION['eCMSregLocation']))			$profile_location		= $_SESSION['eCMSregLocation'];
		if(isset($_SESSION['eCMSregProfession']))		$profile_profession		= $_SESSION['eCMSregProfession'];
		if(isset($_SESSION['eCMSregPic']))				$profile_userpic		= $_SESSION['eCMSregPic'];
		if(isset($_SESSION['eCMSregWebTitle']))			$profile_web_title		= $_SESSION['eCMSregWebTitle'];
		if(isset($_SESSION['eCMSregWebUri']))			$profile_web_uri		= $_SESSION['eCMSregWebUri'];
		if(isset($_SESSION['eCMSregICQ']))				$profile_icq			= $_SESSION['eCMSregICQ'];
		if(isset($_SESSION['eCMSregMSN']))				$profile_msn			= $_SESSION['eCMSregMSN'];
		if(isset($_SESSION['eCMSregAIM']))				$profile_aim			= $_SESSION['eCMSregAIM'];
		if(isset($_SESSION['eCMSregYIM']))				$profile_yim			= $_SESSION['eCMSregYIM'];
		if(isset($_SESSION['eCMSregSkype']))			$profile_skype			= $_SESSION['eCMSregSkype'];
		if(isset($_SESSION['eCMSregSteam']))			$profile_steam			= $_SESSION['eCMSregSteam'];
		if(isset($_SESSION['eCMSregFacebook']))			$profile_facebook		= $_SESSION['eCMSregFacebook'];
		if(isset($_SESSION['eCMSregTwitter']))			$profile_twitter		= $_SESSION['eCMSregTwitter'];
		if(isset($_SESSION['eCMSregWKW']))				$profile_wkw			= $_SESSION['eCMSregWKW'];
		if(isset($_SESSION['eCMSregYoutube']))			$profile_youtube		= $_SESSION['eCMSregYoutube'];
		if(isset($_SESSION['eCMSregMyspace']))			$profile_myspace		= $_SESSION['eCMSregMyspace'];
		
		if(isset($_SESSION['eCMSregPassword']))			$passwordVal			= $_SESSION['eCMSregPassword'];
		if(isset($_SESSION['eCMSregPassword2']))		$password2Val			= $_SESSION['eCMSregPassword2'];
		if(isset($_SESSION['eCMSregSecretQuestion']))	$secretQuestionVal		= $_SESSION['eCMSregSecretQuestion'];
		if(isset($_SESSION['eCMSregSecretAnswer']))		$secretAnswerVal		= $_SESSION['eCMSregSecretAnswer'];
		
		// Switch the registration-steps
		switch($regStep) {
			// STEP 1
			case '1':
				if(isset($_POST['next'])) {
					$_SESSION['eCMSregFirstName']	= $_POST['firstName'];
					$_SESSION['eCMSregSurName']		= $_POST['surName'];
					$profile_first_name				= $_SESSION['eCMSregFirstName'];
					$profile_sur_name				= $_SESSION['eCMSregSurName'];
					
					$_SESSION['eCMSregStep']		= $_SESSION['eCMSregStep'] + '1';
					
					header('LOCATION: '.GENERAL_PAGE_URI.'register/');
				}
				
				break;
			
			// STEP 2
			case '2':
				if(isset($_POST['prev'])) {
					$_SESSION['eCMSregStep'] = $_SESSION['eCMSregStep'] - '1';
					
					header('LOCATION: '.GENERAL_PAGE_URI.'register/');
				}
				
				if(isset($_POST['next'])) {
					$_SESSION['eCMSregNickName']	= $_POST['nickName'];
					$user_username					= $_SESSION['eCMSregNickName'];
					
					// Checks if the username is already taken.
					if(in_array(trim($_POST['nickName']), $usernames)) {
						$errorString = "Dieser Benutzername wird bereits benutzt!";
						$nickNameError = true;
						$error = true;
					}
					
					if($error === false) {
						$_SESSION['eCMSregStep'] = $_SESSION['eCMSregStep'] + '1';
						
						header('LOCATION: '.GENERAL_PAGE_URI.'register/');
					}
				}
				
				break;
			
			// STEP 3
			case '3':
				if(isset($_POST['prev'])) {
					$_SESSION['eCMSregStep'] = $_SESSION['eCMSregStep'] - '1';
					
					header('LOCATION: '.GENERAL_PAGE_URI.'register/');
				}
				
				if(isset($_POST['next'])) {
					$_SESSION['eCMSregEmail'] 		= $_POST['email'];
					$user_email						= $_SESSION['eCMSregEmail'];
					
					if(!preg_match('�^[\w\.-]+@[\w\.-]+\.[\w]{2,4}$�', trim($_POST['email']))) { // Checks whether the e-mail address is valid.
						$errorString = 'Deine E-Mail hat kein g�ltiges Format.';
						$emailError = true;
						$error = true;
					} elseif(in_array(trim($_POST['email']), $emails)) { // Checks if the email is already taken.
						$errorString = "Diese E-Mail ist bereits in Benutzung.";
						$emailError = true;
						$error = true;
					}
					
					if($error === false) {
						$_SESSION['eCMSregStep'] = $_SESSION['eCMSregStep'] + '1';
						
						header('LOCATION: '.GENERAL_PAGE_URI.'register/');
					}
				}
				
				break;
			
			// STEP 4
			case '4':
				if(isset($_POST['prev'])) {
					$_SESSION['eCMSregStep'] = $_SESSION['eCMSregStep'] - '1';
					
					header('LOCATION: '.GENERAL_PAGE_URI.'register/');
				}
				
				if(isset($_POST['next'])) {
					$_SESSION['eCMSregPassword'] 	= $_POST['password'];
					$_SESSION['eCMSregPassword2'] 	= $_POST['password2'];
					$passwordVal					= $_SESSION['eCMSregPassword'];
					$password2Val					= $_SESSION['eCMSregPassword2'];
					
					if(strlen(trim($_POST['password'])) < 6) { // Checks if the password contains at least 6 characters.
						$errorString = 'Dein Passwort muss mindestens 6 Zeichen lang sein.';
						$passwordError = true;
						$error = true;
					}
					if(trim($_POST['password']) != trim($_POST['password2'])) { // Checks if the passwords are the same.
						$errorString = 'Deine Passw&ouml;rter stimmen nicht &uuml;berein.';
						$password2Error = true;
						$error = true;
					}
					
					if($error === false) {
						$_SESSION['eCMSregStep'] = $_SESSION['eCMSregStep'] + '1';
						
						header('LOCATION: '.GENERAL_PAGE_URI.'register/');
					}
				}
				
				break;
			
			// STEP 5
			case '5':
				if(isset($_POST['prev'])) {
					$_SESSION['eCMSregStep'] = $_SESSION['eCMSregStep'] - '1';
					
					header('LOCATION: '.GENERAL_PAGE_URI.'register/');
				}
				
				if(isset($_POST['next'])) {
					$_SESSION['eCMSregSecretQuestion'] 	= $_POST['secretQuestion'];
					$_SESSION['eCMSregSecretAnswer'] 	= $_POST['secretAnswer'];
					$secretQuestionVal					= $_SESSION['eCMSregSecretQuestion'];
					$secretAnswerVal					= $_SESSION['eCMSregSecretAnswer'];
					
					if(trim($_POST['secretQuestion']) == '') {$secretQuestionError = true; $error = true;} // Checks if question was entered.
					if(trim($_POST['secretAnswer']) == '') {$secretAnswerError = true; $error = true;} // Checks if answer was entered.
					
					if($error === false) {
						$_SESSION['eCMSregStep'] = $_SESSION['eCMSregStep'] + '1';
						
						header('LOCATION: '.GENERAL_PAGE_URI.'register/');
					}
				}
				
				break;
			
			// STEP 6
			case '6':
				if(isset($_POST['prev'])) {
					$_SESSION['eCMSregStep'] = $_SESSION['eCMSregStep'] - '1';
					
					header('LOCATION: '.GENERAL_PAGE_URI.'register/');
				}
				
				if(isset($_POST['next'])) {
					$_SESSION['eCMSregBday']			= $_POST['bDay'];
					$_SESSION['eCMSregBmonth']			= $_POST['bMonth'];
					$_SESSION['eCMSregByear']			= $_POST['bYear'];
					$_SESSION['eCMSregGender']			= $_POST['gender'];
					$_SESSION['eCMSregNationality']		= $_POST['nationality'];
					$_SESSION['eCMSregLocation']		= $_POST['location'];
					$_SESSION['eCMSregProfession']		= $_POST['profession'];
					$_SESSION['eCMSregWebTitle']		= $_POST['webTitle'];
					$_SESSION['eCMSregWebUri']			= $_POST['webUri'];
					$_SESSION['eCMSregICQ']				= $_POST['icq'];
					$_SESSION['eCMSregMSN']				= $_POST['msn'];
					$_SESSION['eCMSregAIM']				= $_POST['aim'];
					$_SESSION['eCMSregYIM']				= $_POST['yim'];
					$_SESSION['eCMSregSkype']			= $_POST['skype'];
					$_SESSION['eCMSregSteam']			= $_POST['steam'];
					$_SESSION['eCMSregFacebook']		= $_POST['facebook'];
					$_SESSION['eCMSregTwitter']			= $_POST['twitter'];
					$_SESSION['eCMSregWKW']				= $_POST['wkw'];
					$_SESSION['eCMSregYoutube']			= $_POST['youtube'];
					$_SESSION['eCMSregMyspace']			= $_POST['myspace'];
					
					$profile_b_day						= $_SESSION['eCMSregBday'];
					$profile_b_month					= $_SESSION['eCMSregBmonth'];
					$profile_b_year						= $_SESSION['eCMSregByear'];
					$profile_gender						= $_SESSION['eCMSregGender'];
					$profile_nationality				= $_SESSION['eCMSreg'];
					$profile_location					= $_SESSION['eCMSregLocation'];
					$profile_profession					= $_SESSION['eCMSregProfession'];
					$profile_icq						= $_SESSION['eCMSregICQ'];
					$profile_msn						= $_SESSION['eCMSregMSN'];
					$profile_aim						= $_SESSION['eCMSregAIM'];
					$profile_yim						= $_SESSION['eCMSregYIM'];
					$profile_skype						= $_SESSION['eCMSregSkype'];
					$profile_steam						= $_SESSION['eCMSregSteam'];
					$profile_facebook					= $_SESSION['eCMSregFacebook'];
					$profile_twitter					= $_SESSION['eCMSregTwitter'];
					$profile_wkw						= $_SESSION['eCMSregWKW'];
					$profile_youtube					= $_SESSION['eCMSregYoutube'];
					$profile_myspace					= $_SESSION['eCMSregMyspace'];
					$profile_web_title					= $_SESSION['eCMSregWebTitle'];
					$profile_web_uri					= $_SESSION['eCMSregWebUri'];
					
					if(trim($_POST['location']) == '') {$locationError = true; $error = true;} // Checks if question was entered.
					if(trim($_POST['profession']) == '') {$professionError = true; $error = true;} // Checks if question was entered.
					
					if($error === false) {
						$_SESSION['eCMSregStep'] = $_SESSION['eCMSregStep'] + '1';
						
						header('LOCATION: '.GENERAL_PAGE_URI.'register/');
					}
				}
				
				break;
			
			// STEP 7
			case '7':
				if(isset($_POST['prev'])) {
					$_SESSION['eCMSregStep'] = $_SESSION['eCMSregStep'] - '1';
					
					header('LOCATION: '.GENERAL_PAGE_URI.'register/');
				}
				
				if(isset($_POST['next'])) {
					// Secure out the password.
					$password = $this->db->secureString((string)$passwordVal);
					
					// Now the last things...
					$user_uid = $this->security->register(1, $user_username, $password, $user_email, $secretQuestionVal, $secretAnswerVal, $profile_first_name, $profile_sur_name, (int)$profile_b_day, (int)$profile_b_month, (int)$profile_b_year, (int)$profile_gender, $profile_nationality, $profile_location, $profile_profession);
					$this->user->updateProfile($user_uid, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, $profile_icq, $profile_msn, $profile_aim, $profile_yim, $profile_skype, $profile_steam, $profile_facebook, $profile_twitter, $profile_wkw, $profile_youtube, $profile_myspace, NULL);
					
					$_SESSION['eCMSregStep'] = $_SESSION['eCMSregStep'] + '1';
					
					header('LOCATION: '.GENERAL_PAGE_URI.'register/');
				}
				
				break;
			
			// STEP 8
			case '8':
				$_SESSION['eCMSregFirstName']		= '';
				$_SESSION['eCMSregSurName']			= '';
				$_SESSION['eCMSregNickName']		= '';
				$_SESSION['eCMSregEmail']			= '';
				$_SESSION['eCMSregPassword']		= '';
				$_SESSION['eCMSregPassword2']		= '';
				$_SESSION['eCMSregSecretQuestion']	= '';
				$_SESSION['eCMSregSecretAnswer']	= '';
				$_SESSION['eCMSregBday']			= '';
				$_SESSION['eCMSregBmonth']			= '';
				$_SESSION['eCMSregByear']			= '';
				$_SESSION['eCMSregGender']			= '';
				$_SESSION['eCMSregNationality']		= '';
				$_SESSION['eCMSregLocation']		= '';
				$_SESSION['eCMSregProfession']		= '';
				$_SESSION['eCMSregPic']				= '';
				$_SESSION['eCMSregWebTitle']		= '';
				$_SESSION['eCMSregWebUri']			= '';
				$_SESSION['eCMSregICQ']				= '';
				$_SESSION['eCMSregMSN']				= '';
				$_SESSION['eCMSregAIM']				= '';
				$_SESSION['eCMSregYIM']				= '';
				$_SESSION['eCMSregSkype']			= '';
				$_SESSION['eCMSregSteam']			= '';
				$_SESSION['eCMSregFacebook']		= '';
				$_SESSION['eCMSregTwitter']			= '';
				$_SESSION['eCMSregWKW']				= '';
				$_SESSION['eCMSregYoutube']			= '';
				$_SESSION['eCMSregMyspace']			= '';
				
				$_SESSION['eCMSregStep'] 			= 1;
				
				break;
		}
		
		$m_title = $this->setPageTitle($m_title);
		
		$tpl = 'register';
	}
} else {
	$m_title = $this->setPageTitle('Error');
	$errorString = 'Nicht genug Rechte oder nicht angemeldet!';
	
	$tpl = '_error';
}

$this->assign('pageTitle',					$m_title);
$this->assign('error',						$error);
$this->assign('errorString',				$errorString);
$this->assign('successString',				$successString);

$this->assign('user_uid',					$user_uid);
$this->assign('user_username',				$user_username);
$this->assign('user_email',					$user_email);
$this->assign('user_registered',			$user_registered);
$this->assign('user_activated',				$user_activated);
$this->assign('user_disabled',				$user_disabled);
$this->assign('user_last_login',			$user_last_login);
$this->assign('user_last_action',			$user_last_action);
$this->assign('user_seo',					$user_seo);

$this->assign('profile_first_name',			$profile_first_name);
$this->assign('profile_second_name',		$profile_second_name);
$this->assign('profile_sur_name',			$profile_sur_name);
$this->assign('profile_userpic',			$profile_userpic);
$this->assign('profile_online',				$profile_online);
$this->assign('profile_b_day',				$profile_b_day);
$this->assign('profile_b_month',			$profile_b_month);
$this->assign('profile_b_year',				$profile_b_year);
$this->assign('profile_gender',				$profile_gender);
$this->assign('profile_nationality',		$profile_nationality);
$this->assign('profile_location',			$profile_location);
$this->assign('profile_profession',			$profile_profession);
$this->assign('profile_icq',				$profile_icq);
$this->assign('profile_msn',				$profile_msn);
$this->assign('profile_aim',				$profile_aim);
$this->assign('profile_yim',				$profile_yim);
$this->assign('profile_skype',				$profile_skype);
$this->assign('profile_steam',				$profile_steam);
$this->assign('profile_facebook',			$profile_facebook);
$this->assign('profile_twitter',			$profile_twitter);
$this->assign('profile_google_plus',		$profile_google_plus);
$this->assign('profile_wkw',				$profile_wkw);
$this->assign('profile_youtube',			$profile_youtube);
$this->assign('profile_myspace',			$profile_myspace);
$this->assign('profile_web_title',			$profile_web_title);
$this->assign('profile_web_uri',			$profile_web_uri);

$this->assign('online_string',				$online_string);
$this->assign('age',						$age);
$this->assign('gender_string',				$gender_string);
$this->assign('nationality_string',			$nationality_string);

$this->assign('fListed',					$fListed);
$this->assign('fAdded',						$fAdded);
$this->assign('fFriends',					$fFriends);
$this->assign('fBlocked',					$fBlocked);
$this->assign('canSendPM',					$canSendPM);

$this->assign('passwordVal',				$passwordVal);
$this->assign('password2Val',				$password2Val);

$this->assign('secretQuestionVal',			$secretQuestionVal);
$this->assign('secretAnswerVal',			$secretAnswerVal);

$this->assign('countries',					$countries);

$this->assign('days',					$days);
$this->assign('months',					$months);
$this->assign('years',					$years);

$this->assign('regStep',					$regStep);
$this->assign('maxStep',					$maxStep);

$this->display($tpl);
?>