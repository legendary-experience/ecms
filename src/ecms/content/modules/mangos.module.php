<?php
if(!defined('eCMS')) die('Hacking attempt...');

// NEEDED MODULE-VARIALES
$m_title		= '';

$realmDB; $charDB; $worldDB;

// VARIABLES
$error			= false;
$errorString	= '';
$successString	= '';

$category		= '';

if($this->auth->checkPermission('mangos') === true) {
	$realmDB = new Database(clone $this, $this->db_settings, 'mysql', 2);
	#$charDB = new Database(clone $this, $this->db_settings, 'mysql', 3);
	$worldDB = new Database(clone $this, $this->db_settings, 'mysql', 4);
	
	if(count($this->urlParams) > 1) {
		$category = $this->urlParams[1];
	}
	
	$m_title = $this->setPageTitle($m_title);
	
	$tpl = 'mangos';
} else {
	$m_title = $this->setPageTitle('Error');
	$errorString = 'Nicht genug Rechte oder nicht angemeldet!';
	
	$tpl = '_error';
}

$this->assign('pageTitle',		$m_title);
$this->assign('error',			$error);
$this->assign('errorString',	$errorString);
$this->assign('successString',	$successString);

$this->assign('category',		$category);

$this->display($tpl);
?>