<!DOCTYPE html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>{$pageTitle}</title>
    <meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
    <link ref="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="icon.png">
    <!-- Place favicon.ico in the root directory -->
	
    <link href="{$templatePath}css/_normalize.css" media="all" rel="stylesheet">
    <link href="{$templatePath}css/_html5.css" media="all" rel="stylesheet">
	<link href="{$templatePath}css/_layout_handheld.css" media="handheld" rel="stylesheet">
	<link href="{$templatePath}css/_layout_projection.css" media="projection" rel="stylesheet">
	<link href="{$templatePath}css/_layout_print.css" media="print" rel="stylesheet">
	<link href="{$templatePath}css/_layout_screen.css" media="screen" rel="stylesheet">
	<link href="{$templatePath}css/_layout_tv.css" media="tv" rel="stylesheet">
	<link href="{$templatePath}css/_html5_extra.css" media="all" rel="stylesheet">
	
	<meta name="theme-color" content="#FAFAFA">
	
	<script src="{$jQueryPath}jquery-1.7.2.min.js"></script>
	<script src="{$templatePath}js/_functions.js"></script>
</head>

<body>
    <!--[if IE]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
    <![endif]-->

    <div id="advt_overlay" class="advt_overlay" style="display:none;">DEFAULT_ADVT</div>

    <div class="pagewidth" id="pagewidth">
        <a name="page_top" id="page_top"></a>
        <header class="header">{block name=header}DEFAULT_HEADER{/block}</header>
        
        <div id="wrapper" class="clearfix">
            <div class="content_left" id="content_left">{block name=content_left}DEFAULT_NAV{/block}</div>
            <div class="content_main" id="content_main">{block name=content_main}DEFAULT_CONTENT{/block}</div>
            <div class="content_right" id="content_right">{block name=content_right}DEFAULT_SIDEBAR{/block}</div>
        </div>
        <div class="clearfix"></div>
        
        <footer class="footer">{block name=footer}DEFAULT_FOOTER{/block}</footer>
    </div>

    <!-- JavaScript at the bottom for fast page loading -->
    <!-- All JavaScript at the bottom, except this Modernizr build.
        Modernizr enables HTML5 elements & feature detects for optimal performance.
        Create your own custom Modernizr build: www.modernizr.com/download/ -->
    <script src="{$modernizrPath}modernizr-2.5.3.min.js"></script>
    <!-- <script src="{$webforms2Path}webforms2-p.js"></script> -->

    <!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID.
        mathiasbynens.be/notes/async-analytics-snippet -->
    {*
    <script>
        window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
        ga('create', 'UA-XXXXX-Y', 'auto'); ga('set','transport','beacon'); ga('send', 'pageview')
    </script>
    <script src="https://www.google-analytics.com/analytics.js" async defer></script>
    *}
</body>