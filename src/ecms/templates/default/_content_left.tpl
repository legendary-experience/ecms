{foreach name=navi_cat key=id item=navi_cat from=$naviArr}
	<div class="navi_head">
		<div class="navi_head_font">&gt;&gt; {$navi_cat.title}</div>
	</div>
	<div class="navi_menu">
		{foreach name=navi_entry key=id item=navi_entry from=$navi_cat.entries}
			{if $navi_entry.link == NULL}
				<a class="a_navi_menu" target="_self">{$navi_entry.title}</a>
			{else}
				{if $navi_entry.link === 'index'}
					<a class="a_navi_menu" target="_self" href="{$pageUri}">{$navi_entry.title}</a>
				{else}
					{if $navi_entry.extern === 'FALSE'}<a class="a_navi_menu" target="_self" href="{$pageUri}{$navi_entry.link}/">{$navi_entry.title}</a>{/if}
					{if $navi_entry.extern === 'TRUE'}<a class="a_navi_menu" target="_blank" href="{$pageUri}{$navi_entry.link}/">{$navi_entry.title}</a>{/if}
				{/if}
			{/if}
		{/foreach}
	</div>
	<div class="navi_menu_close"></div>
{/foreach}
