function showRegisterDiv(id) {
	fadeOut('step_1');
	fadeOut('step_2');
	fadeOut('step_3');
	fadeOut('step_4');
	fadeOut('step_5');
	fadeOut('step_6');
	fadeOut('step_7');
	fadeOut('step_8');
	fadeOut('step_9');
	fadeOut('step_10');
	
	fadeIn(id);
}

function updateRegisterInput(id) {
	input = document.getElementById(id);
	passport = document.getElementById('passport_' + id);
	
	switch(id) {
		case 'firstName':
			passportTop = document.getElementById('passport_' + id + 'Top');
			
			passportTop.innerHTML = input.value + '&nbsp;';
			passport.innerHTML = input.value + '&nbsp;';
			
			break;
		
		case 'surName':
			passportTop = document.getElementById('passport_' + id + 'Top');
			
			passportTop.innerHTML = input.value + '&nbsp;';
			passport.innerHTML = input.value + '&nbsp;';
			
			break;
		
		case 'nickName':
			passportTop = document.getElementById('passport_' + id + 'Top');
			
			passportTop.innerHTML = '\'' + input.value + '\'' + '&nbsp;';
			passport.innerHTML = input.value + '&nbsp;';
			
			break;
		
		case 'webTitle':
			web = document.getElementById('passport_web');
			webUri = document.getElementById('passport_webUri');
			inputTitle = document.getElementById('webTitle');
			inputUri = document.getElementById('webUri');
			
			webUri.title = inputTitle.value;
			if(inputTitle.value != '' && inputUri.value != '') web.style.display = 'block';
			else web.style.display = 'none';
			
			break;
		
		case 'webUri':
			web = document.getElementById('passport_web');
			webUri = document.getElementById('passport_webUri');
			inputTitle = document.getElementById('webTitle');
			inputUri = document.getElementById('webUri');
			
			uri = inputUri.value;
			subUri = uri.substr(0, 7);
			
			if(uri.length > 7) {
				if(subUri == 'http://') uri = uri;
				else uri = 'http://' + uri;
			} else {
				uri = 'http://' + uri;
			}
			
			webUri.href = uri;
			if(inputTitle.value != '' && inputUri.value != '') web.style.display = 'block';
			else web.style.display = 'none';
			
			break;
		
		case 'icq':
			
			break;
		
		case 'msn':
			msn = document.getElementById('passport_msn');
			msnUri = document.getElementById('passport_msnUri');
			inputMsn = document.getElementById('msn');
			
			msnUri.href = 'http://members.msn.com/' + inputMsn.value;
			msnUri.title = 'MSN Messenger - ' + inputMsn.value;
			
			if(inputMsn.value != '') msn.style.display = 'block';
			else msn.style.display = 'none';
			
			break;
		
		case 'aim':
			aim = document.getElementById('passport_aim');
			aimUri = document.getElementById('passport_aimUri');
			inputAim = document.getElementById('aim');
			
			aimUri.href = 'aim:goim?screenname=' + inputAim.value + '&amp;message=Hi.+Are+you+there?';
			aimUri.title = 'AOL Instant Messenger - ' + inputAim.value;
			
			if(inputAim.value != '') aim.style.display = 'block';
			else aim.style.display = 'none';
			
			break;
		
		case 'yim':
			yim = document.getElementById('passport_yim');
			yimUri = document.getElementById('passport_yimUri');
			inputYim = document.getElementById('yim');
			
			yimUri.href = 'http://edit.yahoo.com/config/send_webmesg?.target=' + inputYim.value;
			yimUri.title = 'Yahoo Instant Messenger - ' + inputYim.value;
			
			if(inputYim.value != '') yim.style.display = 'block';
			else yim.style.display = 'none';
			
			break;
		
		case 'skype':
			skype = document.getElementById('passport_skype');
			skypeUri = document.getElementById('passport_skypeUri');
			inputSkype = document.getElementById('skype');
			
			skypeUri.href = 'http://myskype.info/' + inputSkype.value;
			skypeUri.title = 'Skype';
			
			if(inputSkype.value != '') skype.style.display = 'block';
			else skype.style.display = 'none';
			
			break;
		
		case 'steam':
			steam = document.getElementById('passport_steam');
			steamUri = document.getElementById('passport_steamUri');
			inputSteam = document.getElementById('steam');
			
			steamUri.href = 'http://steamcommunity.com/id/' + inputSteam.value;
			steamUri.title = 'Steam';
			
			if(inputSteam.value != '') steam.style.display = 'block';
			else steam.style.display = 'none';
			
			break;
		
		
		case 'facebook':
			facebook = document.getElementById('passport_facebook');
			facebookUri = document.getElementById('passport_facebookUri');
			inputFacebook = document.getElementById('facebook');
			
			facebookUri.href = inputFacebook.value;
			facebookUri.title = 'Facebook';
			
			if(inputFacebook.value != '') facebook.style.display = 'block';
			else facebook.style.display = 'none';
			
			break;
		
		case 'twitter':
			twitter = document.getElementById('passport_twitter');
			twitterUri = document.getElementById('passport_twitterUri');
			inputTwitter = document.getElementById('twitter');
			
			twitterUri.href = inputTwitter.value;
			twitterUri.title = 'Twitter';
			
			if(inputTwitter.value != '') twitter.style.display = 'block';
			else twitter.style.display = 'none';
			
			break;
		
		case 'wkw':
			wkw = document.getElementById('passport_wkw');
			wkwUri = document.getElementById('passport_wkwUri');
			inputWKW = document.getElementById('wkw');
			
			wkwUri.href = inputWKW.value;
			wkwUri.title = 'wer-kennt-wen';
			
			if(inputWKW.value != '') wkw.style.display = 'block';
			else wkw.style.display = 'none';
			
			break;
		
		case 'youtube':
			youtube = document.getElementById('passport_youtube');
			youtubeUri = document.getElementById('passport_youtubeUri');
			inputYoutube = document.getElementById('youtube');
			
			youtubeUri.href = inputYoutube.value;
			youtubeUri.title = 'YouTube';
			
			if(inputYoutube.value != '') youtube.style.display = 'block';
			else youtube.style.display = 'none';
			
			break;
		
		case 'myspace':
			myspace = document.getElementById('passport_myspace');
			myspaceUri = document.getElementById('passport_myspaceUri');
			inputMyspace = document.getElementById('myspace');
			
			myspaceUri.href = inputMyspace.value;
			myspaceUri.title = 'MySpace';
			
			if(inputMyspace.value != '') myspace.style.display = 'block';
			else myspace.style.display = 'none';
			
			break;
		
		default:
			if(input.value === '') passport.innerHTML = '<i>n/a</i>';
			else passport.innerHTML = input.value;
			
			break;
	}
}

function updateRegisterSelect(id) {
	switch(id) {
		case 'bDay':
			pAge		= document.getElementById('passport_age');
			bDay		= document.getElementById('bDay').value;
			bMonth		= document.getElementById('bMonth').value;
			bYear		= document.getElementById('bYear').value;
			date		= new Date(bYear, bMonth - 1, bDay).getTime() / 1000;
			nowDay		= new Date().getDay();
			nowMonth	= new Date().getMonth();
			nowYear 	= new Date().getYear();
			if(nowYear < 999) nowYear += 1900;
			
			age = (nowYear - bYear) - parseInt((nowMonth + nowYear) < sprintf('%02d%02d', bMonth, bDay));
			//age = (nowYear - bYear);
			
			//alert(parseInt((nowMonth + nowYear) < ));
			//if($profile['bYear'] != '') $ageVal = (date('Y') - $profile['bYear']) - intval(date('md') < sprintf('%02d%02d' , $profile['bMonth'], $profile['bDay']));
			//alert(date);
			//alert(bDay.value);
			
			
			pAge.innerHTML = age + ' Jahre';
			//alert('bDay');
			
			break;
		
		case 'bMonth':
			pAge		= document.getElementById('passport_age');
			bDay		= document.getElementById('bDay').value;
			bMonth		= document.getElementById('bMonth').value;
			bYear		= document.getElementById('bYear').value;
			date		= new Date(bYear, bMonth - 1, bDay).getTime() / 1000;
			nowDay		= new Date().getDay();
			nowMonth	= new Date().getMonth();
			nowYear 	= new Date().getYear();
			if(nowYear < 999) nowYear += 1900;
			
			age = (nowYear - bYear) - parseInt((nowMonth + nowYear) < sprintf('%02d%02d', bMonth, bDay));
			
			pAge.innerHTML = age + ' Jahre';
			//alert('bMonth');
			
			break;
		
		case 'bYear':
			pAge		= document.getElementById('passport_age');
			bDay		= document.getElementById('bDay').value;
			bMonth		= document.getElementById('bMonth').value;
			bYear		= document.getElementById('bYear').value;
			date		= new Date(bYear, bMonth - 1, bDay).getTime() / 1000;
			nowDay		= new Date().getDay();
			nowMonth	= new Date().getMonth();
			nowYear 	= new Date().getYear();
			if(nowYear < 999) nowYear += 1900;
			
			age = (nowYear - bYear) - parseInt((nowMonth + nowYear) < sprintf('%02d%02d', bMonth, bDay));
			
			pAge.innerHTML = age + ' Jahre';
			//alert('bYear');
			
			break;
		
		case 'gender':
			pGender0	= document.getElementById('passport_gender_0');
			pGender1	= document.getElementById('passport_gender_1');
			gender		= document.getElementById('gender').value;
			
			pGender0.style.display = 'none';
			pGender1.style.display = 'none';
			if(gender == 0) pGender0.style.display = 'block';
			if(gender == 1) pGender1.style.display = 'block';
			
			break;
		
		case 'nationality':
			imgNationality	= document.getElementById('passport_nationality');
			pNationality	= document.getElementById('passport_nationality_string');
			nationality		= document.getElementById('nationality').value;
			
			// imgNationality.src = '{$imagesPath}flags/'' .png';
			// <img id="passport_nationality" src="{$imagesPath}flags/{$profile_nationality}.png" height="11" width="16" alt="{$nationality_string}" title="{$nationality_string}" />
			//pNationality.innerHTML = nationality;
			//alert('nationality');
			
			break;
		
	}
}
/*
function updateRegisterSelect(id) {
	switch(id) {
		case 'bDay':
			pAge		= document.getElementById('passport_age');
			bDay		= document.getElementById('bDay').value;
			bMonth		= document.getElementById('bMonth').value;
			bYear		= document.getElementById('bYear').value;
			date		= new Date(bYear, bMonth - 1, bDay).getTime() / 1000;
			nowDay		= new Date().getDay();
			nowMonth	= new Date().getMonth();
			nowYear 	= new Date().getYear();
			if(nowYear < 999) nowYear += 1900;
			
			//age = (nowYear - bYear) - parseInt((nowMonth + nowYear) < sprintf);
			age = (nowYear - bYear);
			
			//alert(parseInt((nowMonth + nowYear) < ));
			//if($profile['bYear'] != '') $ageVal = (date('Y') - $profile['bYear']) - intval(date('md') < sprintf('%02d%02d' , $profile['bMonth'], $profile['bDay']));
			//alert(date);
			//alert(bDay.value);
			
			
			pAge.innerHTML = age + ' Jahre';
			//alert('bDay');
			
			break;
		
		case 'bMonth':
			pAge	= document.getElementById('passport_age');
			bDay	= document.getElementById('bDay').value;
			bMonth	= document.getElementById('bMonth').value;
			bYear	= document.getElementById('bYear').value;
			date	= new Date(bYear, bMonth - 1, bDay).getTime() / 1000;
			nowYear = new Date().getYear();
			if(nowYear < 999) nowYear += 1900;
			
			age = (nowYear - bYear);
			
			pAge.innerHTML = age + ' Jahre';
			//alert('bMonth');
			
			break;
		
		case 'bYear':
			pAge	= document.getElementById('passport_age');
			bDay	= document.getElementById('bDay').value;
			bMonth	= document.getElementById('bMonth').value;
			bYear	= document.getElementById('bYear').value;
			date	= new Date(bYear, bMonth - 1, bDay).getTime() / 1000;
			nowYear = new Date().getYear();
			if(nowYear < 999) nowYear += 1900;
			
			age = (nowYear - bYear);
			
			pAge.innerHTML = age + ' Jahre';
			//alert('bYear');
			
			break;
		
		case 'gender':
			pGender0	= document.getElementById('passport_gender_0');
			pGender1	= document.getElementById('passport_gender_1');
			gender		= document.getElementById('gender').value;
			
			pGender0.style.display = 'none';
			pGender1.style.display = 'none';
			if(gender == 0) pGender0.style.display = 'block';
			if(gender == 1) pGender1.style.display = 'block';
			//alert('gender');
			
			break;
		
		
	}
	//alert(sel.value);
	
	/*sel = document.getElementById(id);
	val = sel.options[sel.selectedIndex].value;
	passport = document.getElementById('passport_' + id);
	
	alert(val);
	
	passport.innerHTML = formSelect.value;* /
}
*/

$('input').keypress(function() {
	alert('Handler for .change() called.');
});



// Helper
sprintfWrapper = {
 
	init : function () {
 
		if (typeof arguments == "undefined") { return null; }
		if (arguments.length < 1) { return null; }
		if (typeof arguments[0] != "string") { return null; }
		if (typeof RegExp == "undefined") { return null; }
 
		var string = arguments[0];
		var exp = new RegExp(/(%([%]|(\-)?(\+|\x20)?(0)?(\d+)?(\.(\d)?)?([bcdfosxX])))/g);
		var matches = new Array();
		var strings = new Array();
		var convCount = 0;
		var stringPosStart = 0;
		var stringPosEnd = 0;
		var matchPosEnd = 0;
		var newString = '';
		var match = null;
 
		while (match = exp.exec(string)) {
			if (match[9]) { convCount += 1; }
 
			stringPosStart = matchPosEnd;
			stringPosEnd = exp.lastIndex - match[0].length;
			strings[strings.length] = string.substring(stringPosStart, stringPosEnd);
 
			matchPosEnd = exp.lastIndex;
			matches[matches.length] = {
				match: match[0],
				left: match[3] ? true : false,
				sign: match[4] || '',
				pad: match[5] || ' ',
				min: match[6] || 0,
				precision: match[8],
				code: match[9] || '%',
				negative: parseInt(arguments[convCount]) < 0 ? true : false,
				argument: String(arguments[convCount])
			};
		}
		strings[strings.length] = string.substring(matchPosEnd);
 
		if (matches.length == 0) { return string; }
		if ((arguments.length - 1) < convCount) { return null; }
 
		var code = null;
		var match = null;
		var i = null;
 
		for (i=0; i<matches.length; i++) {
 
			if (matches[i].code == '%') { substitution = '%' }
			else if (matches[i].code == 'b') {
				matches[i].argument = String(Math.abs(parseInt(matches[i].argument)).toString(2));
				substitution = sprintfWrapper.convert(matches[i], true);
			}
			else if (matches[i].code == 'c') {
				matches[i].argument = String(String.fromCharCode(parseInt(Math.abs(parseInt(matches[i].argument)))));
				substitution = sprintfWrapper.convert(matches[i], true);
			}
			else if (matches[i].code == 'd') {
				matches[i].argument = String(Math.abs(parseInt(matches[i].argument)));
				substitution = sprintfWrapper.convert(matches[i]);
			}
			else if (matches[i].code == 'f') {
				matches[i].argument = String(Math.abs(parseFloat(matches[i].argument)).toFixed(matches[i].precision ? matches[i].precision : 6));
				substitution = sprintfWrapper.convert(matches[i]);
			}
			else if (matches[i].code == 'o') {
				matches[i].argument = String(Math.abs(parseInt(matches[i].argument)).toString(8));
				substitution = sprintfWrapper.convert(matches[i]);
			}
			else if (matches[i].code == 's') {
				matches[i].argument = matches[i].argument.substring(0, matches[i].precision ? matches[i].precision : matches[i].argument.length)
				substitution = sprintfWrapper.convert(matches[i], true);
			}
			else if (matches[i].code == 'x') {
				matches[i].argument = String(Math.abs(parseInt(matches[i].argument)).toString(16));
				substitution = sprintfWrapper.convert(matches[i]);
			}
			else if (matches[i].code == 'X') {
				matches[i].argument = String(Math.abs(parseInt(matches[i].argument)).toString(16));
				substitution = sprintfWrapper.convert(matches[i]).toUpperCase();
			}
			else {
				substitution = matches[i].match;
			}
 
			newString += strings[i];
			newString += substitution;
 
		}
		newString += strings[i];
 
		return newString;
 
	},
 
	convert : function(match, nosign){
		if (nosign) {
			match.sign = '';
		} else {
			match.sign = match.negative ? '-' : match.sign;
		}
		var l = match.min - match.argument.length + 1 - match.sign.length;
		var pad = new Array(l < 0 ? 0 : l).join(match.pad);
		if (!match.left) {
			if (match.pad == "0" || nosign) {
				return match.sign + pad + match.argument;
			} else {
				return pad + match.sign + match.argument;
			}
		} else {
			if (match.pad == "0" || nosign) {
				return match.sign + match.argument + pad.replace(/0/g, ' ');
			} else {
				return match.sign + match.argument + pad;
			}
		}
	}
}

sprintf = sprintfWrapper.init;