{* SMARTY *}
{* Used-Variables:
 * $variable - Description
 *}

{extends file='../_content.tpl'}

{block name=content}
{* Your code starts here! *}

<link href="{$templatePath}css/register.css" rel="stylesheet" type="text/css" />
<script src="{$templatePath}js/register.js"></script>

<small>Registration - Step {$regStep} of {$maxStep}</small>
<p>
	<h4 style="text-align:left;">Welcome to account registration.</h4>
	Follow the next steps and create your own unique account.<br />
	You can track the changes you do in a live preview to see what you will create.
</p>

<div align="center">{include file="./profile_passport.tpl"}</div>
<div style="margin-top:10px;">{include file="{$templateDir}content/register_step_{$regStep}.tpl"}</div>

{* Your code ends here! *}
{/block}