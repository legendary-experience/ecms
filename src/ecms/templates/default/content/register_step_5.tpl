<form method="post" name="register">
	<div class="error">{$errorString}</div>
	
	<fieldset>
		<legend>Sicherheitsfrage und Antwort</legend>
		Die Sicherheitsfrage wird bei jedem Wiederherstellen des Passworts gefragt.<br />
		Zus&auml;tzlich m&uuml;sst ihr nach dem ersten Login 5 Fragen aus einer Auswahl von 10 definieren und beantworten.<br />
		Diese sollen dazu dienen, euch in &auml;u&szlig;ersten Notf&auml;llen, eindeutig zu identifizieren.<br />
		
		<input autocomplete="off" id="secretQuestion" name="secretQuestion" placeholder="Sicherheitsfrage" required tabindex="1" type="text" value="{$secretQuestionVal}" />
		<input autocomplete="off" id="secretAnswer" name="secretAnswer" placeholder="Sicherheitsantwort" required tabindex="2" type="password" value="{$secretAnswerVal}" />
	</fieldset>
	
	<div align="right">
		<input tabindex="4" id="prev" name="prev" type="submit" value="ZUR&Uuml;CK" />
		<input tabindex="3" id="next" name="next" type="submit" value="WEITER" />
	</div>
</form>