<form method="post" name="register">
	<div class="error">{$errorString}</div>
	
	<fieldset>
		<legend>E-Mail</legend>
		
		<input autocomplete="off" id="email" name="email" placeholder="you@provider.domain" required tabindex="1" type="email" value="{$user_email}" />
	</fieldset>
	
	<div align="right">
		<input tabindex="3" id="prev" name="prev" type="submit" value="ZUR&Uuml;CK" />
		<input tabindex="2" id="next" name="next" type="submit" value="WEITER" />
	</div>
</form>