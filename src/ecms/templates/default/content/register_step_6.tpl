<form method="post" name="register">
	<div class="error">{$errorString}</div>
	
	<fieldset>
		<legend>Pers&ouml;nliche Informationen</legend>
		
		<fieldset>
			<legend>Geburtsdatum</legend>
			
			<!--
			<input autocomplete="off" id="bDay" maxlength="2" name="bDay" placeholder="Tag" required size="2" tabindex="1" type="text" value="{$profile_b_day}" style="width:2em; float:left;" />
			<input autocomplete="off" id="bMonth" maxlength="2" name="bMonth" placeholder="Monat" required size="2" tabindex="2" type="text" value="{$profile_b_month}" style="width:2em; float:left; margin-left:5px;" />
			<input autocomplete="off" id="bYear" maxlength="4" name="bYear" placeholder="Jahr" required size="4" tabindex="3" type="text" value="{$profile_b_year}" style="width:6.50em; float:left; margin-left:5px;" />
			-->
			
			<select id="bDay" name="bDay" onchange="javascript:updateRegisterSelect('bDay');" style="float:left; width:auto;" required tabindex="1">
				{foreach key=id item=day from=$days}
					{if $day < 10}
						{if $profile_b_day == $day}<option value="{$day}" selected="selected">0{$day}.</option>{else}<option value="{$day}">0{$day}.</option>{/if}
					{else}
						{if $profile_b_day == $day}<option value="{$day}" selected="selected">{$day}.</option>{else}<option value="{$day}">{$day}.</option>{/if}
					{/if}
				{/foreach}
			</select>
			<select id="bMonth" name="bMonth" onchange="javascript:updateRegisterSelect('bMonth');" style="float:left; margin-left:5px; width:auto;" required tabindex="2">
				{foreach key=id item=month from=$months}
					{if $profile_b_month == $month.id}<option value="{$month.id}" selected="selected">{$month.title}</option>{else}<option value="{$month.id}">{$month.title}</option>{/if}
				{/foreach}
			</select>
			<select id="bYear" name="bYear" onchange="javascript:updateRegisterSelect('bYear');" style="float:left; margin-left:5px; width:auto;" required tabindex="3">
				{foreach key=id item=year from=$years}
					{if $profile_b_year == $year}<option value="{$year}" selected="selected">{$year}</option>{else}<option value="{$year}">{$year}</option>{/if}
				{/foreach}
			</select>
		</fieldset>
		
		<fieldset>
			<legend>Geschlecht</legend>
			
			<select id="gender" name="gender" onchange="javascript:updateRegisterSelect('gender');" style="clear:both;" tabindex="4">
				{if $profile_gender === '0'}<option value="0" selected="selected">M&auml;nnlich</option>{else}<option value="0">M&auml;nnlich</option>{/if}
				{if $profile_gender === '1'}<option value="1" selected="selected">Weiblich</option>{else}<option value="1">Weiblich</option>{/if}
			</select>
		</fieldset>
		
		<fieldset>
			<legend>Nationalit&auml;t</legend>
			
			<select id="nationality" name="nationality" onchange="javascript:updateRegisterSelect('nationality');" tabindex="5">
				{foreach key=id item=country from=$countries}
					{if $profile_nationality === $country.iso2}<option value="{$country.iso2}" selected="selected">{$country.country}</option>{else}<option value="{$country.iso2}">{$country.country}</option>{/if}
				{/foreach}
			</select>
		</fieldset>
		
		<input autocomplete="off" id="location" name="location" onkeydown="javascript:updateRegisterInput('location');" onkeyup="javascript:updateRegisterInput('location');" placeholder="Wohnort" required tabindex="6" type="text" value="{$profile_location}" />
		<input autocomplete="off" id="profession" name="profession" onkeydown="javascript:updateRegisterInput('profession');" onkeyup="javascript:updateRegisterInput('profession');" placeholder="Beruf" required tabindex="7" type="text" value="{$profile_profession}" />
	</fieldset>
	
	<fieldset>
		<legend>Optionale Informationen</legend>
		Die folgenden Angaben sind komplett optional.<br />
		Felder, welche nicht ausgef&uuml;llt sind, werden sp&auml;ter nicht angezeigt.<br />
		
		<fieldset>
			<legend>Eigene Homepage</legend>
			
			<input autocomplete="off" id="webTitle" name="webTitle" onkeydown="javascript:updateRegisterInput('webTitle');" onkeyup="javascript:updateRegisterInput('webTitle');" placeholder="Homepage Titel" tabindex="8" type="text" value="{$profile_web_title}" />
			<input autocomplete="off" id="webUri" name="webUri" onkeydown="javascript:updateRegisterInput('webUri');" onkeyup="javascript:updateRegisterInput('webUri');" placeholder="Homepage URL (Link)" tabindex="9" type="text" value="{$profile_web_uri}" />
		</fieldset>
		
		<fieldset>
			<legend>Instant Messengers</legend>
			
			<input autocomplete="off" id="icq" name="icq" onkeydown="javascript:updateRegisterInput('icq');" onkeyup="javascript:updateRegisterInput('icq');" placeholder="ICQ" tabindex="10" type="text" value="{$profile_icq}" />
			<input autocomplete="off" id="msn" name="msn" onkeydown="javascript:updateRegisterInput('msn');" onkeyup="javascript:updateRegisterInput('msn');" placeholder="MSN" tabindex="11" type="text" value="{$profile_msn}" />
			<input autocomplete="off" id="aim" name="aim" onkeydown="javascript:updateRegisterInput('aim');" onkeyup="javascript:updateRegisterInput('aim');" placeholder="AOL Instant Messenger" tabindex="12" type="text" value="{$profile_aim}" />
			<input autocomplete="off" id="yim" name="yim" onkeydown="javascript:updateRegisterInput('yim');" onkeyup="javascript:updateRegisterInput('yim');" placeholder="Yahoo Instant Messenger" tabindex="13" type="text" value="{$profile_yim}" />
			<input autocomplete="off" id="skype" name="skype" onkeydown="javascript:updateRegisterInput('skype');" onkeyup="javascript:updateRegisterInput('skype');" placeholder="Skype" tabindex="14" type="text" value="{$profile_skype}" />
			<input autocomplete="off" id="steam" name="steam" onkeydown="javascript:updateRegisterInput('steam');" onkeyup="javascript:updateRegisterInput('steam');" placeholder="Steam" tabindex="15" type="text" value="{$profile_steam}" />
		</fieldset>
		
		<fieldset>
			<legend>Soziale Netzwerke</legend>
			<input autocomplete="off" id="facebook" name="facebook" onkeydown="javascript:updateRegisterInput('facebook');" onkeyup="javascript:updateRegisterInput('facebook');" placeholder="Facebook" tabindex="16" type="text" value="{$profile_facebook}" />
			<input autocomplete="off" id="twitter" name="twitter" onkeydown="javascript:updateRegisterInput('twitter');" onkeyup="javascript:updateRegisterInput('twitter');" placeholder="Twitter" tabindex="17" type="text" value="{$profile_twitter}" />
			<input autocomplete="off" id="wkw" name="wkw" onkeydown="javascript:updateRegisterInput('wkw');" onkeyup="javascript:updateRegisterInput('wkw');" placeholder="wer-kennt-wen" tabindex="18" type="text" value="{$profile_wkw}" />
			<input autocomplete="off" id="youtube" name="youtube" onkeydown="javascript:updateRegisterInput('youtube');" onkeyup="javascript:updateRegisterInput('youtube');" placeholder="YouTube" tabindex="19" type="text" value="{$profile_youtube}" />
			<input autocomplete="off" id="myspace" name="myspace" onkeydown="javascript:updateRegisterInput('myspace');" onkeyup="javascript:updateRegisterInput('myspace');" placeholder="MySpace" tabindex="20" type="text" value="{$profile_myspace}" />
		</fieldset>
	</fieldset>
	
	<div align="right">
		<input tabindex="22" id="prev" name="prev" type="submit" value="ZUR&Uuml;CK" />
		<input tabindex="21" id="next" name="next" type="submit" value="WEITER" />
	</div>
</form>