<form method="post" name="register">
	<div class="error">{$errorString}</div>
	
	<fieldset>
		<legend>Passwort</legend>
		
		<input autocomplete="off" id="password" name="password" placeholder="Passwort" required tabindex="1" type="password" value="{$passwordVal}" />
		<input autocomplete="off" id="password2" name="password2" placeholder="Passwort wiederholen" required tabindex="2" type="password" value="{$password2Val}" />
	</fieldset>
	
	<div align="right">
		<input tabindex="4" id="prev" name="prev" type="submit" value="ZUR&Uuml;CK" />
		<input tabindex="3" id="next" name="next" type="submit" value="WEITER" />
	</div>
</form>