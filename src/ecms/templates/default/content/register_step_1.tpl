<form method="post" name="register">
	<fieldset>
		<legend>Vor- und Nachname</legend>
		
		<input autocomplete="off" id="firstName" name="firstName" onkeydown="javascript:updateRegisterInput('firstName');" onkeyup="javascript:updateRegisterInput('firstName');" placeholder="Vorname" required tabindex="1" type="text" value="{$profile_first_name}" />
		<input autocomplete="off" id="surName" name="surName" onkeydown="javascript:updateRegisterInput('surName');" onkeyup="javascript:updateRegisterInput('surName');" placeholder="Nachname" required tabindex="2" type="text" value="{$profile_sur_name}" />
	</fieldset>
	
	<div align="right">
		<!-- <input tabindex="4" id="prev" name="prev" type="button" value="ZUR&Uuml;CK" /> -->
		<input tabindex="3" id="next" name="next" type="submit" value="WEITER" />
	</div>
</form>