<link href="{$templatePath}css/passport.css" rel="stylesheet" type="text/css" />

<div class="passport">
	<div class="passport_pic">
		{if $profile_userpic == ''}
			<img alt="{$profile_first_name} {$profile_sur_name}" border="0" height="240" src="{$pageUri}media/2165468/" style="border-radius:5px;" title="{$profile_first_name} {$profile_sur_name}" width="180" />
		{else}
			<img alt="{$profile_first_name} {$profile_sur_name}" border="0" height="240" src="{$pageUri}media/{$profile_userpic}/" style="border-radius:5px;" title="{$profile_first_name} {$profile_sur_name}" width="180" />
		{/if}
	</div>
	
	<div class="passport_infos">
		<div style="float:left; width:305px;">
			<div id="passport_online" style="float:left;">
				{if $fListed == true and $fFriends == true}
					<img align="{$online_string}" border="0" height="11" src="{$imagesPath}profile/user_1_{$profile_online}.gif" title="{$online_string}" width="17" />&nbsp;
				{else}
					<img align="{$online_string}" border="0" height="10" src="{$imagesPath}profile/user_0_{$profile_online}.gif" title="{$online_string}" width="12" />&nbsp;
				{/if}
			</div>
			<div id="passport_firstNameTop" style="float:left;">{if $profile_first_name != ''}{$profile_first_name}{else}<i>n/a</i>{/if}&nbsp;</div>
			<div id="passport_nickNameTop" style="float:left;">'{if $user_username != ''}{$user_username}{else}<i>n/a</i>{/if}'&nbsp;</div>
			<div id="passport_surNameTop" style="float:left;">{if $profile_sur_name != ''}{$profile_sur_name}{else}<i>n/a</i>{/if}&nbsp;</div>
		</div>
		<div align="right" style="float:left; text-align:right; width:90px; z-index:10;">
			UID: 
			{if $user_seo != ''}
				<a href="{$pageUri}profile/{$user_seo}/">{$user_uid}</a>
			{elseif $user_uid != ''}
				<a href="{$pageUri}profile/{$user_uid}/">{$user_uid}</a>
			{else}
				<i>n/a</i>
			{/if}
		</div>
		<div style="position:absolute; width:395px;">
			<div style="background-color:#111111; border-radius:5px; display:none; float:right; margin-top:15px; width:150px;"></div>
		</div>
		<div class="clearfix"></div>
		
		<div style="margin-bottom:3px; width:100%;"><hr /></div>
		
		<!--
		<div style="position:absolute; text-align:right; width:395px; z-index:10;">
			<img src="{$imagesPath}" alt="aaa" />
		</div>
		-->
		
		<div align="left" style="float:left; margin-bottom:3px; width:50%;">Name</div>
		<div align="left" style="float:left; margin-bottom:3px; width:50%;">
			<div id="passport_firstName" style="float:left;">{if $profile_first_name != ''}{$profile_first_name}{else}<i>n/a</i>{/if}&nbsp;</div>
			<div id="passport_surName" style="float:left;">{if $profile_sur_name != ''}{$profile_sur_name}{else}<i>n/a</i>{/if}</div>
		</div>
		<div class="clearfix"></div>
		
		<div align="left" style="float:left; margin-bottom:3px; width:50%;">Spitzname</div>
		<div align="left" style="float:left; margin-bottom:3px; width:50%;" id="passport_nickName">{if $user_username != ''}{$user_username}{else}<i>n/a</i>{/if}&nbsp;</div>
		<div class="clearfix"></div>
		
		<div align="left" style="float:left; margin-bottom:3px; width:50%;">Mitglied seit</div>
		<div align="left" style="float:left; margin-bottom:3px; width:50%;" id="passport_registered">{if $user_registered != ''}{$user_registered|timeSpan}{else}<i>n/a</i>{/if}&nbsp;</div>
		<div class="clearfix"></div>
		
		<div align="left" style="float:left; margin-bottom:3px; width:50%;">Alter</div>
		<div align="left" style="float:left; margin-bottom:3px; width:50%;" id="passport_age">{if $age != ''}{$age} Jahre{else}<i>n/a</i>{/if}&nbsp;</div>
		<div class="clearfix"></div>
		
		<div align="left" style="float:left; margin-bottom:3px; width:50%;">Geschlecht</div>
		<div align="left" style="float:left; margin-bottom:3px; width:50%;">
			{if $profile_gender != -1}
				{if $profile_gender == 0}
					<div id="passport_gender_0" style="display:block;">
				{else}
					<div id="passport_gender_0" style="display:none;">
				{/if}
					<img align="M&auml;nnlich" border="0" height="12" src="{$imagesPath}profile/gender_0.gif" title="M&auml;nnlich" /> M&auml;nnlich
				</div>
				
				{if $profile_gender == 1}
					<div id="passport_gender_1" style="display:block;">
				{else}
					<div id="passport_gender_1" style="display:none;">
				{/if}
					<img align="Weiblich" border="0" height="12" src="{$imagesPath}profile/gender_1.gif" title="Weiblich" /> Weiblich
				</div>
			{else}
				<i>n/a</i>
			{/if}
		</div>
		<div class="clearfix"></div>
		
		<div align="left" style="float:left; margin-bottom:3px; width:50%;">Nationalit&auml;t</div>
		<div align="left" style="float:left; margin-bottom:3px; width:50%;">
			{if $profile_nationality != ''}
				<img alt="{$nationality_string}" border="0" height="11" id="passport_nationality" src="{$imagesPath}flags/{$profile_nationality}.png" title="{$nationality_string}" width="16" />
				<span id="passport_nationality_string">{$nationality_string}</span>
			{else}
				<i>n/a</i>
			{/if}
		</div>
		<div class="clearfix"></div>
		
		<div align="left" style="float:left; margin-bottom:3px; width:50%;">Wohnort</div>
		<div align="left" style="float:left; margin-bottom:3px; width:50%" id="passport_location">{if $profile_location != ''}{$profile_location}{else}<i>n/a</i>{/if}</div>
		<div class="clearfix"></div>
		
		<div align="left" style="float:left; margin-bottom:3px; width:50%;">Beruf</div>
		<div align="left" style="float:left; margin-bottom:3px; width:50%;" id="passport_profession">{if $profile_profession != ''}{$profile_profession}{else}<i>n/a</i>{/if}</div>
		<div class="clearfix"></div>
		
		<div style="width:100%; margin-bottom:3px;"><hr /></div>
		
		<div align="left" style="width:100%;">
			{*
			<div id="passport_email" style="display:none; float:left;">
				<a title="jaymc@arcor.de" rel="nofollow" style="cursor:pointer;"><img src="{$imagesPath}profile/contact/mail.gif" alt="eMail" width="16" height="16" /></a>
			</div>
			*}
			
			{if $profile_web_title != '' && $profile_web_uri != ''}
				<div id="passport_web" style="display:block; float:left;">
			{else}
				<div id="passport_web" style="display:none; float:left;">
			{/if}
				<a id="passport_webUri" href="{$profile_web_uri}" title="{$profile_web_title}" target="_blank">
						<img src="{$imagesPath}profile/contact/www.gif" alt="Website" width="16" height="16" />
				</a>
			</div>
			
			{*
			{if $profile_icq != '' && $profile_icq != '0'}
				<div id="passport_icq" style="display:block; float:left;">
			{else}
				<div id="passport_icq" style="display:none; float:left;">
			{/if}
				{icq icq={$profile_icq} title={$profile_icq}}
			</div>
			*}
			
			{if $profile_msn != ''}
				<div id="passport_msn" style="display:block; float:left;">
			{else}
				<div id="passport_msn" style="display:none; float:left;">
			{/if}
				<a id="passport_msnUri" href="http://members.msn.com/{$profile_msn}" title="MSN Messenger - {$profile_msn}" target="_blank">
					<img src="{$imagesPath}profile/contact/msn.gif" alt="" width="16" height="16" alt="MSN" />
				</a>
			</div>
			
			{if $profile_aim != ''}
				<div id="passport_aim" style="display:block; float:left;">
			{else}
				<div id="passport_aim" style="display:none; float:left;">
			{/if}
				<a id="passport_aimUri" href="aim:goim?screenname={$profile_aim}&amp;message=Hi.+Are+you+there?" title="AOL Instant Messenger - {$profile_aim}" target="_blank">
					<img src="{$imagesPath}profile/contact/aim.gif" width="16" height="16" alt="AIM" />
				</a>
			</div>
			
			{if $profile_yim != ''}
				<div id="passport_yim" style="display:block; float:left;">
			{else}
				<div id="passport_yim" style="display:none; float:left;">
			{/if}
				<a id="passport_yimUri" href="http://edit.yahoo.com/config/send_webmesg?.target={$profile_yim}" title="Yahoo Instant Messenger - {$profile_yim}" target="_blank">
					<img src="{$imagesPath}profile/contact/yim.gif" width="16" height="16" alt="YIM" />
				</a>
			</div>
			
			{if $profile_skype != ''}
				<div id="passport_skype" style="display:block; float:left;">
			{else}
				<div id="passport_skype" style="display:none; float:left;">
			{/if}
				<a id="passport_skypeUri" href="http://myskype.info/{$profile_skype}" title="Skype - {$profile_skype}" target="_blank">
					<img src="{$imagesPath}profile/contact/skype.png" width="16" height="16" alt="Skype" />
				</a>
			</div>
			
			{if $profile_steam != ''}
				<div id="passport_steam" style="display:block; float:left;">
			{else}
				<div id="passport_steam" style="display:none; float:left;">
			{/if}
				<a id="passport_steamUri" href="http://steamcommunity.com/id/{$profile_steam}" title="Steam - {$profile_steam}" target="_blank">
					<img src="{$imagesPath}profile/contact/steam.gif" width="16" height="16" alt="Steam" />
				</a>
			</div>
			
			{if $profile_facebook != ''}
				<div id="passport_facebook" style="display:block; float:left;">
			{else}
				<div id="passport_facebook" style="display:none; float:left;">
			{/if}
				<a id="passport_facebookUri" href="{$profile_facebook}" title="Facebook - {$profile_first_name} {$profile_sur_name}" target="_blank">
					<img src="{$imagesPath}profile/contact/fb.gif" width="16" height="16" alt="Facebook" />
				</a>
			</div>
			
			{if $profile_twitter != ''}
				<div id="passport_twitter" style="display:block; float:left;">
			{else}
				<div id="passport_twitter" style="display:none; float:left;">
			{/if}
				<a id="passport_twitterUri" href="{$profile_twitter}" title="Twitter - {$profile_first_name} {$profile_sur_name}" target="_blank">
					<img src="{$imagesPath}profile/contact/twitter.png" width="16" height="16" alt="Twitter" />
				</a>
			</div>
			
			{if $profile_google_plus != ''}
				<div id="passport_google_plus" style="display:block; float:left;">
			{else}
				<div id="passport_google_plus" style="display:none; float:left;">
			{/if}
				<a id="passport_google_plusUri" href="{$profile_google_plus}" title="Google+ - {$profile_first_name} {$profile_sur_name}" target="_blank">
					<img src="{$imagesPath}profile/contact/google_plus.png" width="16" height="16" align="Google+" />
				</a>
			</div>
			
			{if $profile_wkw != ''}
				<div id="passport_wkw" style="display:block; float:left;">
			{else}
				<div id="passport_wkw" style="display:none; float:left;">
			{/if}
				<a id="passport_wkwUri" href="{$profile_wkw}" title="werk-kennt-wen - {$profile_first_name} {$profile_sur_name}" target="_blank">
					<img src="{$imagesPath}profile/contact/wkw.png" width="16" height="16" alt="wer-kennt-wen" />
				</a>
			</div>
			
			{if $profile_youtube != ''}
				<div id="passport_youtube" style="display:block; float:left;">
			{else}
				<div id="passport_youtube" style="display:none; float:left;">
			{/if}
				<a id="passport_youtubeUri" href="{$profile_youtube}" title="YouTube - {$profile_first_name} {$profile_sur_name}" target="_blank">
					<img src="{$imagesPath}profile/contact/youtube.png" width="16" height="16" alt="YouTube" />
				</a>
			</div>
			
			{if $profile_myspace != ''}
				<div id="passport_myspace" style="display:block; float:left;">
			{else}
				<div id="passport_myspace" style="display:none; float:left;">
			{/if}
				<a id="passport_myspaceUri" href="{$profile_myspace}" title="MySpace - {$profile_first_name} {$profile_sur_name}" target="_blank">
					<img src="{$imagesPath}profile/contact/myspace.png" width="16" height="16" alt="MySpace" />
				</a>
			</div>
		</div>
	</div>
</div>