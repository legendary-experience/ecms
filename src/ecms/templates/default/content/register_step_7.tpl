<form method="post" name="register">
	<div class="error">{$errorString}</div>
	
	<fieldset>
		<legend>Deine Daten</legend>
		Wir haben deine Daten gesammelt.<br />
		&Uuml;berpr&uuml;fe bitte, ob du dein Profil so anlegen m&ouml;chtest.<br />
		Du kannst jederzeit zur&uuml;ck gehen und Daten &auml;ndern.<br />
		Wenn dir alles gef&auml;llt, klicke auf WEITER und dein Account wird erstellt.<br />
		Vergiss nicht, dass du eine E-Mail bekommst, mit der du deinen Account erst freischalten musst.<br />
		
		<div style="float:left; width:25%;">E-Mail Adresse:</div>
		<div style="float:left; width:75%;">{$user_email}</div>
		<div style="float:left; width:25%;">Sicherheitsfrage:</div>
		<div style="float:left; width:75%;">{$secretQuestionVal}</div>
		<div style="float:left; width:25%;">Antwort:</div>
		<div style="float:left; width:75%;"><i>beantwortet</i></div>
	</fieldset>
	
	<div align="right">
		<input tabindex="2" id="prev" name="prev" type="submit" value="ZUR&Uuml;CK" />
		<input tabindex="1" id="next" name="next" type="submit" value="WEITER" />
	</div>
</form>