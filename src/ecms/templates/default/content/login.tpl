{* SMARTY *}
{* Used-Variables:
 * $variable - Description
 *}

{extends file='../_content.tpl'}

{block name=content}
{* Your code starts here! *}

<link href="{$templatePath}css/login.css" rel="stylesheet" type="text/css" />

<form method="POST">
    <div class="error">{$errorString}</div>
    <div class="success">{$successString}</div>
    
    <fieldset>
        <legend>Einloggen</legend>
        
        <input autocomplete="off" id="email-field" name="email-field" placeholder="E-Mail" required tabindex="1" type="email" value="{$email}" />
        <input autocomplete="off" id="password-field" name="password-field" placeholder="Password" required tabindex="2" type="password" value="{$password}" />
        <!--
        {if $autoLogin === true}<input id="autoLogin" name="autoLogin" tabindex="3" type="checkbox" checked="checked" />{else}<input id="autoLogin" name="autoLogin" tabindex="3" type="checkbox" />{/if}
		<label for="autoLogin">Eingeloggt bleiben</label>
        -->
    </fieldset>
    
    <div align="right"><input id="submitLogin" name="submitLogin" tabindex="4" type="submit" value="EINLOGGEN" /></div>
</form>

{* Your code ends here! *}
{/block}