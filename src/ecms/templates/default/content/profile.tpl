{* SMARTY *}
{* Used-Variables:
 * $variable - Description
 *}

{extends file='../_content.tpl'}

{block name=content}
{* Your code starts here! *}

<link href="{$templatePath}css/profile.css" rel="stylesheet" type="text/css" />

<div align="center">{include file="{$templateDir}content/profile_passport.tpl"}</div>

{if $loggedIn === true}
	{if $user_uid !== $ownUID && $user_uid !== '1234567'}
		<div style="text-align:right; width:100%; margin-right:15px; margin-top:-1.4em;">
			<small>
				{if $fListed == false}
					{if $user_seo !== ''}
						<a href="{$pageUri}friends/add/{$user_seo}/">{$user_username} hinzuf&uuml;gen</a>&nbsp;
					{else}
						<a href="{$pageUri}friends/add/{$user_uid}/">{$user_username} hinzuf&uuml;gen</a>&nbsp;
					{/if}
				{elseif $fListed == true and $fAdded == true}
					{if $user_seo !== ''}
						<a href="{$pageUri}friends/req/{$user_seo}/" style="color:#FF0000;">Freundschaftsanfrage best&auml;tigen</a>&nbsp;
					{else}
						<a href="{$pageUri}friends/req/{$user_uid}/" style="color:#FF0000;">Freundschaftsanfrage best&auml;tigen</a>&nbsp;
					{/if}
				{elseif $fListed == true and $fFriends == false}
					Freundschaftsanfrage ausstehend&nbsp;
				{elseif $fListed == true and $fFriends == true}
					{if $user_seo !== ''}
						<a href="{$pageUri}friends/del/{$user_seo}/">{$user_username} l&ouml;schen</a>&nbsp;
					{else}
						<a href="{$pageUri}friends/del/{$user_uid}/">{$user_username} l&ouml;schen</a>&nbsp;
					{/if}
				{/if}
				
				{if $fBlocked == false}
					{if $user_seo !== ''}
						<a href="{$pageUri}friends/ignore/{$user_seo}/">{$user_username} blockieren</a>&nbsp;
					{else}
						<a href="{$pageUri}friends/ignore/{$user_uid}/">{$user_username} blockieren</a>&nbsp;
					{/if}
				{/if}
				
				{if $user_seo !== ''}
					<a href="{$pageUri}report/{$user_seo}/">{$user_username} melden</a>&nbsp;
				{else}
					<a href="{$pageUri}report/{$user_uid}/">{$user_username} melden</a>&nbsp;
				{/if}
				
				{if $canSendPM == true}
					{if $user_seo !== ''}
						<a href="{$pageUri}messages/send/{$user_seo}/">Nachricht senden</a>&nbsp;
					{else}
						<a href="{$pageUri}messages/send/{$user_uid}/">Nachricht senden</a>&nbsp;
					{/if}
				{/if}
			</small>
		</div>
	{/if}
{/if}
<hr />

<div align="center" style="width:100%;" id="profile_navi">
	{if $canViewInfo === true}<a onclick="javascript:showProfileDiv('info');" style="cursor:pointer;">Informationen</a>{/if}
	{if $canViewWall === true}| <a onclick="javascript:showProfileDiv('wall');" style="cursor:pointer;">Pinnwand</a>{/if}
	{if $canViewGuestbook === true}| <a onclick="javascript:showProfileDiv('guestbook');" style="cursor:pointer;">G&auml;stebuch</a>{/if}
	{if $canViewFriends === true}| <a onclick="javascript:showProfileDiv('friends');" style="cursor:pointer;">Freunde</a>{/if}
	{if $canViewGroups === true}| <a onclick="javascript:showProfileDiv('groups');" style="cursor:pointer;">Gruppen</a>{/if}
	{if $canViewEvents === true}| <a onclick="javascript:showProfileDiv('events');" style="cursor:pointer;">Events</a>{/if}
	{if $canViewLastVisitors === true}| <a onclick="javascript:showProfileDiv('last_visitors');" style="cursor:pointer;">Letzte Besucher</a>{/if}
	{if $canViewMedia === true}| <a onclick="javascript:showProfileDiv('media');" style="cursor:pointer;">Mediathek</a>{/if}
</div>

{if $canViewInfo === true}<div style="width:100%; height:100%; display:none;" id="profile_info">{include file="{$templateDir}content/profile_info.tpl"}</div>{/if}

{if $canViewWall === true}<div style="width:100%; height:100%; display:none;" id="profile_wall">{include file="{$templateDir}content/profile_wall.tpl"}</div>{/if}

{if $canViewGuestbook === true}<div style="width:100%; height:100%; display:none;" id="profile_guestbook">{include file="{$templateDir}content/profile_guestbook.tpl"}</div>{/if}

{if $canViewFriends === true}<div style="width:100%; height:100%; display:none;" id="profile_friends">{include file="{$templateDir}content/profile_friends.tpl"}</div>{/if}

{if $canViewGroups === true}<div style="width:100%; height:100%; display:none;" id="profile_groups">{include file="{$templateDir}content/profile_groups.tpl"}</div>{/if}

{if $canViewEvents === true}<div style="width:100%; height:100%; display:none;" id="profile_events">{include file="{$templateDir}content/profile_events.tpl"}</div>{/if}

{if $canViewLastVisitors === true}<div style="width:100%; height:100%; display:none;" id="profile_last_visitors">{include file="{$templateDir}content/profile_last_visitors.tpl"}</div>{/if}

{if $canViewMedia === true}<div style="width:100%; height:100%; display:none;" id="profile_media">{include file="{$templateDir}content/profile_media.tpl"}</div>{/if}

<script src="{$templatePath}js/profile.js"></script>
{$jsString}

{* Your code ends here! *}
{/block}