<form method="post" name="register">
	<div class="error">{$errorString}</div>
	
	<fieldset>
		<legend>Spitzname/Nickname</legend>
		
		<input autocomplete="off" id="nickName" name="nickName" onkeydown="javascript:updateRegisterInput('nickName');" onkeyup="javascript:updateRegisterInput('nickName');" placeholder="Spitzname/Nickname" required tabindex="1" type="text" value="{$user_username}" />
	</fieldset>
	
	<div align="right">
		<input tabindex="3" id="prev" name="prev" type="submit" value="ZUR&Uuml;CK" />
		<input tabindex="2" id="next" name="next" type="submit" value="WEITER" />
	</div>
</form>