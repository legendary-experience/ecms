<?php
/***********************************************************\
| Functions:												|
| - void	public	__construct(object eCMS);				|
| - array	public	getProfile(string $uid = NULL);			|
| - array	public	getProfiles();							|
| - bool	public	updateProfile(string $uid,				|
|								  string $firstName,		|
|								  $secondName,				|
|								  $surName,					|
|								  $userpic,					|
|								  $online,					|
|								  $bDay,					|
|								  $bMonth,					|
|								  $bYear,					|
|								  $gender,					|
|								  $nationality,				|
|								  $location,				|
|								  $profession,				|
|								  $icq,						|
|								  $msn,						|
|								  $aim,						|
|								  $yim,						|
|								  $skype,					|
|								  $steam,					|
|								  $facebook,				|
|								  $twitter,					|
|								  $wkw,						|
|								  $youtube,					|
|								  $myspace,					|
|								  $signature);				|
| - array	public	getCountry(string $iso2 = NULL);		|
| - array	public	getRelationship(string $rid = NULL);	|
| - array	public	getWall(string $uid = NULL);			|
| - bool	public	addWallEntry(string $uid,				|
|								 string $text);				|
| - bool	public	delWallEntry(string $wid);				|
| - bool	public	wallLike(string $wid,					|
|							 boolean $like);				|
| - array	public	getGuestbook(string $uid = NULL);		|
| - bool	public	addGuestbookEntry(string $uid,			|
|									  string $text);		|
| - bool	public	delGuestbookEntry(string $wid);			|
| - bool	public	guestbookLike(string $gid,				|
|								  boolean $like);			|
| - void	public	__destruct();							|
\***********************************************************/

if(!defined('eCMS')) die('Hacking attempt...');

class Profile {
	private $eCMS;
	
	private $uid;
	private $first_name;
	private $second_name;
	private $sur_name;
	private $userpic;
	private $online;
	private $b_day;
	private $b_month;
	private $b_year;
	private $gender;
	private $nationality;
	private $location;
	private $profession;
	private $icq;
	private $msn;
	private $aim;
	private $yim;
	private $skype;
	private $steam;
	private $facebook;
	private $twitter;
	private $google_plus;
	private $wkw;
	private $youtube;
	private $myspace;
	private $signature;
	private $web_title;
	private $web_uri;
	private $slogan;
	private $about;
	private $relationship;
	private $fav_radio;
	private $fav_dj;
	private $fav_club;
	private $fav_genre;
	private $fav_disco;
	private $fav_song;
	private $fav_movie;
	private $fav_series;
	private $board_posts;
	private $profile_edited;
	
	public function __construct($eCMS) {
		$this->eCMS = $eCMS;
		
		if($this->eCMS->security->checkLogin() === true) {
			$this->uid = $_SESSION['eCMSuserUID'];
			
			$row = $this->eCMS->db->getArray("SELECT 
													first_name, 
													second_name, 
													sur_name, 
													userpic, 
													online, 
													b_day, 
													b_month, 
													b_year, 
													gender, 
													nationality, 
													location, 
													profession, 
													icq, 
													msn, 
													aim, 
													yim, 
													skype, 
													steam, 
													facebook, 
													twitter, 
													google_plus, 
													wkw, 
													youtube, 
													myspace, 
													signature, 
													wid, 
													slogan, 
													about, 
													relationship, 
													fav_radio, 
													fav_dj, 
													fav_club, 
													fav_genre, 
													fav_disco, 
													fav_song, 
													fav_movie, 
													fav_series, 
													board_posts, 
													profile_edited 
												FROM 
													{$this->eCMS->db_prefix}user_profiles 
												WHERE 
													uid = '".$this->eCMS->db->secureString($this->uid)."'");
			
			if(count($row) > 0 && $row[0]['wid'] > 0) $row2 = $this->eCMS->db->getArray("SELECT 
																								web_title, 
																								web_uri 
																							FROM 
																								{$this->eCMS->db_prefix}user_websites 
																							WHERE 
																								wid = '".$this->eCMS->db->secureString($row[0]['wid'])."' 
																							AND 
																								uid = '".$this->eCMS->db->secureString($this->uid)."'");
			
			if(count($row) > 0 && $row[0]['relationship'] > 0) $row3 = $this->eCMS->db->getArray("SELECT 
																										title, 
																										sort 
																									FROM 
																										{$this->eCMS->db_prefix}user_relationships 
																									WHERE 
																										rid = '".$this->eCMS->db->secureString($row[0]['relationship'])."'");
			
			$this->first_name		= $row[0]['first_name'];
			$this->second_name		= $row[0]['second_name'];
			$this->sur_name			= $row[0]['sur_name'];
			$this->userpic			= $row[0]['userpic'];
			$this->online			= $row[0]['online'];
			$this->b_day			= $row[0]['b_day'];
			$this->b_month			= $row[0]['b_month'];
			$this->b_year			= $row[0]['b_year'];
			$this->gender			= $row[0]['gender'];
			$this->nationality		= $row[0]['nationality'];
			$this->location			= $row[0]['location'];
			$this->profession		= $row[0]['profession'];
			$this->icq				= $row[0]['icq'];
			$this->msn				= $row[0]['msn'];
			$this->aim				= $row[0]['aim'];
			$this->yim				= $row[0]['yim'];
			$this->skype			= $row[0]['skype'];
			$this->steam			= $row[0]['steam'];
			$this->facebook			= $row[0]['facebook'];
			$this->twitter			= $row[0]['twitter'];
			$this->google_plus		= $row[0]['google_plus'];
			$this->wkw				= $row[0]['wkw'];
			$this->youtube			= $row[0]['youtube'];
			$this->myspace			= $row[0]['myspace'];
			$this->signature		= $row[0]['signature'];
			if($row[0]['wid'] > 0 && count($row2) === 1) {
				$this->web_title		= $row2[0]['web_title'];
				$this->web_uri			= $row2[0]['web_uri'];
			}
			$this->slogan			= $row[0]['slogan'];
			$this->about			= $row[0]['about'];
			if($row[0]['relationship'] > 0 && count($row3) === 1) $this->relationship = $row3[0]['title'];
			else $this->relationship = 0;
			$this->fav_radio		= $row[0]['fav_radio'];
			$this->fav_dj			= $row[0]['fav_dj'];
			$this->fav_club			= $row[0]['fav_club'];
			$this->fav_genre		= $row[0]['fav_genre'];
			$this->fav_disco		= $row[0]['fav_disco'];
			$this->fav_song			= $row[0]['fav_song'];
			$this->fav_movie		= $row[0]['fav_movie'];
			$this->fav_series		= $row[0]['fav_series'];
			$this->board_posts		= $row[0]['board_posts'];
			$this->profile_edited	= $row[0]['profile_edited'];
		}
	}
	
	
	
	// 
	public function getProfile($uid = NULL) {
		if($uid === NULL) {
			$first_name		= $this->first_name;
			$second_name	= $this->second_name;
			$sur_name		= $this->sur_name;
			$userpic		= $this->userpic;
			$online			= $this->online;
			$b_day			= $this->b_day;
			$b_month		= $this->b_month;
			$b_year			= $this->b_year;
			$gender			= $this->gender;
			$nationality	= $this->nationality;
			$location		= $this->location;
			$profession		= $this->profession;
			$icq			= $this->icq;
			$msn			= $this->msn;
			$aim			= $this->aim;
			$yim			= $this->yim;
			$skype			= $this->skype;
			$steam			= $this->steam;
			$facebook		= $this->facebook;
			$twitter		= $this->twitter;
			$google_plus	= $this->google_plus;
			$wkw			= $this->wkw;
			$youtube		= $this->youtube;
			$myspace		= $this->myspace;
			$signature		= $this->signature;
			$web_title		= $this->web_title;
			$web_uri		= $this->web_uri;
			$slogan			= $this->slogan;
			$about			= $this->about;
			$relationship	= $this->relationship;
			$fav_radio		= $this->fav_radio;
			$fav_dj			= $this->fav_dj;
			$fav_club		= $this->fav_club;
			$fav_genre		= $this->fav_genre;
			$fav_disco		= $this->fav_disco;
			$fav_song		= $this->fav_song;
			$fav_movie		= $this->fav_movie;
			$fav_series		= $this->fav_series;
			$board_posts	= $this->board_posts;
			$profile_edited	= $this->profile_edited;
		} else {
			if(!ctype_digit($uid)) {
				$row = $this->eCMS->db->getArray("SELECT uid FROM {$this->eCMS->db_prefix}user WHERE seo = '".$this->eCMS->db->secureString($uid)."'");
				
				if(count($row) === 1) $uid = $row[0]['uid'];
			}
			
			$row = $this->eCMS->db->getArray("SELECT 
													first_name, 
													second_name, 
													sur_name, 
													userpic, 
													online, 
													b_day, 
													b_month, 
													b_year, 
													gender, 
													nationality, 
													location, 
													profession, 
													icq, 
													msn, 
													aim, 
													yim, 
													skype, 
													steam, 
													facebook, 
													twitter, 
													google_plus, 
													wkw, 
													youtube, 
													myspace, 
													signature, 
													wid, 
													slogan, 
													about, 
													relationship, 
													fav_radio, 
													fav_dj, 
													fav_club, 
													fav_genre, 
													fav_disco, 
													fav_song, 
													fav_movie, 
													fav_series, 
													board_posts, 
													profile_edited 
												FROM 
													{$this->eCMS->db_prefix}user_profiles 
												WHERE 
													uid = '".$this->eCMS->db->secureString($uid)."'");
			
			if(count($row) > 0) {
				if($row[0]['wid'] > 0) $row2 = $this->eCMS->db->getArray("SELECT 
																				web_title, 
																				web_uri 
																			FROM 
																				{$this->eCMS->db_prefix}user_websites 
																			WHERE 
																				wid = '".$this->eCMS->db->secureString($row[0]['wid'])."' 
																			AND 
																				uid = '".$this->eCMS->db->secureString($uid)."'");
				
				if($row[0]['relationship'] > 0) $row3 = $this->eCMS->db->getArray("SELECT 
																						title, 
																						sort 
																					FROM 
																						{$this->eCMS->db_prefix}user_relationships 
																					WHERE 
																						rid = '".$this->eCMS->db->secureString((string)$row[0]['relationship'])."'");
				
				$first_name		= $row[0]['first_name'];
				$second_name	= $row[0]['second_name'];
				$sur_name		= $row[0]['sur_name'];
				$userpic		= $row[0]['userpic'];
				$online			= $row[0]['online'];
				$b_day			= $row[0]['b_day'];
				$b_month		= $row[0]['b_month'];
				$b_year			= $row[0]['b_year'];
				$gender			= $row[0]['gender'];
				$nationality	= $row[0]['nationality'];
				$location		= $row[0]['location'];
				$profession		= $row[0]['profession'];
				$icq			= $row[0]['icq'];
				$msn			= $row[0]['msn'];
				$aim			= $row[0]['aim'];
				$yim			= $row[0]['yim'];
				$skype			= $row[0]['skype'];
				$steam			= $row[0]['steam'];
				$facebook		= $row[0]['facebook'];
				$twitter		= $row[0]['twitter'];
				$google_plus	= $row[0]['google_plus'];
				$wkw			= $row[0]['wkw'];
				$youtube		= $row[0]['youtube'];
				$myspace		= $row[0]['myspace'];
				$signature		= $row[0]['signature'];
				if($row[0]['wid'] > 0 && count($row2) > 0) {
					$web_title		= $row2[0]['web_title'];
					$web_uri		= $row2[0]['web_uri'];
				} else {
					$web_title		= '';
					$web_uri		= '';
				}
				$slogan			= $row[0]['slogan'];
				$about			= $row[0]['about'];
				if($row[0]['relationship'] > 0 && count($row3) > 0) $relationship = $row3[0]['title'];
				else $relationship = 0;
				$fav_radio		= $row[0]['fav_radio'];
				$fav_dj			= $row[0]['fav_dj'];
				$fav_club		= $row[0]['fav_club'];
				$fav_genre		= $row[0]['fav_genre'];
				$fav_disco		= $row[0]['fav_disco'];
				$fav_song		= $row[0]['fav_song'];
				$fav_movie		= $row[0]['fav_movie'];
				$fav_series		= $row[0]['fav_series'];
				$board_posts	= $row[0]['board_posts'];
				$profile_edited	= $row[0]['profile_edited'];
			} else return false;
		}
		
		$arr = array(	'first_name'		=> $first_name, 
						'second_name'		=> $second_name, 
						'sur_name'			=> $sur_name, 
						'userpic'			=> $userpic, 
						'online'			=> $online, 
						'b_day'				=> $b_day, 
						'b_month'			=> $b_month, 
						'b_year'			=> $b_year, 
						'gender'			=> $gender, 
						'nationality'		=> $nationality, 
						'location'			=> $location, 
						'profession'		=> $profession, 
						'icq'				=> $icq, 
						'msn'				=> $msn, 
						'aim'				=> $aim, 
						'yim'				=> $yim, 
						'skype'				=> $skype, 
						'steam'				=> $steam, 
						'facebook'			=> $facebook, 
						'twitter'			=> $twitter, 
						'google_plus'		=> $google_plus, 
						'wkw'				=> $wkw, 
						'youtube'			=> $youtube, 
						'myspace'			=> $myspace, 
						'signature'			=> $signature, 
						'web_title'			=> $web_title, 
						'web_uri'			=> $web_uri, 
						'slogan'			=> $slogan, 
						'about'				=> $about, 
						'relationship'		=> $relationship, 
						'fav_radio'			=> $fav_radio, 
						'fav_dj'			=> $fav_dj, 
						'fav_club'			=> $fav_club, 
						'fav_genre'			=> $fav_genre, 
						'fav_disco'			=> $fav_disco, 
						'fav_song'			=> $fav_song, 
						'fav_movie'			=> $fav_movie, 
						'fav_series'		=> $fav_series, 
						'board_posts'		=> $board_posts, 
						'profile_edited'	=> $profile_edited);
		
		return $arr;
	}
	
	public function getProfiles() {
		$ret = array();
		
		$row = $this->eCMS->db->getArray("SELECT 
												first_name, 
												second_name, 
												sur_name, 
												userpic, 
												online, 
												b_day, 
												b_month, 
												b_year, 
												gender, 
												nationality, 
												location, 
												profession, 
												icq, 
												msn, 
												aim, 
												yim, 
												skype, 
												steam, 
												facebook, 
												twitter, 
												google_plus, 
												wkw, 
												youtube, 
												myspace, 
												signature, 
												slogan, 
												about, 
												relationship, 
												fav_radio, 
												fav_dj, 
												fav_club, 
												fav_genre, 
												fav_disco, 
												fav_song, 
												fav_movie, 
												fav_series, 
												wid, 
												profile_edited 
											FROM 
												{$this->eCMS->db_prefix}user_profiles");
		
		if(count($row) > 0) $ret = $row;
		
		return $ret;
	}
	
	public function updateProfile($uid, $firstName, $secondName, $surName, $userpic, $online, $bDay, $bMonth, $bYear, $gender, $nationality, $location, $profession, $icq, $msn, $aim, $yim, $skype, $steam, $facebook, $twitter, $google_plus, $wkw, $youtube, $myspace, $slogan, $about, $signature, $relationship, $fav_radio, $fav_dj, $fav_club, $fav_genre, $fav_disco, $fav_song, $fav_movie, $fav_series) {
		if($uid !== NULL) {
			if($firstName !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET first_name = '".$this->eCMS->db->secureString((string)$firstName)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($secondName !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET second_name = '".$this->eCMS->db->secureString((string)$secondName)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($surName !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET sur_name = '".$this->eCMS->db->secureString((string)$surName)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($userpic !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET userpic = '".$this->eCMS->db->secureString((string)$userpic)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($bDay!== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET b_day = '".$this->eCMS->db->secureString((string)$bDay)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($bMonth !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET b_month = '".$this->eCMS->db->secureString((string)$bMonth)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($bYear !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET b_year = '".$this->eCMS->db->secureString((string)$bYear)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($gender !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET gender = '".$this->eCMS->db->secureString((string)$gender)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($nationality !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET nationality = '".$this->eCMS->db->secureString((string)$nationality)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($location !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET location = '".$this->eCMS->db->secureString((string)$location)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($profession !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET profession = '".$this->eCMS->db->secureString((string)$profession)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($icq !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET icq = '".$this->eCMS->db->secureString((string)$icq)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($msn !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET msn = '".$this->eCMS->db->secureString((string)$msn)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($aim !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET aim = '".$this->eCMS->db->secureString((string)$aim)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($yim !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET yim = '".$this->eCMS->db->secureString((string)$yim)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($skype !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET skype = '".$this->eCMS->db->secureString((string)$skype)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($steam !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET steam = '".$this->eCMS->db->secureString((string)$steam)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($facebook !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET facebook = '".$this->eCMS->db->secureString((string)$facebook)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($twitter !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET twitter = '".$this->eCMS->db->secureString((string)$twitter)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($google_plus !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET google_plus = '".$this->eCMS->db->secureString((string)$google_plus)."' WHERE uid = '".$this->eCMS->db->secureString((string)$google_plus)."'");
			if($wkw !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET wkw = '".$this->eCMS->db->secureString((string)$wkw)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($youtube !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET youtube = '".$this->eCMS->db->secureString((string)$youtube)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($myspace !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET myspace = '".$this->eCMS->db->secureString((string)$myspace)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($signature !== NULL) 
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET signature = '".$this->eCMS->db->secureString((string)$signature)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($slogan !== NULL)
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET slogan = '".$this->eCMS->db->secureString((string)$slogan)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($about !== NULL)
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET about = '".$this->eCMS->db->secureString((string)$about)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($relationship !== NULL)
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET relationship = '".$this->eCMS->db->secureString((string)$relationship)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
				
			if($fav_radio !== NULL)
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET fav_radio = '".$this->eCMS->db->secureString((string)$fav_radio)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($fav_dj !== NULL)
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET fav_dj = '".$this->eCMS->db->secureString((string)$fav_dj)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($fav_club !== NULL)
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET fav_club = '".$this->eCMS->db->secureString((string)$fav_club)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($fav_genre !== NULL)
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET fav_genre = '".$this->eCMS->db->secureString((string)$fav_genre)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($fav_disco !== NULL)
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET fav_disco = '".$this->eCMS->db->secureString((string)$fav_disco)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($fav_song !== NULL)
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET fav_song = '".$this->eCMS->db->secureString((string)$fav_song)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($fav_movie !== NULL)
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET fav_movie = '".$this->eCMS->db->secureString((string)$fav_movie)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
			if($fav_series !== NULL)
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET fav_series = '".$this->eCMS->db->secureString((string)$fav_series)."' WHERE uid = '".$this->eCMS->db->secureString((string)$uid)."'");
		} else return false;
	}
	
	public function getCountry($iso2 = NULL) {
		if($iso2 !== NULL) return $this->eCMS->db->getArray("SELECT country FROM {$this->eCMS->db_prefix}countries WHERE iso2 = '".$this->eCMS->db->secureString($iso2)."'");
		if($iso2 === NULL) return $this->eCMS->db->getArray("SELECT iso2, country FROM {$this->eCMS->db_prefix}countries ORDER BY country ASC");
	}
	
	public function getRelationship($rid = NULL) {
		if($rid !== NULL) return $this->eCMS->db->getArray("SELECT rid, title, sort FROM {$this->eCMS->db_prefix}user_relationships WHERE rid = '".$this->eCMS->db->secureString($rid)."'");
		if($rid === NULL) return $this->eCMS->db->getArray("SELECT rid, title, sort FROM {$this->eCMS->db_prefix}user_relationships ORDER BY sort ASC");
	}
	
	
	
	public function getWall($uid = NULL) {}
	
	public function addWallEntry($uid, $text) {}
	
	public function delWallEntry($wid) {}
	
	public function wallLike($wid, $like) {}
	
	
	
	public function getGuestbook($uid = NULL) {}
	
	public function addGuestbookEntry($uid, $text) {}
	
	public function delGuestbookEntry($wid) {}
	
	public function guestbookLike($gid, $like) {}
	
	
	
	public function __destruct() {}
}
?>