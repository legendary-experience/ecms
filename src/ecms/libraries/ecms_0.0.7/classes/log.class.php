<?php
/*******************************************************\
| Functions:											|
| - void	public	__construct(object eCMS);			|

| - void	public	__destruct();						|
\*******************************************************/

if(!defined('eCMS')) die('Hacking attempt...');

class Log {
	private $eCMS;
	
	private $logDate;
	private $logDateTime;
	private $logFile;
	private $logFileName;
	private $logString;
	
	private $logInit = false;
	
	public function __construct($eCMS) {
		$this->eCMS = $eCMS;
		
		$this->logInit = $this->initLog();
	}
	
	
	
	// Init log.
	# @param string $logFile
	# @return bool
	private function initLog($logFile = NULL) {
		if(!is_string(DIR_LOGS)) die('Hacking attempt...');
		
		$this->logDate		= date('Y-m-d', time());
		$this->logDateTime	= date('H:i:s', time());
		
		if($logFile !== NULL && is_string($logFile)) $this->logFileName = $this->logDate.'_'.$logFile.'.log';
		else $this->logFileName = $this->logDate.'_kernel.log';
		$this->logFile = str_replace('\\', '/', DIR_LOGS.'/'.$this->logFileName);
		
		if(DIR_LOGS !== '' && is_dir(DIR_LOGS)) {
			if(file_exists($this->logFile) && is_file($this->logFile)) {
				if(is_writeable($this->logFile)) {
					if(!is_writeable($this->logFile)) die("Can't open logfile in writeable-mode!<br />\r\neCMS-Kernel closed to protect the system.");
					else return true;
				} else die("Folder 'logs' is not writeable<br />\r\neCMS-Kernel closed to protect the system.");
			} else {
				file_put_contents($this->logFile, "eCMS Kernel-Logfile.\r\n\r\n", LOCK_EX);
				file_put_contents($this->logFile, "-------------------------\r\n", FILE_APPEND|LOCK_EX);
				file_put_contents($this->logFile, "eCMS Kernel: 1.0.5\r\n", FILE_APPEND|LOCK_EX);
				file_put_contents($this->logFile, "-------------------------\r\n\r\n", FILE_APPEND|LOCK_EX);
				
				return true;
			}
		} else die("Folder 'logs' doesn't exists!<br />\r\neCMS-Kernel closed to protect the system.");
	}
	
	
	
	// Set log-entry.
	# @param string	$logString
	# @param string	$type
	# @param string	$logFile
	public function setLog($logString, $type, $logFile = NULL) {
		if($this->logInit) {
			if(is_string($type)) {
				switch($type) {
					case 'verbose':
						$typeString = 'VERBOSE';
						
						break;
					
					case 'debug':
						$typeString = 'DEBUG';
						
						break;
					
					case 'info':
						$typeString = 'INFO';
						
						break;
					
					case 'notice':
						$typeString = 'NOTICE';
						
						break;
					
					case 'warning':
						$typeString = 'WARNING';
						
						break;
					
					case 'error':
						$typeString = 'ERROR';
						
						break;
					
					case 'critical':
						$typeString = 'CRITICAL';
						
						break;
					
					case 'alert':
						$typeString = 'ALERT';
						
						break;
					
					case 'emerg':
						$typeString = 'EMERG';
						
						break;
				}
				
				// Check if log-level is high enough
				if($typeString !== 'DEBUG') {
					file_put_contents($this->logFile, "[".$typeString."] ".$this->logDateTime.": ".$logString."\r\n", FILE_APPEND|LOCK_EX);
					file_put_contents($this->logFile, " Script executed by ".$_SERVER['REMOTE_ADDR'].":".$_SERVER['REMOTE_PORT']."\r\n", FILE_APPEND|LOCK_EX);
					
					file_put_contents($this->logFile, "\r\n", FILE_APPEND|LOCK_EX);
				}
			} else {
				$this->setFunctionCall(	'setLog',
												array(	0	=> array(	'title'	=> 'logString',
																		'nType'	=> 'string',
																		'gType'	=> gettype($logString),
																		'value'	=> $logString
																	),
														1	=> array(	'title'	=> 'type',
																		'nType'	=> 'string',
																		'gType'	=> gettype($type),
																		'value'	=> $type
																	),
														2	=> array(	'title'	=> 'logFile',
																		'nType'	=> 'string',
																		'gType'	=> gettype($logFile),
																		'value'	=> $logFile
																	)),
												true,
												array(	'title'	=> 'type',
														'nType'	=> 'string',
														'gType'	=> gettype($type),
														'value'	=> $type
													 )
										);
				
				file_put_contents($this->logFile, "\r\n", FILE_APPEND|LOCK_EX);
			}
		}
	}
	
	
	
	// Logs the function calls.
	# @param string	$func
	# @param array	$conditions
	# @param bool	$fail
	# @param array	$failCondition
	public function setFunctionCall($func, $conditions, $fail = false, $failCondition = NULL) {
		
	}
	
	
	
	
	
	
	
	
	/*
	// Set log-entry.
	# @param string $logString
	# @param string $type
	# @param string $logFile
	public function setLog($logString, $type, $logFile = NULL) {
		if($this->initLog($logFile)) {
			if(is_string($type)) {
				switch($type) {
					case 'verbose':
						$typeString = 'VERBOSE';
						
						break;
					
					case 'debug':
						$typeString = 'DEBUG';
						
						break;
					
					case 'info':
						$typeString = 'INFO';
						
						break;
					
					case 'notice':
						$typeString = 'NOTICE';
						
						break;
					
					case 'warning':
						$typeString = 'WARNING';
						
						break;
					
					case 'error':
						$typeString = 'ERROR';
						
						break;
					
					case 'critical':
						$typeString = 'CRITICAL';
						
						break;
					
					case 'alert':
						$typeString = 'ALERT';
						
						break;
					
					case 'emerg':
						$typeString = 'EMERG';
						
						break;
				}
				
				// Check if log-level is high enough
				if($typeString !== 'DEBUG') {
					file_put_contents($this->logFile, "[".$typeString."] ".$this->logDateTime.": ".$logString."\r\n", FILE_APPEND|LOCK_EX);
					file_put_contents($this->logFile, " Script executed by ".$_SERVER['REMOTE_ADDR'].":".$_SERVER['REMOTE_PORT']."\r\n", FILE_APPEND|LOCK_EX);
					
					file_put_contents($this->logFile, "\r\n", FILE_APPEND|LOCK_EX);
				}
			} else {
				$this->setFuncCall(	'setLog', 
									array(	0	=> array(	'title'	=> 'logString', 
															'nType'	=> 'string',
															'gType'	=> gettype($logString),
															'value'	=> $logString
														), 
											1	=> array(	'title'	=> 'type', 
															'nType'	=> 'string', 
															'gType'	=> gettype($type), 
															'value'	=> $type
														), 
											2	=> array(	'title'	=> 'type', 
															'nType'	=> 'string', 
															'gType'	=> gettype($logFile), 
															'value'	=> $logFile
														)), 
									true, 
									array(	'title'	=> 'type', 
											'nType'	=> 'string', 
											'gType'	=> gettype($type), 
											'value'	=> $type
									 )
								  );
				
				file_put_contents($this->logFile, "\r\n", FILE_APPEND|LOCK_EX);
			}
		}
	}
	
	// Logs the function calls.
	# @param string $func
	# @param array $conditions
	# @param bool $fail
	# @param array $failCondition
	public function setFuncCall($func, $conditions, $fail = false, $failCondition = NULL) {
		$this->setLog('Call setFuncCall.', 'debug');
		$error = false;
		
		if(!is_string($func)) { 
			$error = true;
			$this->setFuncCall(	'setFuncCall', 
								array(	0	=> array(	'title'	=> 'func', 
														'nType'	=> 'string', 
														'gType' => gettype($func),
														'value' => $func
													), 
										1	=> array(	'title'	=> 'conditions', 
														'nType'	=> 'array', 
														'gType' => gettype($conditions),
														'value' => $conditions
													), 
										2	=> array(	'title'	=> 'fail', 
														'nType'	=> 'bool', 
														'gType' => gettype($fail),
														'value' => $fail
													), 
										3	=> array(	'title'	=> 'failCondition', 
														'nType'	=> 'array', 
														'gType' => gettype($failCondition),
														'value' => $failCondition
													)
									 ),
								true, 
								array(	'title'	=> 'func', 
										'nType'	=> 'string', 
										'gType'	=> gettype($func), 
										'value'	=> $func
									 )
							  );
		}
		if(!is_array($conditions)) {
			$error = true;
			$this->setFuncCall(	'setFuncCall', 
								array(	0	=> array(	'title'	=> 'func', 
														'nType'	=> 'string', 
														'gType' => gettype($func),
														'value' => $func
													), 
										1	=> array(	'title'	=> 'conditions', 
														'nType'	=> 'array', 
														'gType' => gettype($conditions),
														'value' => $conditions
													), 
										2	=> array(	'title'	=> 'fail', 
														'nType'	=> 'bool', 
														'gType' => gettype($fail),
														'value' => $fail
													), 
										3	=> array(	'title'	=> 'failCondition', 
														'nType'	=> 'array', 
														'gType' => gettype($failCondition),
														'value' => $failCondition
													)
									 ), 
								true, 
								array(	'title'	=> 'func', 
										'nType'	=> 'array', 
										'gType'	=> gettype($conditions), 
										'value'	=> $conditions
									 )
							  );
		}
		if(!is_bool($fail)) {
			$error = true;
			$this->setFuncCall(	'setFuncCall', 
								array(	0	=> array(	'title'	=> 'func', 
														'nType'	=> 'string', 
														'gType' => gettype($func),
														'value' => $func
													), 
										1	=> array(	'title'	=> 'conditions', 
														'nType'	=> 'array', 
														'gType' => gettype($conditions),
														'value' => $conditions
													), 
										2	=> array(	'title'	=> 'fail', 
														'nType'	=> 'bool', 
														'gType' => gettype($fail),
														'value' => $fail
													), 
										3	=> array(	'title'	=> 'failCondition', 
														'nType'	=> 'array', 
														'gType' => gettype($failCondition),
														'value' => $failCondition
													)
									 ), 
								true, 
								array(	'title'	=> 'func', 
										'nType'	=> 'boolean', 
										'gType'	=> gettype($fail), 
										'value'	=> $fail
									 )
							  );
		}
		if(!is_array($failCondition) && $failCondition != NULL) {
			$error = true;
			$this->setFuncCall(	'setFuncCall', 
								array(	0	=> array(	'title'	=> 'func', 
														'nType'	=> 'string', 
														'gType' => gettype($func),
														'value' => $func
													), 
										1	=> array(	'title'	=> 'conditions', 
														'nType'	=> 'array', 
														'gType' => gettype($conditions),
														'value' => $conditions
													), 
										2	=> array(	'title'	=> 'fail', 
														'nType'	=> 'bool', 
														'gType' => gettype($fail),
														'value' => $fail
													), 
										3	=> array(	'title'	=> 'failCondition', 
														'nType'	=> 'array', 
														'gType' => gettype($failCondition),
														'value' => $failCondition
													)
									 ), 
								true, 
								array(	'title'	=> 'func', 
										'nType'	=> 'array', 
										'gType'	=> gettype($failCondition), 
										'value'	=> $failCondition
									 )
							  );
		}
		
		if($error === false) {
			if($fail === true) {
				file_put_contents($this->logFile, "[ALERT] ".$this->logDateTime."\r\n", FILE_APPEND|LOCK_EX);
				file_put_contents($this->logFile, "Called function ".$func." with fail conditions.\r\n", FILE_APPEND|LOCK_EX);
				file_put_contents($this->logFile, "Condition: \$".$failCondition['title']."\r\n", FILE_APPEND|LOCK_EX);
				file_put_contents($this->logFile, "Needed type: ".$failCondition['nType']."\r\n", FILE_APPEND|LOCK_EX);
				file_put_contents($this->logFile, "Given type: ".$failCondition['gType']."\r\n\r\n", FILE_APPEND|LOCK_EX);
				file_put_contents($this->logFile, "Given value: ".$failCondition['value']."\r\n\r\n", FILE_APPEND|LOCK_EX);
				
				for($i = 0; $i < count($conditions); $i++) {
					file_put_contents($this->logFile, " - @param ".$conditions[$i]['gType']." (".$conditions[$i]['nType'].") \$".$conditions[$i]['title'].": ".$conditions[$i]['value']."\r\n", FILE_APPEND|LOCK_EX);
				}
				
				file_put_contents($this->logFile, " Script executed by ".$_SERVER['REMOTE_ADDR'].":".$_SERVER['REMOTE_PORT']."\r\n", FILE_APPEND|LOCK_EX);
			} else {
				
			}
		}
		
		file_put_contents($this->logFile, "\r\n", FILE_APPEND|LOCK_EX);
	}
	*/
	
	
	
	public function __destruct() {}
}
?>