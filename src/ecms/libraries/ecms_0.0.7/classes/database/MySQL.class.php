<?php
if(!defined('eCMS')) die('Hacking attempt...');

class MySQL {
	private $eCMS;				// Holds the eCMS-class.
	private $dbSettings;		// Holds the database-settings.
	
	private $hostname;			// Holds the ip/hostname.
	private $username;			// Holds the username.
	private $password;			// Holds the password.
	private $database;			// Holds the database-name.
	private $port;				// Holds the port.
	
	private $connected = false;	// Connected? ;)
	
	private $connection;		// Holds the connect as self.
	private $sql_query;			// Holds the sql-query.
	private $query_counter;		// Count the queries.
	private $result;			// Holds the query-result.
	
	public $resource;			// Holds the public resource.
	
	// The constructor.
	# @param int $id
	# 
	# @return bool
	public function __construct($eCMS, $settings, $id = DB_DEFAULT_01) {
		$this->eCMS = $eCMS;
		$this->dbSettings = $settings;
		
		if(is_int($id)) {
			$this->hostname = $this->dbSettings[$id]['host'];
			$this->username = $this->dbSettings[$id]['user'];
			$this->password = $this->dbSettings[$id]['pass'];
			$this->database = $this->dbSettings[$id]['db'];
			$this->port		= $this->dbSettings[$id]['port'];
			
			$this->connection = new mysqli($this->hostname, $this->username, $this->password, $this->database, $this->port);
			
			if(mysqli_connect_error()) {
				$this->error('CONN.OPEN', mysqli_connect_error());
				
				return false;
			}
			
			return true;
			
			/*
			$this->connection = @mysqli_connect($this->hostname.':'.$this->port, $this->username, $this->password, false, 0);
			
			if(is_resource($this->connection)) {
				if(@mysql_select_db($this->database, $this->connection) === false) $this->error("DB.SELECT", mysql_error());
				else $this->connected = true;
				
				return true;
			}
			if(!is_resource($this->connection)) {
				echo "test: '".mysqli_error($this->connection)."'\r\n";
				#$this->log->setLog('Can\'t connect to database.', 'error');
				$this->error('CONN.OPEN', mysqli_error($this->connection));
				
				return false;
			}
			*/
		}
		
		return false;
	}
	
	// The destructor.
	# @return void
	public function __destruct() {
		//$this->connection->close();
	}
	
	
	
	// Returns the count affected rows. (mysql_affected_rows)
	# @return int
	public function getAffectedRows() {
		if($this->connected === true) return mysql_affected_rows($this->connection);
		else $this->error('CONN.NO', NULL);
	}
	
	// Returns the encoding. (mysql_client_encoding)
	# @return string
	public function getClientEncoding() {
		if($this->connected === true) return mysql_client_encoding($this->connection);
		else $this->error('CONN.NO', NULL);
	}
	
	// Handles database creation. (mysql_create_db)
	# @param string $database
	# 
	# @return bool
	public function createDatabase($database) {}
	
	// Handles database dropping. (mysql_drop_db)
	# @param string $database
	# 
	# @return bool
	public function dropDatabase($database) {}
	
	// mysql_fetch_array
	# @param string $query
	# @param int $type
	# 
	# @return array
	public function getArray($query, $type = MYSQLI_BOTH) {
		if($this->query($query) === true) {
			$count = mysqli_num_rows($this->result);
			$array = array();
			
			for($i = 0; $i < $count; $i++) {
				$array[$i] = $this->result->fetch_array($type);
			}
			
			return $array;
		} else return false;
	}
	
	// Execute query. (mysql_query)
	# @param string $sql
	# 
	# @return bool
	public function query($sql) {
		if(is_string($sql)) {
			$this->sql_query = $sql;
			$this->query_counter++;
			$this->result = $this->connection->query($this->sql_query);
			
			if($this->result === false) { $this->error('QUERY.FAILED', NULL); return false; }
			else return true;
		} else return false;
	}
	
	// Returns the query counter.
	# @return int
	public function getDbQueries() {
		if($this->connected === true) return $this->query_counter;
		else $this->error('CONN.NO', NULL);
	}
	
	// Makes full log-string.
	# @return string
	public function drawDbQueries() {
		if($this->connected === true) {
			if($this->getDbQueries == 1) $text = "Script executed with 1 database-request.";
			else $text = "Script executed with ".$this->DBQueries." database-requests.";
			
			$this->log->setLog($text, 'debug');
			return $text;
		} else $this->error('CONN.NO', NULL);
	}
	
	// Secure a string for query using. (mysql_real_escape_string)
	# @param string $unescapedString
	# 
	# @return string
	public function secureString($unescapedString) {
		if(!is_string($unescapedString)) $this->eCMS->dieFunctionCall('secureString', 'unescapedString', gettype($unescapedString), 'string');
		
		if(is_string($unescapedString)) {
			$escapedString = '';
			$temp = $unescapedString;
			
			$temp = $this->connection->real_escape_string($temp);
			
			$escapedString = $temp;
			
			return $escapedString;
		} else return false;
	}
	
	// Secure a string for query using. (mysql_real_escape_string)
	# @param int $unescapedString
	# 
	# @returnstring
	public function secureInt($unescapedString) {
		if(!is_int($unescapedString)) $this->eCMS->dieFunctionCall('secureInt', 'unescapedString', gettype($unescapedString), 'integer');
		
		if(is_int($unescapedString)) {
			$escapedString = '';
			$temp = $unescapedString;
			
			$temp = mysql_real_escape_string($temp);
			
			$escapedString = $temp;
			
			return $escapedString;
		} else return false;
	}
	
	
	
	// Error-Handler
	# @param string $error_type
	# @param string $error
	private function error($error_type, $error = NULL) {
		if(is_string($error_type)) {
			switch($error_type) {
				case 'CONN.OPEN':
					$text = "Error while opening database-connection.\r\n".$error;
					
					break;
				
				case 'CONN.CLOSE':
					$text = "Error while closing database-connection.\r\n".$error;
					
					break;
				
				case 'CONN.NO':
					$text = "Try to execute database-command, but database isn\'t connected!";
					
					break;
				
				case 'DB.SELECT':
					$text = "Error while selecting the database (Database exists?).";
					
					break;
				
				case 'QUERY.FAILED':
					$text = "Error while query:\r\n".$this->sql_query;
					
					break;
				
				default:
					$text = "Database-Error:\r\n".$error_type;
					
					break;
			}
			
			if($this->connected === true) $text .= "\r\nDatabase-Error (MySQL):\r\nError No.: ".@mysql_errno($this->connection)."\r\nError Description: ".@mysql_error($this->connection);
			
			#$this->log->setLog($text, 'error');
			echo $text;
		} else return false;
	}
}
?>