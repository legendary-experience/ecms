<?php
/***********************************************************\
| Functions:												|
| - void	public	__construct(string type);				|
| - int		public	getAffectedRows();						|
| - string	public	getClientEncoding();					|
| - bool	public	createDatabase(string database);		|
| - bool	public	dropDatabase(string database);			|
| - array	public	getArray(string query,					|
|							 int type = MYSQL_BOTH)			|
| - bool	public	query(string sql)						|
| - int		public	getDbQueries()							|
| - string	public	drowDbQueries()							|
| - string	public	secureString(string unescapedString)	|
| - int		public	secureInt(int unescapedString)			|
\***********************************************************/

if(!defined('eCMS')) die('Hacking attempt...');

class Database {
	private $eCMS;
	private $type;
	private $db;
	
	// The constructor.
	# @param string $type
	public function __construct($eCMS, $settings, $type, $id = DB_DEFAULT_01) {
		$this->eCMS = $eCMS;
		
		if(is_string($type)) {
			$this->type = $type;
			
			switch($this->type) {
				case 'mysql':
					
					require_once(DIR_LIB_ECMS.'classes/database/MySQL.class.php');
					
					$this->db = new MySQL($this->eCMS, $settings, $id);
					
					return $this->db;
					
					break;
				
			}
			
			return false;
		} else return false;
	}
	
	
	
	// Returns the count affected rows. (mysql_affected_rows)
	# @return int
	public function getAffectedRows() {
		return $this->db->getAffectedRows();
	}
	
	// Returns the encoding. (mysql_client_encoding)
	# @return string
	public function getClientEncoding() {
		return $this->db->getClientEncoding();
	}
	
	// Handles database creation. (mysql_create_db)
	# @param string $database
	# 
	# @return bool
	public function createDatabase($database) {
		return $this->db->createDatabase($database);
	}
	
	// Handles database dropping. (mysql_drop_db)
	# @param string $database
	# 
	# @return bool
	public function dropDatabase($database) {
		return $this->db->dropDatabase($database);
	}
	
	// mysql_fetch_array
	# @param string $query
	# @param int $type
	# 
	# @return array
	public function getArray($query, $type = MYSQLI_BOTH) {
		return $this->db->getArray($query, $type);
	}
	
	// Execute query. (mysql_query)
	# @param string $sql
	# 
	# @return bool
	public function query($sql) {
		return $this->db->query($sql);
	}
	
	// Returns the query counter.
	# @return int
	public function getDbQueries() {
		return $this->db->getDbQueries();
	}
	
	// Makes full log-string.
	# @return string
	public function drawDbQueries() {
		return $this->db->drawDbQueries();
	}
	
	// Secure a string for query using. (mysql_real_escape_string)
	# @param string $unescapedString
	# 
	# @return string
	public function secureString($unescapedString) {
		return $this->db->secureString($unescapedString);
	}
	
	// Secure a string for query using. (mysql_real_escape_string)
	# @param int $unescapedString
	# 
	# @returnstring
	public function secureInt($unescapedString) {
		return $this->db->secureInt($unescapedString);
	}
	
	
	
	// The destructor.
	# @return void
	public function __destruct() {
		$this->db->__destruct();
	}
}
?>