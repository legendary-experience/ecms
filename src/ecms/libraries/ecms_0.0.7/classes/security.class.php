<?php
/*******************************************************\
| Functions:											|
| - void	public	__construct(object eCMS);			|
| - string	private	saltPassword(string $pass,			|
|								 string $salt);			|
| - string	public	genPassHash(string $pass,			|
|								string $salt);			|
| - bool	private	checkSession();						|
| - bool	private	checkCookie();						|
| - bool	private	checkHostname();					|
| - bool	private	checkIP();							|
| - bool	private	checkMacAddr();						|
| - bool	private	checkLoginState();					|
| - bool	private	checkLogin();						|
| - bool	public	login(string $email,				|
|						  string $password);			|
| - bool	public	logout();							|
| - void	private	doLogin(string $uid,				|
|							string $session_id,			|
|							string $auto_login = NULL);	|
| - void	private	doLogout()							|
| - void	private	autoLogout();						|
| - string	public	register(string $ual,				|
|							 string $username,			|
|							 string $password,			|
|							 string $email,				|
|							 string $secretQuestion,	|
|							 string $secretAnswer,		|
|							 string $firstName,			|
|							 string $surName,			|
|							 string $bDay,				|
|							 string $bMonth,			|
|							 string $bYear,				|
|							 string $gender,			|
|							 string $nationality,		|
|							 string $location,			|
|							 string $profession);		|
| - bool	public	activate(string $email,				|
|							 string $key);				|
| - void	public	__destruct();						|
\*******************************************************/

if(!defined('eCMS')) die('Hacking attempt...');

class Security {
	private $eCMS;
	
	public function __construct($eCMS) {
		$this->eCMS = $eCMS;
	}
	
	
	
	// Generate Password-Salt.
	# @param string	$pass
	# @param string	$salt
	# 
	# @return string
	private function saltPassword($pass, $salt) {
		if(!is_string($pass)) $this->eCMS->dieFunctionCall('saltPassword', 'pass', gettype($pass), 'string');
		if(!is_string($salt)) $this->eCMS->dieFunctionCall('saltPassword', 'salt', gettype($salt), 'string');
		
		return hash('sha256', $pass.$salt);
	}
	// Generate Password-Hash.
	# @param string $pass
	# @param string $salt
	# 
	# @return string
	public function genPassHash($pass, $salt) {
		if(!is_string($pass)) $this->eCMS->dieFunctionCall('genPassHash', 'pass', gettype($pass), 'string');
		if(!is_string($salt)) $this->eCMS->dieFunctionCall('genPassHash', 'salt', gettype($salt), 'string');
		
		// Define variables to prevent errors.
		$passHash = ''; $saltPassHash = ''; $saltedPassHash = '';
		
		$passHash = md5(sha1($pass));
		$saltPassHash = $this->saltPassword($passHash, $salt);
		$saltedPassHash = sha1($saltPassHash);
		
		return $saltedPassHash;
	}
	
	
	
	// Check Session
	# @return boolean
	private function checkSession() {
		if(isset($_SESSION['eCMSuserUID']) && is_int((int)$_SESSION['eCMSuserUID'])) {
			$check = $this->eCMS->db->getArray("SELECT 
													session_id 
												FROM 
													{$this->eCMS->db_prefix}user 
												WHERE 
													uid = '".$this->eCMS->db->secureString($_SESSION['eCMSuserUID'])."' 
												AND 
													session_id = '".$this->eCMS->db->secureString(session_id())."'");
			if(count($check) === 1) return true;
			else return false;
		} else return false;
	}
	// Check Cookie
	# @return boolean
	private function checkCookie() {
		if(isset($_COOKIE['eCMSuserSessionID']) && is_string($_SESSION['eCMSuserUID']) && session_id() == $_COOKIE['eCMSuserSessionID']) return true;
		else return false;
	}
	// Check Hostname
	# @return boolean
	private function checkHostname() {}
	// Check IP
	# @return boolean
	private function checkIP() {}
	// Check Mac-Address
	# @return boolean
	private function checkMacAddr() {}
	// Check Login-State
	# @return boolean
	private function checkLoginState() {
		if(isset($_SESSION['eCMSuserLogin']) || isset($_COOKIE['eCMSuserLogin'])) {
			if(isset($_SESSION['eCMSuserUID']) && $_SESSION['eCMSuserUID'] !== '' && is_int((int)$_SESSION['eCMSuserUID'])) {
				if(count($this->eCMS->db->getArray("SELECT online FROM {$this->eCMS->db_prefix}user WHERE uid = '".$this->eCMS->db->secureString($_SESSION['eCMSuserUID'])."' AND online = 'TRUE'"))) return true;
				else return false;
			} else return false;
		} else return false;
	}
	// Check Login
	# @return boolean
	public function checkLogin() {
		if(isset($_SESSION['eCMSuser']) && $_SESSION['eCMSuser'] === true || isset($_COOKIE['eCMSuser']) && $_COOKIE['eCMSuser'] === 'true') {
			#if($this->checkSession() == true && $this->checkCookie() == true && $this->checkLoginState() == true) return true;
			if($this->checkSession() == true && $this->checkLoginState() == true) return true;
			else return false;
		} else return false;
	}
	
	
	
	// The external login-function
	# @param string	$email
	# @param string	$password
	# 
	# @return boolean
	public function login($email, $password, $autoLogin = false) {
		if(!is_string($email))		$this->eCMS->dieFunctionCall('login', 'email',		gettype($email),	'string');
		if(!is_string($password))	$this->eCMS->dieFunctionCall('login', 'password',	gettype($password),	'string');
		
		$row = $this->eCMS->db->getArray("SELECT salt FROM {$this->eCMS->db_prefix}user WHERE email = '".$this->eCMS->db->secureString($email)."' LIMIT 1", MYSQLI_ASSOC);
		if(count($row) === 1) {
			$row = $row[0];
			
			// Read out  the salt and generate the salted password-hash
			$salt = $row['salt'];
			$saltedPassHash = $this->genPassHash($password, $salt);
			
			// If username and password matches ...
			$row = $this->eCMS->db->getArray("SELECT uid, username, activated, disabled FROM {$this->eCMS->db_prefix}user WHERE email = '".$this->eCMS->db->secureString($email)."' AND password = '$saltedPassHash'");
			// ... login the user ...
			if(count($row) === 1) {
				$row = $row[0];
				
				// ... but check activated and disabled first!
				if($row['disabled'] === 'TRUE') return 'disabled';
				elseif($row['activated'] === 'FALSE') return 'activated';
				else {
					// Now it can be login.
					if($this->doLogin($row['uid'], session_id(), $autoLogin) === true) return  true;
					else return 'int_error';
				}
			} else return false;
		} else return false;
	}
	
	// The external logout-function
	# @return boolean
	public function logout() {
		$this->doLogout();
		
		return true;
	}
	
	// Login the user ...
	# @return void
	private function doLogin($uid, $session_id, $auto_login = NULL) {
		$q1 = false;
		$q2 = false;
		
		// ... with storing the session id in the database.
		$q1 = $this->eCMS->db->query("UPDATE 
											{$this->eCMS->db_prefix}user 
										SET 
											session_id = '".$this->eCMS->db->secureString(session_id())."', 
											auto_login = NULL, 
											ip = '".$this->eCMS->db->secureString($_SERVER['REMOTE_ADDR'])."', 
											port = '".$this->eCMS->db->secureString($_SERVER['REMOTE_PORT'])."', 
											hostname = '".$this->eCMS->db->secureString(gethostbyaddr($_SERVER['REMOTE_ADDR']))."', 
											user_agent = '".$this->eCMS->db->secureString($_SERVER['HTTP_USER_AGENT'])."', 
											last_login = '".time()."', 
											last_action = '".time()."', 
											online = 'TRUE' 
										WHERE 
											uid = '".$this->eCMS->db->secureString($uid)."'");
		$q2 = $this->eCMS->db->query("UPDATE 
											{$this->eCMS->db_prefix}user_profiles 
										SET 
											online = '1' 
										WHERE 
											uid = '".$this->eCMS->db->secureString($uid)."'");
		
		// Save userdata in cookies ...
		setcookie('eCMSuser',			'true',			time()+60*60*24*356*10, '', '', false, false);
		setcookie('eCMSuserUID',		$uid,			time()+60*60*24*356*10, '', '', false, false);
		setcookie('eCMSuserLogin',		'true',			time()+60*60*24*356*10, '', '', false, false);
		setcookie('eCMSuserAutoLogin',	'',				time()+60*60*24*356*10, '', '', false, false);
		setcookie('eCMSuserSessionID',	$session_id,	time()+60*60*24*356*10, '', '', false, false);
		
		// ... and sessions
		$_SESSION['eCMSuser']		= true;
		
		$_SESSION['eCMSuserUID']	= $uid;
		$_SESSION['eCMSuserLogin']	= true;
		
		$_SESSION['eCMSuserChat']	= false;
		
		if($auto_login) {
			// Random-number generation
			$part1		= substr(time() - rand(100, 1000000), 5, 10);
			$part2		= substr(time() - rand(100, 1000000), -5);
			$loginID	= md5($part1.$part2);
			
			// Save the loginID into cookie. 10 years seems to be enough ;)
			setcookie('eCMSuserAutoLogin', $loginID, time()+60*60*24*356*10, '', '', false, false);
			
			$this->eCMS->db->query("UPDATE 
										{$this->eCMS->db_prefix}user 
									SET 
										auto_login= '".$loginID."' 
									WHERE 
										uid = '".$uid."'");
		}
		
		if($q1 === true && $q2 === true) return true;
		else return false;
	}
	
	// Logout the user ...
	private function doLogout() {
		// ... with cookie and ...
		setcookie('eCMSuser',			'true',		time()+60*60*24*356*10, '', '', false, false);
		setcookie('eCMSuserLogin',		'false',	time()+60*60*24*356*10, '', '', false, false);
		setcookie('eCMSuserAutoLogin',	'',			time()+60*60*24*356*10, '', '', false, false);
		
		// ... sessions ...
		$_SESSION['eCMSuser']		= true;
		$_SESSION['eCMSuserLogin']	= false;
		$_SESSION['eCMSuserChat']	= false;
		
		// ... and database
		$this->eCMS->db->query("UPDATE 
									{$this->eCMS->db_prefix}user 
								SET 
									online = 'FALSE', 
									session_id = NULL, 
									auto_login = NULL 
								WHERE 
									uid ='".$this->eCMS->db->secureString($_SESSION['eCMSuserUID'])."'");
		$this->eCMS->db->query("UPDATE 
									{$this->eCMS->db_prefix}user_profiles 
								SET 
									online = 'FALSE' 
								WHERE 
									uid = '".$this->eCMS->db->secureString($_SESSION['eCMSuserUID'])."'");
		
		// ... and cookie.
		if(isset($_COOKIE['eCMSuserAutoLogin'])) setcookie('eCMSuserAutoLogin', '', time()+60*60*24*356*10, '', '', false, false);
	}
	
	// AutoLogout for user
	public function autoLogout() {
		// Check if user needs to autologout.
		if(isset($_COOKIE['eCMSuserAutoLogin']) && !isset($_SESSION['eCMSuserUID'])) {
			$row = $this->eCMS->db->getArray("SELECT uid FROM {$this->eCMS->db_prefix}user WHERE auto_login = '".$this->eCMS->db->secureString($_COOKIE['eCMSuserAutoLogin'])."'");
			
			if(count($row) == 1) $this->doLogin($row[0]['uid'], session_id(), $_COOKIE['eCMSuserAutoLogin']);
		}
		
		// Update online-state
		if($this->checkLogin() == true) {
			$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user SET last_action = '".time()."' WHERE uid = '".$this->eCMS->db->secureString($_SESSION['eCMSuserUID'])."'");
			$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET online = '1' WHERE uid = '".$this->eCMS->db->secureString($_SESSION['eCMSuserUID'])."'");
		}
		
		// Logout user without autoLogin
		$row = $this->eCMS->db->getArray("SELECT uid FROM {$this->eCMS->db_prefix}user WHERE '".(time() - 60)."' > last_action AND auto_login = 'NULL'");
		
		for($i = 0; $i < count($row); $i++) {
			$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user SET online = 'FALSE', session_id = NULL WHERE uid = '".$row[$i]['uid']."'");
			$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user_profiles SET online = '0' WHERE uid = '".$row[$i]['uid']."'");
		}
		
		// Check if auto logged out user has a session
		if(isset($_SESSION['eCMSuserUID']) && $_SESSION['eCMSuserUID'] != '') {
			$row = $this->eCMS->db->getArray("SELECT session_id FROM {$this->eCMS->db_prefix}user WHERE uid = '".$this->eCMS->db->secureString($_SESSION['eCMSuserUID'])."'");
			
			if(!$row[0]['session_id']) $this->doLogout();
		}
	}
	
	
	
	// Register the user
	public function register($ual, $username, $password, $email, $secretQuestion, $secretAnswer, $firstName, $surName, $bDay, $bMonth, $bYear, $gender, $nationality, $location, $profession) {
		if(!is_int($ual))				$this->eCMS->dieFunctionCall('register', 'ual',				gettype($ual),				'integer');
		if(!is_string($username))		$this->eCMS->dieFunctionCall('register', 'username',		gettype($username),			'string');
		if(!is_string($password))		$this->eCMS->dieFunctionCall('register', 'password',		gettype($password),			'string');
		if(!is_string($email))			$this->eCMS->dieFunctionCall('register', 'email',			gettype($email),			'string');
		if(!is_string($secretQuestion))	$this->eCMS->dieFunctionCall('register', 'secretQuestion',	gettype($secretQuestion),	'string');
		if(!is_string($secretAnswer))	$this->eCMS->dieFunctionCall('register', 'secretAnswer',	gettype($secretAnswer),		'string');
		if(!is_string($firstName))		$this->eCMS->dieFunctionCall('register', 'firstName',		gettype($firstName),		'string');
		if(!is_string($surName))		$this->eCMS->dieFunctionCall('register', 'surName',			gettype($surName),			'string');
		if(!is_int($bDay))				$this->eCMS->dieFunctionCall('register', 'bDay',			gettype($bDay),				'integer');
		if(!is_int($bMonth))			$this->eCMS->dieFunctionCall('register', 'bMonth',			gettype($bMonth),			'integer');
		if(!is_int($bYear))				$this->eCMS->dieFunctionCall('register', 'bYear',			gettype($bYear),			'integer');
		if(!is_int($gender))			$this->eCMS->dieFunctionCall('register', 'gender',			gettype($gender),			'integer');
		if(!is_string($nationality))	$this->eCMS->dieFunctionCall('register', 'nationality',		gettype($nationality),		'string');
		if(!is_string($location))		$this->eCMS->dieFunctionCall('register', 'location',		gettype($location),			'string');
		if(!is_string($profession))		$this->eCMS->dieFunctionCall('register', 'profession',		gettype($profession),		'string');
		
		// Examination of the various required fields
		// All UIDs, usernames, emails and salts for comparison from the database fetch.
		$uids = array();
		$usernames = array();
		$emails = array();
		$salts = array();
		
		$row = $this->eCMS->db->getArray("SELECT uid, username, email, salt FROM {$this->eCMS->db_prefix}user", MYSQL_ASSOC);
		for($i = 0; $i < count($row); $i++) {
			$uids[] = $row[$i]['uid'];
			$usernames[] = $row[$i]['username'];
			$emails[] = $row[$i]['email'];
			$salts[] = $row[$i]['salt'];
		}
		
		// Generate new UID.
		$finished = false;
		while($finished == false) {
			$uid = mt_rand(1000000, 9999999);
			
			if(in_array($uid, $uids)) $finished = false;
			else $finished = true;
		}
		
		// Generate new salt.
		$finished = false;
		while($finished == false) {
			$salt = mt_rand(1000000, 9999999);
			
			if(in_array($salt, $salts)) $finished = false;
			else $finished = true;
		}
		
		// Generate new activation-key.
		$finished = false;
		while($finished == false) {
			$actkey = mt_rand(1000000, 9999999);
			
			if(in_array($salt, $salts)) $finished = false;
			else $finished = true;
		}
		
		$saltedPassHash = $this->genPassHash($password, (string)$salt); // Generate salted-passhash.
		$activationKey = md5($actkey); // Decode activation-key.
		
		
		
		// Now the last things...
		$this->eCMS->db->query("INSERT INTO {$this->eCMS->db_prefix}user (
																			uid, 
																			ual, 
																			username, 
																			password, 
																			salt, 
																			email, 
																			registered, 
																			activated, 
																			activation_key, 
																			disabled, 
																			language, 
																			last_login, 
																			last_action, 
																			online, 
																			auto_login, 
																			session_id, 
																			ip, 
																			port, 
																			hostname, 
																			user_agent, 
																			secret_question, 
																			secret_answer, 
																			chat_room_id, 
																			chat_last_action, 
																			chat_last_msg
																		) VALUES (
																			'$uid', 
																			'$ual', 
																			'".$this->eCMS->db->secureString($username)."', 
																			'".$saltedPassHash."', 
																			'".$salt."', 
																			'".$this->eCMS->db->secureString($email)."', 
																			'".time()."', 
																			'FALSE', 
																			'$activationKey', 
																			'FALSE', 
																			NULL, 
																			NULL, 
																			NULL, 
																			'FALSE', 
																			NULL, 
																			NULL, 
																			NULL, 
																			NULL, 
																			NULL, 
																			NULL, 
																			NULL, 
																			NULL, 
																			NULL, 
																			NULL, 
																			NULL
																		)");
		
		$this->eCMS->db->query("INSERT INTO {$this->eCMS->db_prefix}user_profiles (
																					uid, 
																					first_name, 
																					second_name, 
																					sur_name, 
																					userpic, 
																					online, 
																					b_day, 
																					b_month, 
																					b_year, 
																					gender, 
																					nationality, 
																					location, 
																					profession, 
																					icq, 
																					msn, 
																					aim, 
																					yim, 
																					skype, 
																					steam, 
																					facebook, 
																					twitter, 
																					wkw, 
																					youtube, 
																					myspace, 
																					signature, 
																					wid, 
																					profile_edited
																				) VALUES (
																					'$uid', 
																					'".$this->eCMS->db->secureString($firstName)."', 
																					NULL, 
																					'".$this->eCMS->db->secureString($surName)."', 
																					NULL, 
																					'FALSE', 
																					'".$this->eCMS->db->secureInt($bDay)."', 
																					'".$this->eCMS->db->secureInt($bMonth)."', 
																					'".$this->eCMS->db->secureInt($bYear)."', 
																					'".$this->eCMS->db->secureInt($gender)."', 
																					'".$this->eCMS->db->secureString($nationality)."', 
																					'".$this->eCMS->db->secureString($location)."', 
																					'".$this->eCMS->db->secureString($profession)."', 
																					NULL, 
																					NULL, 
																					NULL, 
																					NULL, 
																					NULL, 
																					NULL, 
																					NULL, 
																					NULL, 
																					NULL, 
																					NULL, 
																					NULL, 
																					NULL, 
																					NULL, 
																					'FALSE'
																				)");
		
		$widgetArr = $this->eCMS->db->getArray("SELECT wid FROM {$this->eCMS->db_prefix}widgets WHERE 'default' = 'TRUE'");
		
		for($i = 0; $i < count($widgetArr); $i++) {
			$this->eCMS->db->query("INSERT INTO {$this->eCMS->db_prefix}user_widgets (
																						wid, 
																						uid, 
																						sort
																					) VALUES (
																						'".$this->eCMS->db->secureString($widgetArr[$i]['wid'])."', 
																						'$uid', 
																						'$i'
																					)");
		}
		
		$this->eCMS->db->query("INSERT INTO {$this->eCMS->db_prefix}auth_permissions_user (
																							uid, 
																							groups
																						) VALUES (
																							'$uid', 
																							'6'
																						)");
		
		return $uid;
	}
	
	// Activate the user.
	# @param string	$email
	# @param string $key
	# 
	# @return boolean
	public function activate($email, $key) {
		$row = $this->eCMS->db->getArray("SELECT uid FROM {$this->eCMS->db_prefix}user WHERE email = '".$this->eCMS->db->secureString($email)."' AND activation_key = '".$this->eCMS->db->secureString($key)."'");
		
		if(count($row) === 1) {
			$uid = $row[0]['uid'];
			
			$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}user SET activated = 'TRUE', activation_key = NULL WHERE uid = '$uid'");
			
			return true;
		} else return false;
	}
	
	
	
	
	
	
	
	public function __destruct() {}
}
?>