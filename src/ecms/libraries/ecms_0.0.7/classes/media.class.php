<?php
/*******************************************************\
| Functions:											|
| - void	public	__construct(object eCMS);			|
| - bool	public	loadMedia(string $mid);				|
| - void	public	showMedia();						|
| - bool	public	setMedia(string $mid,				|
|							 string $name,				|
|							 string $mime,				|
|							 integer $size,				|
|							 string $type,				|
|							 boolean $tmp = false);		|
| - void	private	getApplication();					|
| - void	private	setApplication();					|
| - void	private	displayApplication(string $mime);	|
| - void	private	getAudio(string $mime);				|
| - void	private	setAudio();							|
| - void	private	displayAudio(string $mime);			|
| - void	private	getImage(string $mime);				|
| - void	public	setImage(string $mid,				|
|							 boolean $tmp = false);		|
| - void	public	setCrop(string $mid,				|
|							string $x,					|
|							string $y,					|
|							string $x2,					|
|							string $y2,					|
|							string $w,					|
|							string $h);					|
| - void	private	getCrop(string $mid);				|
| - void	private	applyBorder();						|
| - void	private	applyWatermark();					|
| - void	private	displayImage(string $mime);			|
| - void	private	getText(string $mime);				|
| - void	private	setText();							|
| - void	private	displayText(string $mime);			|
| - void	private	getVideo(string $mime);				|
| - void	private	setVideo();							|
| - void	private	displayVideo(string $mime);			|
| - void	private	error(string $errorString,			|
|						  string $bg = '255,0,0',		|
|						  string $color = '0,0,0');		|
| - void	public	getNavi(integer $lvl = 1,			|
|							string $link = NULL);		|
| - void	public	__destruct();						|
\*******************************************************/

if(!defined('eCMS')) die('Hacking attempt...');

require_once(DIR_LIB_ECMS.'classes/upload.class.php');

class Media extends Upload {
	private $eCMS;
	
	private $mid;
	private $uid;
	private $filename;
	private $mime;
	private $size;
	private $timestamp;
	private $groups;
	private $title;
	private $description;
	private $type;
	private $border;
	private $temp;
	
	private $user;
	
	private $crop = array();
	private $needCrop = false;
	
	private $bFile;
	
	private $mObj;
	private $mObjTemp;
	
	private $loaded	= false;
	private $ready 	= false;
	
	public function __construct($eCMS) {
		parent::__construct($eCMS);
		
		$this->eCMS = $eCMS;
	}
	
	
	
	// This function loads all of the database-informations and put them into the variables.
	# @param int	$mid;
	# 
	# @return boolean
	public function loadMedia($mid, $uid = NULL) {
		if(!is_int($mid)) $this->eCMS->dieFunctionCall('loadMedia', 'mid', gettype($mid), 'integer');
		if($uid !== NULL && !is_int($uid)) $this->eCMS->dieFunctionCall('loadMedia', 'uid', gettype($uid), 'integer');
		
		// After this, we can work with it.
		// Check out, if we have an entry for the $mid.
		$row = $this->eCMS->db->getArray("SELECT 
												mid, 
												seo, 
												uid, 
												file, 
												mime, 
												size, 
												date, 
												groups, 
												title, 
												description, 
												type, 
												border, 
												temp 
											FROM 
												{$this->eCMS->db_prefix}media 
											WHERE 
												mid = '".$this->eCMS->db->secureInt($mid)."'");
		
		// Now we check, if we have 1 entry.
		if(count($row) === 1) {
			// At least, we save them to variables.
			$this->mid			= $row[0]['mid'];
			$this->seo			= $row[0]['seo'];
			$this->uid			= $row[0]['uid'];
			$this->filename		= $row[0]['file'];
			$this->mime			= $row[0]['mime'];
			$this->size			= $row[0]['size'];
			$this->timestamp	= $row[0]['date'];
			$this->groups		= $row[0]['groups'];
			$this->title		= $row[0]['title'];
			$this->description	= $row[0]['description'];
			$this->type			= $row[0]['type'];
			$this->border		= $row[0]['border'];
			$this->temp			= $row[0]['temp'];
			
			$this->user = $this->eCMS->user->getUser($this->uid);
			
			$this->loaded = true;
		} else $this->loaded = false;
	}
	
	// This function display out our media.
	public function showMedia() {
		// Check if media is loaded.
		if($this->loaded === true) {
			// Explode our mime-type for general file-handling.
			$mime = explode('/', $this->mime);
			
			// Now let us switch the first part of the mime-type.
			// In this case, we call general the needed function for the next steps.
			switch($mime[0]) {
				case 'application':
					// Get the file.
					$this->getApplication($mime[1]);
					// Check if all ready and loaded. If true, display out the full media-object.
					if($this->ready === true) $this->displayApplication($mime[1]);
					
					break;
				
				case 'audio':
					// Get the file.
					$this->getAudio($mime[1]);
					// Check if all ready and loaded. If true, display out the full media-object.
					if($this->ready === true) $this->displayAudio($mime[1]);
					
					break;
				
				case 'image':
					// Get the file.
					$this->getImage($mime[1]);
					// Check if all ready and loaded. If true, display out the full media-object.
					if($this->ready === true) $this->displayImage($mime[1]);
					
					break;
				
				case 'text':
					// Get the file.
					$this->getText($mime[1]);
					// Check if all ready and loaded. If true, display out the full media-object.
					if($this->ready === true) $this->displayText($mime[1]);
					
					break;
				
				case 'video':
					// Get the file.
					$this->getVideo($mime[1]);
					// Check if all ready and loaded. If true, display out the full media-object.
					if($this->ready === true) $this->displayVideo($mime[1]);
					
					break;
				
				default:
					// Call the error-function.
					$this->error('Can\'t detect MIME-Type. Please contact the administrator.');
					
					break;
			}
		// Call the error-function.
		} else $this->error('Can\'t load media-file. Please contact the administrator.');
	}
	
	// This function save a new media into our database.
	public function setMedia($mid, $name, $mime, $size, $type, $tmp = false) {
		if(!is_int($mid))		$this->eCMS->dieFunctionCall('setMedia', 'mid',		gettype($mid),	'integer');
		if(!is_string($name))	$this->eCMS->dieFunctionCall('setMedia', 'name',	gettype($name),	'string');
		if(!is_string($mime))	$this->eCMS->dieFunctionCall('setMedia', 'mime',	gettype($mime),	'string');
		if(!is_int($size))		$this->eCMS->dieFunctionCall('setMedia', 'size',	gettype($size),	'integer');
		if(!is_int($type))		$this->eCMS->dieFunctionCall('setMedia', 'type',	gettype($type),	'integer');
		if(!is_bool($tmp))		$this->eCMS->dieFunctionCall('setMedia', 'tmp',		gettype($tmp),	'boolean');
		
		// Check for temp and put into the database.
		if($tmp === true) {
			$this->eCMS->db->query("INSERT INTO {$this->eCMS->db_prefix}media (
																			mid, 
																			seo, 
																			uid, 
																			file, 
																			mime, 
																			size, 
																			date, 
																			groups, 
																			title, 
																			description, 
																			type, 
																			border, 
																			temp
																		) VALUES (
																			'".$this->eCMS->db->secureString((string)$mid)."', 
																			NULL, 
																			'".$this->eCMS->db->secureString((string)$_SESSION['eCMSuserUID'])."', 
																			'".$this->eCMS->db->secureString((string)$name)."', 
																			'".$this->eCMS->db->secureString((string)$mime)."', 
																			'".$this->eCMS->db->secureString((string)$size)."', 
																			'".time()."', 
																			'1,2,3,4,5,6,7', 
																			NULL, 
																			NULL, 
																			'".$this->eCMS->db->secureString((string)$type)."', 
																			'0', 
																			'1' 
																		)");
		} else {
			$this->eCMS->db->query("INSERT INTO {$this->eCMS->db_prefix}media (
																			mid, 
																			seo, 
																			uid, 
																			file, 
																			mime, 
																			size, 
																			date, 
																			title, 
																			description, 
																			type, 
																			border, 
																			temp
																		) VALUES (
																			'".$this->eCMS->db->secureString((string)$mid)."', 
																			NULL, 
																			'".$this->eCMS->db->secureString((string)$_SESSION['eCMSuserUID'])."', 
																			'".$this->eCMS->db->secureString((string)$name)."', 
																			'".$this->eCMS->db->secureString((string)$mime)."', 
																			'".$this->eCMS->db->secureString((string)$size)."', 
																			'".time()."', 
																			NULL, 
																			NULL, 
																			'".$this->eCMS->db->secureString((string)$type)."', 
																			'0', 
																			'1', 
																		)");
		}
	}
	
	
	
	private function getApplication() {}
	private function setApplication() {}
	private function displayApplication($mime) {
		if(!is_string($mime)) $this->eCMS->dieFunctionCall('displayApplication', 'mime', gettype($mime), 'string');
		
		switch($mime) {
			case 'gzip':
				$this->error('Can\'t display media-file. MIME \'application/gzip\' isn\'t currently supported.');
				
				break;
			
			case 'pdf':
				$this->error('Can\'t display media-file. MIME \'application/pdf\' isn\'t currently supported.');
				
				break;
			
			case 'rtf':
				$this->error('Can\'t display media-file. MIME \'application/rtf\' isn\'t currently supported.');
				
				break;
			
			case 'xhtml+xml':
				$this->error('Can\'t display media-file. MIME \'application/xhtml+xml\' isn\'t currently supported.');
				
				break;
			
			case 'xml':
				$this->error('Can\'t display media-file. MIME \'application/xml\' isn\'t currently supported.');
				
				break;
			
			case 'x-compress':
				$this->error('Can\'t display media-file. MIME \'application/x-compress\' isn\'t currently supported.');
				
				break;
			
			case 'x-gtar':
				$this->error('Can\'t display media-file. MIME \'application/x-gtar\' isn\'t currently supported.');
				
				break;
			
			case 'x-httpd-php':
				$this->error('Can\'t display media-file. MIME \'application/x-httpd-php\' isn\'t currently supported.');
				
				break;
			
			case 'x-javascript':
				$this->error('Can\'t display media-file. MIME \'application/x-javascript\' isn\'t currently supported.');
				
				break;
			
			case 'x-sh':
				$this->error('Can\'t display media-file. MIME \'application/x-sh\' isn\'t currently supported.');
				
				break;
			
			case 'x-shockwave-flash':
				$this->error('Can\'t display media-file. MIME \'application/x-shockwave-flash\' isn\'t currently supported.');
				
				break;
			
			case 'x-tar':
				$this->error('Can\'t display media-file. MIME \'application/x-tar\' isn\'t currently supported.');
				
				break;
			
			case 'zip':
				$this->error('Can\'t display media-file. MIME \'application/zip\' isn\'t currently supported.');
				
				break;
			
			default:
				$this->error('Can\'t display media-file. Please contact the administrator.');
				
				break;
		}
	}
	
	
	
	private function getAudio($mime) {}
	private function setAudio() {}
	private function displayAudio($mime) {
		if(!is_string($mime)) $this->eCMS->dieFunctionCall('displayAudio', 'mime', gettype($mime), 'string');
		
		switch($mime) {
			case 'basic':
				$this->error('Can\'t display media-file. MIME \'audio/basic\' isn\'t currently supported.');
				
				break;
			
			case 'voxware':
				$this->error('Can\'t display media-file. MIME \'audio/voxware\' isn\'t currently supported.');
				
				break;
			
			case 'x-aiff':
				$this->error('Can\'t display media-file. MIME \'audio/x-aiff\' isn\'t currently supported.');
				
				break;
			
			case 'x-midi':
				$this->error('Can\'t display media-file. MIME \'audio/x-midi\' isn\'t currently supported.');
				
				break;
			
			case 'x-mpeg':
				$this->error('Can\'t display media-file. MIME \'audio/x-mpeg\' isn\'t currently supported.');
				
				break;
			
			case 'x-wav':
				$this->error('Can\'t display media-file. MIME \'audio/x-wav\' isn\'t currently supported.');
				
				break;
			
			default:
				$this->error('Can\'t display media-file. Please contact the administrator.');
				
				break;
		}
	}
	
	
	
	// In this function, we load our image and do all the funny things, that php can do with GD-Lib ;)
	private function getImage($mime) {
		if(!is_string($mime)) $this->eCMS->dieFunctionCall('getImage', 'mime', gettype($mime), 'string');
		
		// First switch our second part of the mime-type to check, what type of picture it is.
		// Why is this essential?
		// Because of the fact, that we need different loading-functions for our image (imagecreatefromjpeg, imagecreatefrompng, etc).
		switch($mime) {
			case 'gif':
				$this->error('Can\'t display media-file. MIME \'image/gif\' isn\'t currently supported.');
				
				break;
			
			case 'jpeg':
				// Let's check, if it's temp-file for cropping, etc. ...
				// ... and create the original media-object a.k.a. $mObjTemp.
				if($this->temp === 'TRUE') $this->mObjTemp = @imagecreatefromjpeg(DIR_MEDIA_TMP.$this->filename);
				if($this->temp === 'FALSE') $this->mObjTemp = @imagecreatefromjpeg(DIR_MEDIA_IMAGE.$this->filename);
				
				break;
			
			case 'png':
				// Let's check, if it's temp-file for cropping, etc.
				if($this->temp === 'TRUE') $this->mObjTemp = @imagecreatefrompng(DIR_MEDIA_TEMP.$this->filename);
				if($this->temp === 'FALSE') $this->mObjTemp = @imagecreatefrompng(DIR_MEDIA_IMAGE.$this->filename);
				
				break;
			
			case 'tiff':
				$this->error('Can\'t display media-file. MIME \'image/tiff\' isn\'t currently supported.');
				
				break;
			
			case 'x-icon':
				$this->error('Can\'t display media-file. MIME \'image/x-icon\' isn\'t currently supported.');
				
				break;
			
			default:
				$this->error('Can\'t display media-file. Please contact the administrator.');
				
				break;
		}
		
		// Now we check, if source is successfully loaded.
		if($this->mObjTemp) {
			// Source loaded.
			// Now we needs to now many things and setup little things. What a sentences ;)
			
			// First, we needs to know, if we need a crop.
			if($this->getCrop((int)$this->mid) === true) $this->needCrop = true;
			else $this->needCrop = false;
			
			// Now we setup the needed informations for our finish media-object.
			
			// Create our finish media-object.
			// But what for a w/h we need?
			// There are 2 options:
			//  - Normal Picture (use the same w/h-values as the original picture)
			//  - Profile Picture (use fixed w/h-values 170x226)
			if($this->needCrop == true)  $this->mObj = @imagecreatetruecolor(170, 226);
			if($this->needCrop == false) $this->mObj = @imagecreatetruecolor(imagesx($this->mObjTemp), imagesy($this->mObjTemp));
			
			// Now we copy the cropped area to the finish media-object.
			// But we need a check, if cropping is needed. Else copy 1:1.
			if($this->needCrop === true) imagecopyresampled($this->mObj, $this->mObjTemp, 0, 0, $this->crop['x'], $this->crop['y'], 170, 226, $this->crop['w'], $this->crop['h']);
			if($this->needCrop === false) imagecopy($this->mObj, $this->mObjTemp, 0, 0, 0, 0, imagesx($this->mObjTemp), imagesy($this->mObjTemp));
			
			// This for alpha-channel. (PNG only)
			if($mime === 'png') {
				// Turn on alpha blending and set alpha flag
				imagealphablending($this->mObjTemp, true);
				imagesavealpha($this->mObjTemp, true);
			}
			
			// Let us check now, if border or overlay is needed.
			// Read out the type-informations.
			$type = $this->eCMS->db->getArray("SELECT 
													title, 
													border 
												FROM 
													{$this->eCMS->db_prefix}media_types 
												WHERE 
													tid = '".$this->eCMS->db->secureString($this->type)."'");
			
			// Check if border is needed and apply border or watermark.
			if(count($type) === 1) {
				// Border needed.
				if($type[0]['border'] === 'TRUE' && $this->temp === 'FALSE') $this->applyBorder();
				// Watermark needed.
				if($type[0]['border'] === 'FALSE') $this->applyWatermark();
				
				// Set ready to true.
				// Now our media-object is completely and ready to display out.
				$this->ready = true;
			}
		}
	}
	
	public function setImage($mid, $tmp = false) {
		$row = $this->eCMS->db->getArray("SELECT file FROM {$this->eCMS->db_prefix}media WHERE mid = '".$this->eCMS->db->secureString((string)$mid)."'");
		
		if(count($row) === 1) {
			if($tmp === false) {
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}media SET temp = 'FALSE' WHERE mid = '".$this->eCMS->db->secureString((string)$mid)."'");
				
				copy(DIR_MEDIA_TMP.$row[0]['file'], DIR_MEDIA_IMAGE.$row[0]['file']);
				unlink(DIR_MEDIA_TMP.$row[0]['file']);
			} else {
				$this->eCMS->db->query("UPDATE {$this->eCMS->db_prefix}media SET temp = 'TRUE' WHERE mid = '".$this->eCMS->db->secureString((string)$mid)."'");
				
				copy(DIR_MEDIA_IMAGE.$row[0]['file'], DIR_MEDIA_TMP.$row[0]['file']);
				unlink(DIR_MEDIA_IMAGE.$row[0]['file']);
			}
		} else return false;
	}
	
	public function setCrop($mid, $x, $y, $x2, $y2, $w, $h) {
		if(!$this->eCMS->db->query("INSERT INTO {$this->eCMS->db_prefix}media_crops (
																					mid, 
																					x, 
																					y, 
																					x2, 
																					y2, 
																					w, 
																					h
																			) VALUES (
																				'".$this->eCMS->db->secureString((string)$mid)."', 
																				'".$this->eCMS->db->secureString((string)$x)."', 
																				'".$this->eCMS->db->secureString((string)$y)."', 
																				'".$this->eCMS->db->secureString((string)$x2)."', 
																				'".$this->eCMS->db->secureString((string)$y2)."', 
																				'".$this->eCMS->db->secureString((string)$w)."', 
																				'".$this->eCMS->db->secureString((string)$h)."')")) return false;
		else return true;
	}
	
	private function getCrop($mid) {
		if(!is_integer($mid)) $this->eCMS->dieFunctionCall('getCrop', 'mid', gettype($mid), 'integer');
		
		$row = $this->eCMS->db->getArray("SELECT 
												mid, 
												x, 
												y, 
												x2, 
												y2, 
												w, 
												h 
											FROM 
												{$this->eCMS->db_prefix}media_crops 
											WHERE 
												mid = '".$this->eCMS->db->secureInt($mid)."'");
		
		if(count($row) === 1) {
			$this->crop = $row[0];
			
			return true;
		} else return false;
	}
	
	private function applyBorder() {
		// Let us load our border and put over the picture.
		// First read out the border-informations.
		if($this->border > 0) $border = $this->eCMS->db->getArray("SELECT 
																		title, 
																		file 
																	FROM 
																		{$this->eCMS->db_prefix}media_border 
																	WHERE 
																		bid = '".$this->eCMS->db->secureString($this->border)."'");
		else $border = $this->eCMS->db->getArray("SELECT 
														title, 
														file 
													FROM 
														{$this->eCMS->db_prefix}media_border 
													WHERE 
														bid = '1'");
		
		// Check for existing border-entry, ...
		if(count($border) === 1) {
			// ... try to load the image, ...
			$b_im = @imagecreatefrompng(IMAGES_DIR.'/media/border/'.$border[0]['file']);
			// ... set the scale ...
			$b_w = 170;
			$b_h = 226;
			
			// ... and check for right loading.
			if($b_im) imagecopy($this->mObj, $b_im, 0, 0, 0, 0, $b_w, $b_h);
			else $this->error('Can\'t load border-file. Please contact the administrator.');
			
			// We add the nick on top at least.
			// Load font.
			#$font = imageloadfont($this->eCMS->template['templatePath'].'fonts/pirulen.ttf');
			
			$center_x = ceil( ( imagesx($this->mObj) - ( ImageFontWidth(5) * strlen($this->user['username']) ) ) / 2 ); 
			imagestring($this->mObj, 5, $center_x, 8, $this->user['username'], imagecolorallocate($this->mObj, 0, 0, 0));
			// int imagestring ( resource $im , int $font , int $x , int $y , string $s , int $col )
		} else $this->error('Can\'t load border-file. Please contact the administrator.');
	}
	
	private function applyWatermark() {
		// Get the width and height.
		$dst_w = imagesx($this->mObj);
		$dst_h = imagesy($this->mObj);
		
		// We needs to know, what for an width our overlay has to be.
		if($dst_w / 2 >= 350) {
			$wm_im = @imagecreatefrompng(IMAGES_DIR.'/media/watermark/_watermark_300.png');
			$wm_w  = 300;
			$wm_h  = 135;
		}
		elseif($dst_w / 2 >= 200) {
			$wm_im = @imagecreatefrompng(DIMAGES_DIR.'/media/watermark/_watermark_150.png');
			$wm_w  = 150;
			$wm_h  = 68;
		}
		elseif($dst_w / 2 >= 150) {
			$wm_im = @imagecreatefrompng(IMAGES_DIR.'/media/watermark/_watermark_100.png');
			$wm_w  = 100;
			$wm_h  = 45;
		}
		else {
			$wm_im = @imagecreatefrompng(IMAGES_DIR.'/media/watermark/_watermark_50.png');
			$wm_w  = 50;
			$wm_h  = 23;
		}
		
		imagecopy($this->mObj, $wm_im, ($dst_w - ($wm_w + 10)), ($dst_h - ($wm_h + 10)), 0, 0, $wm_w, $wm_h);
	}
	
	private function displayImage($mime) {
		if(!is_string($mime)) $this->eCMS->dieFunctionCall('displayImage', 'mime', gettype($mime), 'string');
		
		switch($mime) {
			case 'gif':
				$this->error('Can\'t display media-file. MIME \'image/gif\' isn\'t currently supported.');
				
				break;
			
			case 'jpeg':
				// Set the header.
				header("Content-type:image/jpeg");
				
				imagejpeg($this->mObj);
				imagedestroy($this->mObj);
				
				break;
			
			case 'png':
				// Set the header.
				header("Content-type:image/png");
				
				imagepng($this->mObj);
				imagedestroy($this->mObj);
				
				break;
			
			case 'tiff':
				$this->error('Can\'t display media-file. MIME \'image/tiff\' isn\'t currently supported.');
				
				break;
			
			case 'x-icon':
				$this->error('Can\'t display media-file. MIME \'image/x-icon\' isn\'t currently supported.');
				
				break;
			
			default:
				$this->error('Can\'t display media-file. Please contact the administrator.');
				
				break;
		}
	}
	
	
	
	private function getText($mime) {}
	private function setText() {}
	private function displayText($mime) {
		if(!is_string($mime)) $this->eCMS->dieFunctionCall('displayText', 'mime', gettype($mime), 'string');
		
		switch($mime) {
			case 'css':
				$this->error('Can\'t display media-file. MIME \'text/css\' isn\'t currently supported.');
				
				break;
			
			case 'html':
				$this->error('Can\'t display media-file. MIME \'text/html\' isn\'t currently supported.');
				
				break;
			
			case 'javascript':
				$this->error('Can\'t display media-file. MIME \'text/javascript\' isn\'t currently supported.');
				
				break;
			
			case 'plain':
				$this->error('Can\'t display media-file. MIME \'text/plain\' isn\'t currently supported.');
				
				break;
			
			case 'rtf':
				$this->error('Can\'t display media-file. MIME \'text/rtf\' isn\'t currently supported.');
				
				break;
			
			case 'xml':
				$this->error('Can\'t display media-file. MIME \'text/xml\' isn\'t currently supported.');
				
				break;
			
			default:
				$this->error('Can\'t display media-file. Please contact the administrator.');
				
				break;
		}
	}
	
	
	
	private function getVideo($mime) {}
	private function setVideo() {}
	private function displayVideo($mime) {
		if(!is_string($mime)) $this->eCMS->dieFunctionCall('displayVideo', 'mime', gettype($mime), 'string');
		
		switch($mime) {
			case 'mpeg':
				$this->error('Can\'t display media-file. MIME \'video/mpeg\' isn\'t currently supported.');
				
				break;
			
			case 'quicktime':
				$this->error('Can\'t display media-file. MIME \'video/quicktime\' isn\'t currently supported.');
				
				break;
			
			case 'x-msvideo':
				$this->error('Can\'t display media-file. MIME \'video/x-msvideo\' isn\'t currently supported.');
				
				break;
			
			default:
				$this->error('Can\'t display media-file. Please contact the administrator.');
				
				break;
		}
	}
	
	
	
	private function error($errorString, $bg = '255,0,0', $color = '0,0,0') {
		header("Content-type:image/png");
		
		$im = @imagecreatetruecolor(800, 50);
		
		$errorString = 'ERROR: '.$errorString;
		$bg = explode(',', $bg);
		$color = explode(',', $color);
		
		$bg		= imagecolorallocate($im,	$bg[0],		$bg[1],		$bg[2]);
		$color	= imagecolorallocate($im,	$color[0],	$color[1],	$color[2]);
		
		imagefill($im, 0, 0, $bg);
		imagestring($im, 5, 10, 17.5, $errorString, $color);
		
		imagepng($im);
		imagedestroy($im);
	}
	
	public function getNavi($lvl = 1, $link = NULL) {
		switch($lvl) {
			case 1:
				$ret = array();
				
				$row = $this->eCMS->db->getArray("SELECT 
														nid, 
														title, 
														link 
													FROM 
														{$this->eCMS->db_prefix}media_navi 
													WHERE 
														level = '1' 
													ORDER BY 
														sort 
													ASC");
				
				if(count($row) > 0) $ret = $row;
				
				return $ret;
				
				break;
			
			case 2:
				$ret = array();
				
				$row = $this->eCMS->db->getArray("SELECT 
														nid, 
														title, 
														link 
													FROM 
														{$this->eCMS->db_prefix}media_navi 
													WHERE 
														level = '2' 
													ORDER BY 
														sort 
													ASC");
				
				if(count($row) > 0) $ret = $row;
				
				return $ret;
				
				break;
			
			case 3:
				$ret = array();
				
				$row = $this->eCMS->db->getArray("SELECT 
														nid, 
														title, 
														link 
													FROM 
														{$this->eCMS->db_prefix}media_navi 
													WHERE 
														level = '3' 
													ORDER BY 
														sort 
													ASC");
				
				if(count($row) > 0) $ret = $row;
				
				return $ret;
				
				break;
			
			default:
				return false;
				
				break;
		}
	}
	
	
	
	public function getRandomMedia($type, $sType) {
		$ret = '';
		
		switch($type) {
			case 'image':
				switch($sType) {
					case 'normal':
						$row = $this->eCMS->db->getArray("SELECT mid FROM {$this->eCMS->db_prefix}media WHERE (mime = 'image/png' OR mime = 'image/jpeg') AND type = '1' AND uid > '0' AND temp = 'FALSE'");
						
						if(count($row) > 0) {
							$mCount = count($row) - 1;
							$rnd = rand(0, $mCount);
							
							$ret = $row[$rnd]['mid'];
						}
						
						break;
					
					case 'profile':
						$row = $this->eCMS->db->getArray("SELECT mid FROM {$this->eCMS->db_prefix}media WHERE (mime = 'image/png' OR mime = 'image/jpeg') AND type = '2' AND uid > '0' AND temp = 'FALSE'");
						
						if(count($row) > 0) {
							$mCount = count($row) - 1;
							$rnd = rand(0, $mCount);
							
							$ret = $row[$rnd]['mid'];
						}
						
						break;
				}
				
				break;
		}
		
		return $ret;
	}
	
	public function getFileInfo($mid) {
		$ret = array();
		
		$this->loadMedia($mid);
		
		if($this->loaded == true) {
			$user = $this->eCMS->user->getUser($this->uid);
			
			$ret = array(
						'mid' => $this->mid, 
						'uid' => $this->uid, 
						'username' => $user['username'], 
						'filename' => $this->filename, 
						'mime' => $this->mime, 
						'size' => $this->size, 
						'timestamp' => $this->timestamp, 
						'groups' => $this->groups, 
						'title' => $this->title, 
						'description' => $this->description, 
						'type' => $this->type, 
						'border' => $this->border, 
						'temp' => $this->temp);
		}
		
		return $ret;
	}
	
	
	
	
	public function __destruct() {}
}
?>