<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * File:     modifier.timeSpan.php
 * Type:     modifier
 * Name:     timeSpan
 * Purpose:  onvert date to a human friendly string such as "32 minutes ago"
 * -------------------------------------------------------------
 */
function smarty_modifier_timeSpan($string) {
	$output = '';
	
	// Actual timestamp;
	$now					= time();
	
	// Day
	$nowDayNumeric			= date('d', $now);
	$nowDayOfWeekShort		= date('D', $now);
	$nowDayOfWeekLong		= date('l', $now);
	$nowDayOfWeekNumeric	= date('N', $now);
	$nowDayOfYear			= date('z', $now);
	
	// Week
	$nowWeekNumeric			= date('W', $now);
	
	// Month
	$nowMonthNumeric		= date('m', $now);
	$nowMonthShort			= date('M', $now);
	$nowMonthLong			= date('F', $now);
	$nowMonthDays			= date('t', $now);
	
	// Year
	$nowYearNumericShort	= date('y', $now);
	$nowYearNumericLong		= date('Y', $now);
	$nowYearLeap			= date('L', $now);
	
	// Time
	$nowHour				= date('H', $now);
	$nowMinute				= date('i', $now);
	$nowSecond				= date('s', $now);
	
	
	$tStamp					= $string;
	$tSpan					= $now - $tStamp;
	
	$date					= date('d.m.Y', $tStamp);
	$time					= date('H:i', $tStamp);
	
	$seconds				= round($tSpan);
	$minutes				= round($tSpan / 60);
	$hours					= round($tSpan / 3600);
	
	
	
	if($tSpan < 0)						$output = $date;
	elseif($tSpan == 1)					$output = 'vor einer Sekunde';
	elseif($tSpan < 60)					$output = 'vor '.$seconds.' Sekunden';
	elseif($tSpan < 120)				$output = 'vor einer Minute';
	elseif($tSpan < 3600)				$output = 'vor '.$minutes.' Minuten';
	elseif($tSpan < 7200) 				$output = 'vor einer Stunde';
	elseif($tSpan < 86400)				$output = 'vor '.$hours.' Stunden';
	#elseif($date == $dateYesterday)		$output = 'Gestern';
	else								$output = $date.' um '.$time.' Uhr';
	
	
	
	/*
	if($timeSpan < 0) $spanMessage = $date.' '.$time;
	elseif($timeSpan < 60) $spanMessage = 'vor '.$seconds.' Sekunden';														// less than 1 minute
	elseif($timeSpan < 120) $spanMessage = 'vor '.$minutes.' Minute';															// less than 2 minutes
	elseif($timeSpan < 3600) $spanMessage = 'vor '.$minutes.' Minuten';														// less than 60 minutes ago
	elseif($timeSpan < 7200) $spanMessage = 'vor '.$hours.' Stunde';															// less than 2 hours ago
	elseif($timeSpan < 86400) $spanMessage = 'vor '.$hours.' Stunden';														// less than 24 hours ago
	elseif($date == $dateYesterday) $spanMessage = 'Gestern';																			// yesterday
	elseif($timeSpan < 432000) $spanMessage = $dayOfMonth .' um '.$time.' Uhr';										// less than 5 days ago
	elseif($year == $yearNow) $spanMessage = $dayOfMonthNumeric.'. '.$month.' um '.$time.' Uhr';	// more than 5 days ago this year
	else $spanMessage = $dayOfMonthNumeric.'. '.$month.' '.$year.' um '.$time.' Uhr';							// more than a year ago
	*/
	
	/*$now									= time();
	$timeSpan							= $now - $timestamp;
	$date									= date('d.m.Y', $timestamp);
	$dateYesterday				= date('d.m.Y', $now - 86400);
	$dayOfMonth						= date('l', $timestamp);
	$time									= date('h:i', $timestamp);
	$seconds							= round($timeSpan);
	$minutes							= round($timeSpan / 60);
	$hours								= round($timeSpan / 3600);
	$month								= date('m', $timestamp);
	$dayOfMonthNumeric		= date('d', $timestamp);
	$year									= date('Y', $timestamp);
	$yearNow							= date('Y', $now);*/
	
	/*$tStamp		= $string;
	$now		= time();
	$nowYear	= date('Y', $now);
	$tSpan		= $now - $tStamp;
	$date		= date('d. ', $tStamp);
	$yesterday	= date('d. ', $now - 86400);
	$dayOfMonth	= date('l', $Stamp);
	$time		= date('h:i', $tStamp);
	$day		= date('d',	$tStamp);
	$month		= date('m', $tStamp);
	$year		= date('Y', $tStamp);
	$hour		= round($tStamp / 3600);
	$minute		= round($tStamp / 60);
	$second		= round($tStamp);*/
	
	/*switch($dayOfMonth) {
		case 'Monday'		: $dayOfMonth = 'Montag';		break;
		case 'Tuesday'		: $dayOfMonth = 'Dienstag';		break;
		case 'Wednesday'	: $dayOfMonth = 'Mittwoch';		break;
		case 'Thursday'		: $dayOfMonth = 'Donnerstag';	break;
		case 'Friday'		: $dayOfMonth = 'Freitag';		break;
		case 'Saturday'		: $dayOfMonth = 'Samstag';		break;
		case 'Sunday'		: $dayOfMonth = 'Sonntag';		break;
	}*/
	
	
	
	
	
	return $output;
}
?>