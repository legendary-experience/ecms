<?php
if(!defined('eCMS')) die('Hacking attempt...');

/****************\
|* Main defines *|
\****************/
// Define php error reporting level
#     1 - E_ERROR				- Fatale Laufzeit-Fehler.
#									Beispielsweise Probleme bei der Speicherzuweisung.
#									Die Ausf�hrung des Skripts wird abgebrochen.
#     2 - E_WARNING				- Warnungen (keine fatalen Fehler) zur Laufzeit des Skripts.
#									Das Skript wird nicht abgebrochen.
#     4 - E_PARSE				- Parser-Fehler w�hrend der �bersetzung.
#									Parser-Fehler k�nnen nur vom Parser erzeugt werden.
#     8 - E_NOTICE				- Benachrichtigungen w�hrend der Laufzeit.
#									Sie zeigen an, dass im Skript irgend etwas gefunden wurde, was einen Fehler verursachen k�nnte.
#									Es ist aber genauso m�glich, dass Benachrichtigungen im ordnungsgem��en Ablauf eines Skripts ausgegeben werden.
#    16 - E_CORE_ERROR			- Fatale Fehler, die beim Starten von PHP auftreten.
#									Diese sind �hnlich wie E_ERROR, nur dass diese Fehlermeldungen vom PHP-Kern erzeugt werden. 
#    32 - E_CORE_WARNING		- Warnungen (keine fatalen Fehler), die beim Starten von PHP auftreten.
#									Diese sind �hnlich wie E_WARNING, nur dass diese Warnungen vom PHP-Kern erzeugt werden. 
#    64 - E_COMPILE_ERROR		- Fatale Fehler zur �bersetzungszeit.
#									Diese sind �hnlich wie E_ERROR, nur dass diese Fehlermeldungen von der Zend Scripting Engine erzeugt werden. 
#   128 - E_COMPILE_WARNING		- Warnungen zur �bersetzungszeit.
#									Diese sind �hnlich wie E_WARNING, nur dass diese Warnungen von der Zend Scripting Engine erzeugt werden. 
#   256 - E_USER_ERROR			- Benutzerdefinierte Fehlermeldungen.
#									Diese sind �hnlich wie E_ERROR, nur dass diese Fehlermeldungen im PHP-Code mit trigger_error() erzeugt werden.
#   512 - E_USER_WARNING		- Benutzerdefinierte Warnungen.
#									Diese sind �hnlich wie E_WARNING, nur dass diese Warnungen im PHP-Code mit trigger_error() erzeugt werden. 
#  1024 - E_USER_NOTICE			- Benutzerdefinierte Benachrichtigung.
#									Diese sind �hnlich wie E_NOTICE, nur dass diese Benachrichtigungen im PHP-Code mit trigger_error() erzeugt werden. 
#  2048 - E_STRICT				- Benachrichtigungen des Laufzeitsystems.
#									Damit erhalten Sie von PHP Vorschl�ge f�r �nderungen des Programmcodes, die eine bestm�gliche Interoperabilit�t und zuk�nftige Kompatibilit�t Ihres Codes gew�hrleisten. 
#  4096 - E_RECOVERABLE_ERROR	- Abfangbarer fataler Fehler.
#									Dies bedeutet das ein potentiell gef�hrlicher Fehler aufgetreten ist, die Engine aber nicht in einem instabilen Zustand hinterlassen hat.
#									Wird der Fehler nicht durch eine benutzerdefinierte Fehlerbehandlungsroutine abgefangen (siehe auch set_error_handler()) so wird die Anwendung wie bei einem E_ERROR Fehler abgebrochen. 
#  8192 - E_DEPRECATED			- Notices zur Laufzeit des Programms.
#									Aktivieren Sie diese Einstellung, um Warnungen �ber Codebestandteile zu erhalten, die in zuk�nftigen PHP-Versionen nicht mehr funktionieren werden. 
# 16384 - E_USER_DEPRECATED		- Benutzererzeugte Warnmeldung.
#									Diese entspricht E_DEPRECATED mit der Ausnahme, dass sie im PHP-Code durch die Verwendung der Funktion trigger_error() generiert wurde. 
# 32767 - E_ALL					- Alle Fehler und Warnungen die unterst�tzt werden, mit Ausnahme von E_STRICT.
# Development-Mode
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE | E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_COMPILE_WARNING | E_USER_ERROR | E_USER_WARNING | E_USER_NOTICE | E_RECOVERABLE_ERROR);
# Debug-Mode
#error_reporting(E_ALL | E_STRICT);
# Productive-Mode
#error_reporting(0);

########## Page Info ##########
# The name of your page.
define('GENERAL_PAGE_TITLE',			'eCMS - The easyContent Management System for Web2.0');
# URL to your pages's folder. (with the trailing /!)
define('GENERAL_PAGE_URI',				'https://'.$_SERVER['HTTP_HOST'].'/eCMS/');
# Name to display as overlay
define('GENERAL_WEBMASTER_NAME',		'Jay Handke');
# Email address to send emails from. (like noreply@yourdomain.com.)
define('GENERAL_WEBMASTER_MAIL',		'jaymc@arcor.de');

# The default language file set for the page.
define('GENERAL_DEFAULT_LANGUAGE',		'eng');
# Name of the cookie to set for authentication.
define('GENERAL_COOKIE_NAME',			'eCMSCookie1337');



/******************\
|* xajax settings *|
\******************/
define('XAJAX_VERSION',	'0.5');
?>