<?php
if(!defined('eCMS')) die('Hacking attempt...');

/*******************\
|* Smarty settings *|
\*******************/
# Enable/Disable PHP Template-Files
define('SMARTY_PHP_TEMPLATES',			false);

# Enable/Disable auto literal
define('SMARTY_AUTO_LITERAL',			false);

# Enable/Disable caching
define('SMARTY_CACHING',				false);
# Set cache lifetime
define('SMARTY_CACHE_LIFETIME',			'-1');
# Enable/Disable cache looking
#define('SMARTY_CACHE_LOOKING',			false);
# Enable/Disable cache modified check
define('SMARTY_CACHE_MODIFIED_CHECK',	false);
# Set caching type
define('SMARTY_CACHING_TYPE',			'file');

# Set compile-id
define('SMARTY_COMPILE_ID',				$_SERVER['SERVER_NAME']);
# Enable/Disable compile looking
#define('SMARTY_COMPILE_LOOKING',		true);
# Set compiler class
#define('SMARTY_COMPILER_CLASS',			'Smarty_Compiler');

# Enable/Disable config booleanize
define('SMARTY_CONFIG_BOOLEANIZE',		true);
# Enable/Disable config overwrite
define('SMARTY_CONFIG_OVERWRITE',		false);
# Enable/Disable config read hidden
define('SMARTY_CONFIG_READ_HIDDEN',		false);
# Set default config type
define('SMARTY_DEFAULT_CONFIG_TYPE',	'file');

# Enable/Disable debugging
define('SMARTY_DEBUGGING',				false);
# Set debug tpl
define('SMARTY_DEBUG_TPL',				'debug.tpl');
# Set debugging ctrl
define('SMARTY_DEBUGGING_CTRL',			($_SERVER['SERVER_NAME'] == '127.0.0.1') ? 'URL' : 'NONE');

# Set default modifiers
#define('SMARTY_DEFAULT_MODIFIERS',		array('escape:"htmlall"'));
# Set default resource type
define('SMARTY_DEFAULT_RESOURCE_TYPE',	'file');

# Enable/Disable direct access security
define('SMARTY_DIRECT_ACCESS_SECURITY',	true);
?>