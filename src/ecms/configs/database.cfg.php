<?php
if(!defined('eCMS')) die('Hacking attempt...');

if(!defined('DB_DEFAULT_01')) define('DB_DEFAULT_01', 1);
if(!defined('DB_MANGOS_REALM')) define('DB_MANGOS_REALM', 2);
if(!defined('DB_MANGOS_CHARACTER')) define('DB_MANGOS_CHARACTER', 3);
if(!defined('DB_MANGOS_WORLD')) define('DB_MANGOS_WORLD', 4);

/*********************\
|* Database settings *|
\*********************/
$MYSQL_SETTINGS[1]['host']    = '127.0.0.1';
$MYSQL_SETTINGS[1]['port']    = 3306;
$MYSQL_SETTINGS[1]['user']    = 'ecms';
$MYSQL_SETTINGS[1]['pass']    = 'ecms';
$MYSQL_SETTINGS[1]['db']      = 'ecms';
$MYSQL_SETTINGS[1]['prefix']  = 'ecms_';
$MYSQL_SETTINGS[1]['charSet'] = 'utf8';

$MYSQL_SETTINGS[2]['host']    = '127.0.0.1';
$MYSQL_SETTINGS[2]['port']    = 3306;
$MYSQL_SETTINGS[2]['user']    = 'mangos';
$MYSQL_SETTINGS[2]['pass']    = 'mangos';
$MYSQL_SETTINGS[2]['db']      = 'realmd';
$MYSQL_SETTINGS[2]['prefix']  = '';
$MYSQL_SETTINGS[2]['charSet'] = 'utf8';

$MYSQL_SETTINGS[3]['host']    = '127.0.0.1';
$MYSQL_SETTINGS[3]['port']    = 3306;
$MYSQL_SETTINGS[3]['user']    = 'mangos';
$MYSQL_SETTINGS[3]['pass']    = 'mangos';
$MYSQL_SETTINGS[3]['db']      = 'character';
$MYSQL_SETTINGS[3]['prefix']  = '';
$MYSQL_SETTINGS[3]['charSet'] = 'utf8';

$MYSQL_SETTINGS[4]['host']    = '127.0.0.1';
$MYSQL_SETTINGS[4]['port']    = 3306;
$MYSQL_SETTINGS[4]['user']    = 'mangos';
$MYSQL_SETTINGS[4]['pass']    = 'mangos';
$MYSQL_SETTINGS[4]['db']      = 'world';
$MYSQL_SETTINGS[4]['prefix']  = '';
$MYSQL_SETTINGS[4]['charSet'] = 'utf8';
?>